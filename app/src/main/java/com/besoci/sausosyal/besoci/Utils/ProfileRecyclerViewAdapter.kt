package com.besoci.sausosyal.besoci.Utils

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.res.Resources
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.besoci.sausosyal.besoci.Models.UserPosts
import com.besoci.sausosyal.besoci.R
import kotlinx.android.synthetic.main.tek_sutun_grid_resim_profile.view.*
import org.greenrobot.eventbus.EventBus
import android.os.Build
import android.graphics.Bitmap
import android.graphics.Color
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.besoci.sausosyal.besoci.Generic.TekGonderiFragment
import com.besoci.sausosyal.besoci.Profile.ProfileEditFragment
import com.bumptech.glide.load.engine.Resource
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.tek_sutun_grid_resim_profile.*
import java.util.*
import android.databinding.adapters.ViewBindingAdapter.setOnLongClickListener
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.app.AlertDialog
import android.view.MenuItem
import android.widget.PopupMenu
import com.besoci.sausosyal.besoci.Profile.ProfileActivity
import com.besoci.sausosyal.besoci.Share.PostGuncelleActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*


class ProfileRecyclerViewAdapter(var kullaniciPostlari : ArrayList<UserPosts>, var myContext : Context ,var kendimi:Boolean): RecyclerView.Adapter<ProfileRecyclerViewAdapter.MyViewHolder>() {

lateinit var inflater:LayoutInflater

    init
    {

        inflater = LayoutInflater.from(myContext)

        //son çekilen başa gelsin için
        Collections.sort(kullaniciPostlari,object :Comparator<UserPosts>
        {
            override fun compare(p0: UserPosts?, p1: UserPosts?): Int {
                if(p0!!.postYuklenmeTarih!! > p1!!.postYuklenmeTarih!!)
                    return -1
                else
                    return 1
            }

        }
        )
    }




    override fun getItemCount(): Int {
return  kullaniciPostlari.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

var tekSutunDosya = inflater.inflate(R.layout.tek_sutun_grid_resim_profile,parent,false)
        return MyViewHolder(tekSutunDosya)
    }


    override fun onBindViewHolder(viewholder: MyViewHolder, position: Int) {

       var dosyaYolu = kullaniciPostlari.get(position).postURL

        if( dosyaYolu!!.contains(".")) {
            var dosyaTuru = dosyaYolu.substring(dosyaYolu.lastIndexOf("."),dosyaYolu.lastIndexOf(".")+4) //.dan sonra 4 hane getir yani .mp4

            if (dosyaTuru != null) {
                if (dosyaTuru.equals(".mp4"))
                 {
                   //aa  var thumnnailFoto =   retriveVideoFrameFromVideo(dosyaYolu)
                   //aa  viewholder.dosyaResim.setImageBitmap(thumnnailFoto)
                    //aa UniversalImageLoader.setImage( Resources.getSystem().getDrawable(R.drawable.ic_video).toString(),viewholder.dosyaResim,null,"")

                     viewholder.dosyaResim.visibility = View.GONE
                     viewholder.tumlayout.setBackgroundColor(Color.BLACK)
                     viewholder.imgVideoIcon.visibility = View.VISIBLE

                     //        var retriever = MediaMetadataRetriever()
            //        retriever.setDataSource(myContext, Uri.parse("file://"+dosyaYolu))
           //         var videoSuresi = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
           //         var videoSuresiLong = videoSuresi.toLong()

           //         viewholder.videosure.setText(convertDuration(videoSuresiLong))
            //        UniversalImageLoader.setImage(dosyaYolu,viewholder.dosyaResim,null,"file://")


                     viewholder.setData(kullaniciPostlari.get(position),true) //true demek video demek false resim tek post açmak için

                }
                else
                {
                    viewholder.dosyaResim.visibility = View.VISIBLE
                    viewholder.tumlayout.setBackgroundColor(Color.WHITE)
                    viewholder.imgVideoIcon.visibility = View.GONE

                    UniversalImageLoader.setImage(dosyaYolu!!,viewholder.dosyaResim,null,"")
                    viewholder.setData(kullaniciPostlari.get(position),false) //true demek video demek false resim tek post açmak için


                }


//viewholder.tekSutunDosya.setOnClickListener {
    //küçükresmeclickolayı olunca eventbus iler bu resmin yolunu yollar
  //  EventBus.getDefault().post(EventbusDataEvents.GallerySecilenDosyaYoluGonder(dosyaYolu))
//}



            }
        }
    }

    //videonun ilk karesiniresim olarak getiriyor
    @Throws(Throwable::class)
    fun retriveVideoFrameFromVideo(videoPath: String): Bitmap? {
        var bitmap: Bitmap? = null
        var mediaMetadataRetriever: MediaMetadataRetriever? = null
        try {
            mediaMetadataRetriever = MediaMetadataRetriever()
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, HashMap())
            else
                mediaMetadataRetriever.setDataSource(videoPath)

            bitmap = mediaMetadataRetriever.getFrameAtTime(1, MediaMetadataRetriever.OPTION_CLOSEST)
        } catch (e: Exception) {
            e.printStackTrace()
            throw Throwable(
                "Exception in retriveVideoFrameFromVideo(String videoPath)" + e.message
            )

        } finally {
            mediaMetadataRetriever?.release()
        }
        return bitmap
    }
    inner class MyViewHolder(itemView:View?) : RecyclerView.ViewHolder(itemView!!) { //üsttekinden kalıtım aldı inner ile



        var tekSutunDosya = itemView as ConstraintLayout
        var dosyaResim = tekSutunDosya.imageViewtekSutun
        var imgVideoIcon = tekSutunDosya.imageVideoIcon
        var tumlayout = tekSutunDosya.tumLayout

        fun setData( oAnkiGonderi: UserPosts, videoMu: Boolean) {


            tekSutunDosya.setOnLongClickListener {




                var popup: PopupMenu? = null;
                popup = PopupMenu(myContext, tekSutunDosya)
                popup.inflate(R.menu.act_menu)

                popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item: MenuItem? ->

                    when (item!!.itemId) {
                        R.id.header1 -> {



                            val intent =  Intent(myContext , PostGuncelleActivity::class.java)
                            intent.putExtra("OankiPost",oAnkiGonderi.postID)
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)

                            myContext.applicationContext.startActivity(intent)



                            Toast.makeText(myContext, item.title, Toast.LENGTH_SHORT).show();
                        }
                        R.id.header2 -> { //SIL


                            val builder = AlertDialog.Builder(myContext)
                            builder.setTitle("SİL")
                            builder.setMessage("Silmek istediğinizden emin misiniz?")
                            builder.setPositiveButton("İPTAL", object : DialogInterface.OnClickListener {
                                override fun onClick(dialog: DialogInterface, id: Int) {

                                }
                            })
                            builder.setNegativeButton("SİL", object : DialogInterface.OnClickListener {
                                override fun onClick(dialog: DialogInterface, id: Int) {

                                    var mRef : DatabaseReference = FirebaseDatabase.getInstance().reference
                                    var mRefComments : DatabaseReference = FirebaseDatabase.getInstance().reference
                                    var mRefLikes : DatabaseReference = FirebaseDatabase.getInstance().reference
                                    var mAuth=FirebaseAuth.getInstance()
                                    var mUser : FirebaseUser = mAuth!!.currentUser!!


                                    mRef.child("posts").child(mUser.uid).child(oAnkiGonderi.postID.toString()).removeValue() //postu sil
                                    mRef.child("onaylar").child(oAnkiGonderi.postID.toString()).removeValue()  //likelardan da sil
                                    mRef.child("comments").child(oAnkiGonderi.postID.toString()).removeValue()   //yorumlardan da sil


                                    postSayisiniGuncelle()
                                    val intent =  Intent(myContext , ProfileActivity::class.java)
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)

                                    myContext.applicationContext.startActivity(intent)
                                }
                            })
                            builder.show()















                        }

                    }

                    true
                })
                if(kendimi) //kendisi ise popup çıksın
                {
                    popup.show()
                }



                false//Bu satır silinmeyecek
            }




            tekSutunDosya.setOnClickListener {






                (myContext as AppCompatActivity).profileRoute.visibility = View.INVISIBLE //DENEEEEEE
                (myContext as AppCompatActivity).profileContainer.visibility = View.VISIBLE


                EventBus.getDefault().postSticky(EventbusDataEvents.secilenGonderiyiGonder(oAnkiGonderi,videoMu))

                    var transaction=(myContext as AppCompatActivity).supportFragmentManager.beginTransaction() //fragmentbaşlat
                    transaction.replace(R.id.profileContainer,TekGonderiFragment()) //gelenFragmenti koy
                    transaction.addToBackStack("tekgonderifragmenteklendi")
                    transaction.commit()







            }



        }

        fun postSayisiniGuncelle()  //post sayisini 1 arttırıyoruz
        {

            var mRef : DatabaseReference = FirebaseDatabase.getInstance().reference
            var mAuth=FirebaseAuth.getInstance()
            var mUser : FirebaseUser = mAuth!!.currentUser!!
            mRef.child("users").child(mUser.uid).child("user_detail").addListenerForSingleValueEvent(object : ValueEventListener
            {
                override fun onCancelled(p0: DatabaseError?) {

                }

                override fun onDataChange(p0: DataSnapshot?) {
                    var oankiGonderiSayisi = p0!!.child("gonderiSayisi").getValue().toString().toInt() //o anki gönderi sayısını getir
                    oankiGonderiSayisi-- //1 azalt
                    mRef.child("users").child(mUser.uid).child("user_detail").child("gonderiSayisi").setValue(oankiGonderiSayisi.toString())

                }

            }
            )
        }



    }



}