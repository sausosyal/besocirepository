package com.besoci.sausosyal.besoci.Profile


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.besoci.sausosyal.besoci.R
import com.besoci.sausosyal.besoci.VideoRecyclerView.view.VideoView
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.EmailAuthCredential
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.fragment_sifre_degistir.*
import kotlinx.android.synthetic.main.fragment_sifre_degistir.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class SifreDegistirFragment : Fragment() {
    var myView : View?=null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        myView= inflater.inflate(R.layout.fragment_sifre_degistir, container, false)

        myView!!.btn_onayla.setOnClickListener {

            var  sifre =myView!!.FragmentKayitEditTextEskiSifre.text.toString()
            var  yenisifre =myView!!.fragmentKayitEditTextYeniSifre.text.toString()
            var  sifretekrar =myView!!.fragmentKayitEditTextYeniSifre2.text.toString()
            if(sifre.length <6 || yenisifre.length<6 || sifretekrar.length < 6 )
            {
                Toast.makeText(activity,"Alanlar en az 6 karakter uzunlugunda olmalidir",Toast.LENGTH_LONG).show()
            }
            else {
                var muser = FirebaseAuth.getInstance().currentUser
                if (muser != null)
                {

                    var cridental = EmailAuthProvider.getCredential(muser!!.email.toString(),sifre)
                    muser.reauthenticate(cridental).addOnCompleteListener ( object : OnCompleteListener<Void>
                    {
                        override fun onComplete(p0: Task<Void>) {
                            if(sifretekrar.toString().equals(yenisifre.toString())) {
                                if(p0!!.isSuccessful)
                                {
                                var muser = FirebaseAuth.getInstance().currentUser
                                muser!!.updatePassword(yenisifre)
                                    .addOnCompleteListener(object : OnCompleteListener<Void> {
                                        override fun onComplete(p0: Task<Void>) {
                                            if (p0!!.isSuccessful) {
                                                Toast.makeText(activity, "Guncelleme Basarili", Toast.LENGTH_LONG)
                                                    .show()
                                                var mRef = FirebaseDatabase.getInstance().reference
                                                mRef.child("users").child(muser.uid).child("password").setValue(yenisifre.toString())
                                                val intent = Intent(activity,ProfileActivity::class.java)
                                                startActivity(intent)


                                            } else {
                                                Toast.makeText(
                                                    activity,
                                                    "Beklenmedik Bir Hata Olustu",
                                                    Toast.LENGTH_LONG
                                                ).show()
                                            }
                                        }
                                    }
                                    )
                            }
                                else
                                {
                                    Toast.makeText(activity,"Mevcut Sifre Yanlis",Toast.LENGTH_LONG).show()
                                }
                            }
                            else {
                                Toast.makeText(activity, "Yeni Sifreler Uyusmuyor", Toast.LENGTH_LONG).show()
                            }

                        }

                    })
                }

            }

        }



        return  myView
    }


}
