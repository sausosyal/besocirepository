package com.besoci.sausosyal.besoci.Models

class UserDetails {

    var takipciSayisi :String? = null
    var takipEdilenSayisi :String? = null
    var gonderiSayisi :String? = null
    var profilResmi :String? = null
    var aciklama :String? = null
    var webSitesi :String? = null

    constructor(){}
    constructor(takipciSayisi: String?, takipEdilenSayisi: String?, gonderiSayisi: String?,
                profilResmi: String?, aciklama: String?, webSitesi: String?)
    {
        this.takipciSayisi = takipciSayisi
        this.takipEdilenSayisi = takipEdilenSayisi
        this.gonderiSayisi = gonderiSayisi
        this.profilResmi = profilResmi
        this.aciklama = aciklama
        this.webSitesi = webSitesi
    }

    override fun toString(): String {
        return "UserDetails(takipciSayisi=$takipciSayisi, takipEdilenSayisi=$takipEdilenSayisi, gonderiSayisi=$gonderiSayisi, profilResmi=$profilResmi, aciklama=$aciklama, webSitesi=$webSitesi)"
    }


}