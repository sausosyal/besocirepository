package com.besoci.sausosyal.besoci.Models

class Posts {
var user_id :String ? =null
var post_id :String ? =null
var yuklenme_tarihi :Long ? =null
var aciklama :String ? =null
var photo_url :String ? =null
var latitude :String ? =null
var longitude :String ? =null
var mekan_tipi :String ? =null
var kategori :String ? =null
var aile :Boolean ? =null
var sevgili :Boolean ? =null
var cocuk :Boolean ? =null
var yetiskin :Boolean ? =null
var ogrenci :Boolean ? =null
var yalniz :Boolean ? =null


    constructor()
    constructor(
        user_id: String?,
        post_id: String?,
        yuklenme_tarihi: Long?,
        aciklama: String?,
        photo_url: String?,
        latitude: String?,
        longitude: String?,
        mekan_tipi: String?,
        kategori: String?,
        aile: Boolean?,
        sevgili: Boolean?,
        cocuk: Boolean?,
        yetiskin: Boolean?,
        ogrenci: Boolean?,
        yalniz: Boolean?
    ) {
        this.user_id = user_id
        this.post_id = post_id
        this.yuklenme_tarihi = yuklenme_tarihi
        this.aciklama = aciklama
        this.photo_url = photo_url
        this.latitude = latitude
        this.longitude = longitude
        this.mekan_tipi = mekan_tipi
        this.kategori = kategori
        this.aile = aile
        this.sevgili = sevgili
        this.cocuk = cocuk
        this.yetiskin = yetiskin
        this.ogrenci = ogrenci
        this.yalniz = yalniz
    }


}