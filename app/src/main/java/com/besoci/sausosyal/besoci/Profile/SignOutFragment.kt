package com.besoci.sausosyal.besoci.Profile


import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.besoci.sausosyal.besoci.Login.LoginActivity

import com.besoci.sausosyal.besoci.R
import com.besoci.sausosyal.besoci.Session.Session
import com.google.firebase.auth.FirebaseAuth

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class SignOutFragment : DialogFragment() {
    var session: Session? = null


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {


        session = com.besoci.sausosyal.besoci.Session.Session(this!!.activity!!)


        var alert = AlertDialog.Builder(this!!.activity!!)
            .setTitle("Çıkş Yap").setMessage("Çıkmak İstediğinize Emin Misiniz?")
            .setPositiveButton("Evet",object :DialogInterface.OnClickListener
            {
                override fun onClick(p0: DialogInterface?, p1: Int) {

                    FirebaseAuth.getInstance().signOut()
                    session!!.girisDurumu = false
                    var intent = Intent(activity!!, LoginActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    activity!!.finish()
                }

            }
            ).setNegativeButton("Hayır",object :DialogInterface.OnClickListener
            {
                override fun onClick(p0: DialogInterface?, p1: Int) {
                    dismiss() //alert dialog kapanır
                }

            }).create()

        return alert
    }


}
