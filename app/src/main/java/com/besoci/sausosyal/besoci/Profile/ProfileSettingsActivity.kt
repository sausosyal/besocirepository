package com.besoci.sausosyal.besoci.Profile

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import com.besoci.sausosyal.besoci.R
import com.besoci.sausosyal.besoci.Utils.BottomNavigationViewHelper
import kotlinx.android.synthetic.main.activity_profile_settings.*

class ProfileSettingsActivity : AppCompatActivity() {
    private val ACTIVITY_NO = 4;
    private val TAG = "Profile"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_settings)
        setupToolbar()
        setupNavigationView()
        fragmentNavigations()
    }

    private fun fragmentNavigations() {
        textViewHesapAyarlariProfiliDuzenle.setOnClickListener {
             fragmentChange(ProfileEditFragment(),"ProfilDuzenleFragmentEklendi")
        }

        textViewProfilAyarlariCikisYap.setOnClickListener {
            var dialog = SignOutFragment()
            dialog.show(supportFragmentManager,"CikisYapFragmentEklendi")

        }


        textViewHesapAyarlariSifreniDegistir.setOnClickListener {
            fragmentChange(SifreDegistirFragment(),"SifreDegistirFragmentEklendi")
        }

    }

    private fun fragmentChange(gelenFragment : Fragment , gelenKey:String)  {
        profileSettingsRoute.visibility=View.GONE //şimdiki viewi yani profil düzenleme ana ekranını kapat
        var transaction=supportFragmentManager.beginTransaction() //fragmentbaşlat
        transaction.replace(R.id.profileSettingsContainer,gelenFragment) //gelenFragmenti koy
        transaction.addToBackStack(gelenKey)
        transaction.commit()




    }


    private fun setupToolbar() {
        imageViewProfileSettingTopIconBack.setOnClickListener {
           onBackPressed() //geri tuşuna basınca olan olsun


        }

    }


    override fun onBackPressed() {  //geri basınca
        profileSettingsRoute.visibility = View.VISIBLE //yeniden ana viewi görünür yap
        super.onBackPressed()
    }


    fun setupNavigationView()
    {
        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewim)
        BottomNavigationViewHelper.setupNavigation(this,bottomNavigationViewim)
        var menu = bottomNavigationViewim.menu
        var menuitem = menu.getItem(ACTIVITY_NO)
        menuitem.setChecked(true)
    }
}
