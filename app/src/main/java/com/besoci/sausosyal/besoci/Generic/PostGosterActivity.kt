package com.besoci.sausosyal.besoci.Generic

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.besoci.sausosyal.besoci.R
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_post_goster.*

class PostGosterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_goster)
        var aile:Boolean?
        var sevgili:Boolean?
        var yetiskin:Boolean?
        var cocuk:Boolean?
        var ogrenci:Boolean?
        var yalniz:Boolean?
        var mekantipi:String?
        var kategori:String?
        var userID = intent.getStringExtra("UserId")
        var postID = intent.getStringExtra("PostId")
        val IC_KAPALI_ALAN ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fkapalialan.png?alt=media&token=7719ec94-30f3-4ba6-9560-a89805fb018e"
        val IC_ACIK_ALAN ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Facikalan.png?alt=media&token=dac457dc-4372-4994-b558-d27c40d7a957"
        val IC_YEMEK ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fyiyecek.png?alt=media&token=6eecb1dc-76fc-4640-ba51-d01df723cdb5"
        val IC_ALISVERIS ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Falisveris.png?alt=media&token=8ec792c2-a5bd-4304-be52-33e9594aa438"
        val IC_SPOR ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fspor.png?alt=media&token=3fff50e7-ff98-4fbd-a704-fc60455fb1b3"
        val IC_PIKNIK ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fpiknik.png?alt=media&token=18524be4-669d-4d79-ad68-cb076a7efb82"
        val IC_KULTURSANAT ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fkultursanat.png?alt=media&token=ba5853ea-dd0a-48ea-918a-06edd6ac09bf"
        val IC_DIGER = "https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Finformation.png?alt=media&token=0d8465af-c206-46aa-b3ef-4a1ed1f0a822"


        val IC_AILE="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Ffamily.png?alt=media&token=a7b16446-ccbe-44e2-83da-5ed07e3e0dff"
        val IC_YETISKIN="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fparents.png?alt=media&token=9224a382-d841-4701-ba72-6fce74669236"
        val IC_COCUK="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fchildren.png?alt=media&token=98679575-c6ae-4d41-a0b7-71ded6ed5ecd"
        val IC_YALNIZ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Falone.png?alt=media&token=97809c1e-7f79-44aa-846d-85b33599baca"
        val IC_SEVGILI ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Frelationship.png?alt=media&token=c73f8771-3895-4a5b-8c6c-dc64bbbfdcd6"
        val IC_OGRENCI ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fstudents.png?alt=media&token=989ca8a4-740d-4fd6-bda3-ff51d91e6c3d"




        Picasso.with(this@PostGosterActivity).load(IC_SEVGILI).into(imgSevgili)
        Picasso.with(this@PostGosterActivity).load(IC_OGRENCI).into(imgOgrenci)
        Picasso.with(this@PostGosterActivity).load(IC_AILE).into(imgAile)
        Picasso.with(this@PostGosterActivity).load(IC_YETISKIN).into(imgYetiskin)
        Picasso.with(this@PostGosterActivity).load(IC_COCUK).into(imgCocuk)
        Picasso.with(this@PostGosterActivity).load(IC_YALNIZ).into(imgYalniz)






        var mRef = FirebaseDatabase.getInstance().reference
        mRef.child("posts").child(userID).child(postID).addListenerForSingleValueEvent(object :ValueEventListener
            {
                override fun onCancelled(p0: DatabaseError?) {
                }

                override fun onDataChange(p0: DataSnapshot?) {


                    aile = p0!!.child("aile").getValue() as Boolean?
                    sevgili = p0!!.child("sevgili").getValue() as Boolean?
                    yetiskin = p0!!.child("yetiskin").getValue() as Boolean?
                    cocuk = p0!!.child("cocuk").getValue() as Boolean?
                    ogrenci = p0!!.child("ogrenci").getValue() as Boolean?
                    yalniz = p0!!.child("yalniz").getValue() as Boolean?
                    mekantipi =  p0!!.child("mekan_tipi").getValue() as String?
                    kategori =  p0!!.child("kategori").getValue() as String?


                    if(mekantipi.equals("Kapali Alan"))
                    { //imgMekan.setImageResource(R.drawable.kapalialan)
                        Picasso.with(this@PostGosterActivity).load(IC_KAPALI_ALAN).into(imgMekan)

                        checkBoxMekan.setText("Kapali Alan")
                    }
                    else
                    {  // imgMekan.setImageResource(R.drawable.acikalan)
                        Picasso.with(this@PostGosterActivity).load(IC_ACIK_ALAN).into(imgMekan)

                        checkBoxMekan.setText("Acik Alan")

                    }

                    if(kategori.equals("Yemek"))
                    {
                       // imgKategori.setImageResource(R.drawable.yiyecek)
                        Picasso.with(this@PostGosterActivity).load(IC_YEMEK).into(imgKategori)

                        checkBoxKategori.setText("Yemek")
                    }
                    else if(kategori.equals("Alisveris"))
                    {  // imgKategori.setImageResource(R.drawable.alisveris)
                        Picasso.with(this@PostGosterActivity).load(IC_ALISVERIS).into(imgKategori)

                        checkBoxKategori.setText("Alisveris")

                    }
                    else if(kategori.equals("Spor"))
                    {  //  imgKategori.setImageResource(R.drawable.spor)
                        Picasso.with(this@PostGosterActivity).load(IC_SPOR).into(imgKategori)

                        checkBoxKategori.setText("Spor")

                    }
                    else if(kategori.equals("Piknik-Kamp"))
                    {  //  imgKategori.setImageResource(R.drawable.piknik)
                        Picasso.with(this@PostGosterActivity).load(IC_PIKNIK).into(imgKategori)

                        checkBoxKategori.setText("Piknik-Kamp")

                    }
                    else if(kategori.equals("Kultur-Sanat"))
                    {    //imgKategori.setImageResource(R.drawable.kultursanat)
                        Picasso.with(this@PostGosterActivity).load(IC_KULTURSANAT).into(imgKategori)

                        checkBoxKategori.setText("Kultur-Sanat")

                    }
                    else if(kategori.equals("Diger"))
                    {    //imgKategori.setImageResource(R.drawable.kultursanat)
                        Picasso.with(this@PostGosterActivity).load(IC_DIGER).into(imgKategori)

                        checkBoxKategori.setText("Diger")

                    }

                    if(aile!! != null && aile!!)
                        checkBoxAile.isChecked = true
                   if(sevgili!!)
                        checkBoxSevgili.isChecked = true
                    if(yetiskin!!)
                        checkBoxYetiskin.isChecked = true
                    if(cocuk!!)
                        checkBoxCocuk.isChecked = true
                    if(ogrenci!!)
                    checkBoxOgrenci.isChecked = true
                    if(yalniz!!)
                        checkBoxYalniz.isChecked = true




                }
            })

    }
}
