package com.besoci.sausosyal.besoci.Profile


import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.besoci.sausosyal.besoci.Models.Users

import com.besoci.sausosyal.besoci.R
import com.besoci.sausosyal.besoci.Utils.EventbusDataEvents
import com.besoci.sausosyal.besoci.Utils.UniversalImageLoader
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.Task
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.nostra13.universalimageloader.core.ImageLoader
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.fragment_profile_edit.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.lang.Exception

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class ProfileEditFragment : Fragment() {


    lateinit var circleProfileImage :CircleImageView
    lateinit var gelenKullaniciBilgileri :Users
    lateinit var mDatabaseReferance :DatabaseReference
    lateinit var mStorageReference: StorageReference
    val RESIM_SEC = 1
    var profilePhotoURI : Uri? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_profile_edit, container, false)

        mDatabaseReferance = FirebaseDatabase.getInstance().reference
        mStorageReference = FirebaseStorage.getInstance().reference


        setupKullaniciBilgileri(view)

        view.imageViewProfilDuzenleClose.setOnClickListener {
            activity!!.onBackPressed()
        }


        view.textViewProfiliDuzenleFotografiDegistir.setOnClickListener {


            FotografDegistir()


            //onActivityResult a gider

        }


        view.ProfileEditCircleProfileImage.setOnClickListener{
            FotografDegistir()
        }



        view.imageViewProfilDuzenleCheck.setOnClickListener {

            var dialogYukleniyor = YukleniyorFragment()
            dialogYukleniyor.show(activity!!.supportFragmentManager,"yukleniyorfragmentiEklendi")
            dialogYukleniyor.isCancelable = false

            var guncellemeYapildi = false
            var kullanimda = false
            var resimdegisti = false

            if(gelenKullaniciBilgileri!!.user_name!!.equals(view.ProfilDuzenleEditTextKullaniciAdi.text.toString()))
            {
                if(!gelenKullaniciBilgileri!!.name_surname!!.equals(view.ProfilDuzenleEditTextAd.text.toString()))
                {
                    mDatabaseReferance.child("users").child(gelenKullaniciBilgileri!!.user_id).child("name_surname")
                        .setValue(view.ProfilDuzenleEditTextAd.text.toString())
                    guncellemeYapildi = true
                }
                if(!gelenKullaniciBilgileri!!.user_detail!!.aciklama!!.equals(view.ProfilDuzenleEditTextAciklama.text.toString()))
                {
                    mDatabaseReferance.child("users").child(gelenKullaniciBilgileri!!.user_id).child("user_detail").child("aciklama")
                        .setValue(view.ProfilDuzenleEditTextAciklama.text.toString())
                    guncellemeYapildi = true

                }

                if(!gelenKullaniciBilgileri!!.user_detail!!.webSitesi!!.equals(view.ProfilDuzenleEditTextInternetSitesi.text.toString()))
                {
                    mDatabaseReferance.child("users").child(gelenKullaniciBilgileri!!.user_id).child("user_detail").child("webSitesi")
                        .setValue(view.ProfilDuzenleEditTextInternetSitesi.text.toString())
                    guncellemeYapildi = true

                }

                if(profilePhotoURI!=null)
                {

                    guncellemeYapildi = true
                    resimdegisti = true

                    var uploadTask = mStorageReference.child("users").child(gelenKullaniciBilgileri!!.user_id!!).child(gelenKullaniciBilgileri!!.user_id!!+"_profile_photo").
                            putFile(profilePhotoURI!!).addOnCompleteListener(object  : OnCompleteListener<UploadTask.TaskSnapshot>
                    {
                        override fun onComplete(p0: Task<UploadTask.TaskSnapshot>) {
                            if(p0!!.isSuccessful)
                            {



                                mDatabaseReferance.child("users").child(gelenKullaniciBilgileri!!.user_id!!).child("user_detail").child("profilResmi")
                                    .setValue(p0!!.getResult().downloadUrl.toString())


                           //     dialogYukleniyor.dismiss()
                                Toast.makeText(
                                    activity, "Güncelleme Başarılı",
                                    Toast.LENGTH_SHORT
                                ).show()


                                var intent =
                                    Intent(activity, ProfileActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                startActivity(intent)
                            }
                        }

                    }
                    ).addOnFailureListener(object : OnFailureListener
                    {


                        override fun onFailure(p0: Exception) {

                       //     dialogYukleniyor.dismiss()
                            Toast.makeText(activity,"Resim Yüklenemedi",
                                Toast.LENGTH_SHORT).show()


                            var intent =
                                Intent(activity, ProfileActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                            startActivity(intent)
                        }

                    })
                }



                if(guncellemeYapildi)
                {

                //    dialogYukleniyor.dismiss()
                    if(!resimdegisti) {
                        Toast.makeText(
                            activity, "Güncelleme Başarılı",
                            Toast.LENGTH_SHORT
                        ).show()



                        var intent =
                            Intent(activity, ProfileActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                        startActivity(intent)
                    }

                }
                else
                {


                       dialogYukleniyor.dismiss()
                        Toast.makeText(
                            activity, "Herhangi Bir Değişiklik Algılanmadı",
                            Toast.LENGTH_SHORT
                        ).show()



                }

            }
            else
            {
                mDatabaseReferance.child("users").orderByChild("user_name").addListenerForSingleValueEvent(object : ValueEventListener{
                    override fun onCancelled(p0: DatabaseError?) {

                    }

                    override fun onDataChange(p0: DataSnapshot?) {




                        for(ds in p0!!.children)
                        {
                            var gelenKullaniciAdi = ds!!.getValue(Users::class.java)!!.user_name

                            if(gelenKullaniciAdi!!.equals(view.ProfilDuzenleEditTextKullaniciAdi.text.toString()))
                            {
                   //             dialogYukleniyor.dismiss()
                                kullanimda = true
                                Toast.makeText(activity,"Kullanıcı Adı Daha Önceden Başkası Tarafından Alınmış",
                                    Toast.LENGTH_SHORT).show()

                                    dialogYukleniyor.dismiss()


                                break

                            }



                        }


                        if(!kullanimda)
                        {

                            mDatabaseReferance.child("users").child(gelenKullaniciBilgileri!!.user_id).child("user_name")
                                .setValue(view.ProfilDuzenleEditTextKullaniciAdi.text.toString())


                            if(!gelenKullaniciBilgileri!!.name_surname!!.equals(view.ProfilDuzenleEditTextAd.text.toString()))
                            {
                                mDatabaseReferance.child("users").child(gelenKullaniciBilgileri!!.user_id).child("name_surname")
                                    .setValue(view.ProfilDuzenleEditTextAd.text.toString())
                                guncellemeYapildi = true
                            }
                            if(!gelenKullaniciBilgileri!!.user_detail!!.aciklama!!.equals(view.ProfilDuzenleEditTextAciklama.text.toString()))
                            {
                                mDatabaseReferance.child("users").child(gelenKullaniciBilgileri!!.user_id).child("user_detail").child("aciklama")
                                    .setValue(view.ProfilDuzenleEditTextAciklama.text.toString())
                                guncellemeYapildi = true

                            }

                            if(!gelenKullaniciBilgileri!!.user_detail!!.webSitesi!!.equals(view.ProfilDuzenleEditTextInternetSitesi.text.toString()))
                            {
                                mDatabaseReferance.child("users").child(gelenKullaniciBilgileri!!.user_id).child("user_detail").child("webSitesi")
                                    .setValue(view.ProfilDuzenleEditTextInternetSitesi.text.toString())
                                guncellemeYapildi = true

                            }


                            if(profilePhotoURI!=null)
                            {

                                guncellemeYapildi = true
                                resimdegisti = true

                                var uploadTask = mStorageReference.child("users").child(gelenKullaniciBilgileri!!.user_id!!).child(gelenKullaniciBilgileri!!.user_id!!+"_profile_photo").
                                    putFile(profilePhotoURI!!).addOnCompleteListener(object  : OnCompleteListener<UploadTask.TaskSnapshot>
                                {
                                    override fun onComplete(p0: Task<UploadTask.TaskSnapshot>) {
                                        if(p0!!.isSuccessful)
                                        {

                          //                  dialogYukleniyor.dismiss()
                                            Toast.makeText(
                                                activity, "Güncelleme Başarılı",
                                                Toast.LENGTH_SHORT
                                            ).show()

                                            var intent =
                                                Intent(activity, ProfileActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                            startActivity(intent)
                                        }
                                    }

                                }
                                ).addOnFailureListener(object : OnFailureListener
                                {
                                    override fun onFailure(p0: Exception) {

                             //           dialogYukleniyor.dismiss()

                                        Toast.makeText(activity,"Resim Yüklenemedi",
                                            Toast.LENGTH_SHORT).show()

                                        var intent =
                                            Intent(activity, ProfileActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                        startActivity(intent)
                                    }

                                })
                            }


                        }


                        if(guncellemeYapildi)
                        {
                            if(!resimdegisti) {

                        //        dialogYukleniyor.dismiss()
                                Toast.makeText(
                                    activity, "Güncelleme Başarılı",
                                    Toast.LENGTH_SHORT
                                ).show()

                                var intent = Intent(
                                    activity,
                                    ProfileActivity::class.java
                                ).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                startActivity(intent)
                            }

                        }
                        else
                        {
                            if(!kullanimda) {

                       //         dialogYukleniyor.dismiss()
                                Toast.makeText(
                                    activity, "Herhangi Bir Değişiklik Algılanmadı",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }



                    }
                })
            }





        }




        return view
    }

    private fun FotografDegistir() {

        var intent = Intent()
        intent.setType("image/*")
        intent.setAction(Intent.ACTION_PICK)
        startActivityForResult(intent,RESIM_SEC)



    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if(requestCode == RESIM_SEC && resultCode == AppCompatActivity.RESULT_OK && data!!.data!= null)
        {

            profilePhotoURI = data!!.data

            ProfileEditCircleProfileImage.setImageURI(profilePhotoURI)


        }






    }

    private fun setupKullaniciBilgileri(view: View?) {
     view!!.ProfilDuzenleEditTextAd.setText(gelenKullaniciBilgileri.name_surname)
     view!!.ProfilDuzenleEditTextKullaniciAdi.setText(gelenKullaniciBilgileri.user_name)
        if(!gelenKullaniciBilgileri.user_detail!!.aciklama.isNullOrEmpty())
     view!!.ProfilDuzenleEditTextAciklama.setText(gelenKullaniciBilgileri.user_detail!!.aciklama)
        if(!gelenKullaniciBilgileri.user_detail!!.webSitesi.isNullOrEmpty())
        view!!.ProfilDuzenleEditTextInternetSitesi.setText(gelenKullaniciBilgileri.user_detail!!.webSitesi)


        var imgURL = gelenKullaniciBilgileri!!.user_detail!!.profilResmi
        UniversalImageLoader.setImage(imgURL!!,view!!.ProfileEditCircleProfileImage,null,"")
    }





    @Subscribe(sticky = true) //eventbustan poststicky ile bişey gönderilmişse yakala
    internal fun onKullaniciBilgileriEvent(kullaniciBilgileri : EventbusDataEvents.KullaniciBilgileriniGonder)//benim oluşturduğum classdan geldi
    {


        gelenKullaniciBilgileri = kullaniciBilgileri!!.kullanici!!

    }






    override fun onAttach(context: Context?) {
        super.onAttach(context)
        EventBus.getDefault().register(this)
    }

    override fun onDetach() {
        super.onDetach()
        EventBus.getDefault().unregister(this)

    }
}
