package com.besoci.sausosyal.besoci.Utils

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class HomePagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {
// ( ) içinde yazılanlar aslında kurucuya gelenler gibi düşün : sonra da babasına gidiyo

    private var mFragmentList : ArrayList<Fragment> = ArrayList() //fragment listemizi tutacak


    override fun getItem(position: Int): Fragment {
        return mFragmentList.get(position)
    }

    override fun getCount(): Int {

     return mFragmentList.size
    }


    fun addFragment( fragment: Fragment)
    {

        mFragmentList.add(fragment)  //gelen fragmentı listeye ekliyor
    }



}