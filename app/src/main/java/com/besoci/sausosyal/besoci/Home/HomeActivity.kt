package com.besoci.sausosyal.besoci.Home

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.View
import android.widget.ImageView
import com.besoci.sausosyal.besoci.Login.LoginActivity
import com.besoci.sausosyal.besoci.R
import com.besoci.sausosyal.besoci.Session.Session
import com.besoci.sausosyal.besoci.Utils.BottomNavigationViewHelper
import com.besoci.sausosyal.besoci.Utils.HomePagerAdapter
import com.besoci.sausosyal.besoci.Utils.SharePagerAdapter
import com.besoci.sausosyal.besoci.Utils.UniversalImageLoader
import com.besoci.sausosyal.besoci.Weather.WeatherActivity
import com.google.firebase.auth.FirebaseAuth
import com.nostra13.universalimageloader.core.ImageLoader
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_home.*


class HomeActivity : AppCompatActivity() {

    private val ACTIVITY_NO = 0;
    private val TAG = "Home"

    var session: Session? = null

    lateinit var mAuth : FirebaseAuth
    lateinit var mAuthListener : FirebaseAuth.AuthStateListener


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        session = com.besoci.sausosyal.besoci.Session.Session(this!!)
        session!!.girisDurumu=true

       // setupAuthListener()
        mAuth=FirebaseAuth.getInstance()

        initImageLoader()
        setupHomeViewPager(0) //Home daki Viewpagerı ata yani 3 ksıımdan oluşan var ya kaydırmalı olan kısım






     /*   val button = findViewById<ImageView>(R.id.imageViewTopWeather)
        button.setOnClickListener {
            val intent =Intent(this,WeatherActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            this.startActivity(intent)
        }
*/




    }

    override fun onResume() {
       // setupNavigationView()   //navigationviewi ata (Alt Menu)
        super.onResume()

    }

    private fun initImageLoader() {

        var universalImageLoader= UniversalImageLoader(this)
        ImageLoader.getInstance().init(universalImageLoader.config)

    }

    override fun onBackPressed() {
        ViewPagerHome.visibility = View.VISIBLE
        homeFragmentContainer.visibility = View.GONE
        if(ViewPagerHome.currentItem==0) {

            super.onBackPressed()
        }
        else
        {
            ViewPagerHome.setCurrentItem(0)
        }
    }

    private fun setupHomeViewPager(a:Int) {
        var tabAdlar = ArrayList<String>()
      //  tabAdlar.add("WEATHER")
        tabAdlar.add("HOME")
     //   tabAdlar.add("MESSAGES")


        var homePagerAdapter = SharePagerAdapter(supportFragmentManager,tabAdlar)
       // homePagerAdapter.addFragment(WeatherFragment()) //index 0 sıra önemli
        homePagerAdapter.addFragment(HomeFragment()) //index 1 sıra önemli
    //    homePagerAdapter.addFragment(MessagesFragment()) //index 2 sıra önemli

        ViewPagerHome.adapter = homePagerAdapter //layouttaki viewpagera oluşturduğumuz adapteri atadık
        ViewPagerHome.setCurrentItem(0) //ilk 1 numaradan yani anasayfadan başlasın


        ViewPagerHome.offscreenPageLimit = a


        ////////////////////////////////////////////////////////////////////////////////////////////
        //FRAGMENTLERDE AÇILANI AKTİF ETME
        //sharepager adaptere bak

       // homePagerAdapter.fragmentiPagerdanSil(ViewPagerHome,0)
  //      homePagerAdapter.fragmentiPagerdanSil(ViewPagerHome,1)


       /*
        ViewPagerHome.addOnPageChangeListener(object : ViewPager.OnPageChangeListener
        {
            override fun onPageScrollStateChanged(pos: Int) {


            }

            override fun onPageScrolled(pos: Int, p1: Float, p2: Int) {

            }

            override fun onPageSelected(pos: Int) {

                if(pos == 0) //galeri
                {
                    homePagerAdapter.fragmentiPagerdanSil(ViewPagerHome,1)
              //      homePagerAdapter.fragmentiPagerdanSil(ViewPagerHome,2)
                   homePagerAdapter.fragmentiPageraEkle(ViewPagerHome,0)

                }
                else if(pos == 1) //fotocek
                {
                    homePagerAdapter.fragmentiPagerdanSil(ViewPagerHome,0)
                //    homePagerAdapter.fragmentiPagerdanSil(ViewPagerHome,2)
                    homePagerAdapter.fragmentiPageraEkle(ViewPagerHome,1)
                }
             //   else if(pos == 2) //videocek
             //   {
              //      homePagerAdapter.fragmentiPagerdanSil(ViewPagerHome,0)
             //       homePagerAdapter.fragmentiPagerdanSil(ViewPagerHome,1)
               //     homePagerAdapter.fragmentiPageraEkle(ViewPagerHome,2)
               // }
            }

        }
        */

      //  )
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////






    }
   /* private fun setupAuthListener() {
        mAuthListener = object : FirebaseAuth.AuthStateListener{
            override fun onAuthStateChanged(p0: FirebaseAuth) {

                var user = FirebaseAuth.getInstance().currentUser

                if(user == null) //giriş yapmadıysa
                {
                    var intent = Intent(this@HomeActivity,LoginActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    finish()
                }
            }
        }
    }*/
/*    override fun onStart() {
        super.onStart()
        mAuth.addAuthStateListener(mAuthListener)
    }*/

    /*override fun onStop() {
        super.onStop()
        if(mAuthListener != null)
        {
            mAuth.removeAuthStateListener(mAuthListener)
        }
    }*/
}
