package com.besoci.sausosyal.besoci.Share


import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast

import com.besoci.sausosyal.besoci.R
import com.besoci.sausosyal.besoci.Utils.*
import kotlinx.android.synthetic.main.activity_share.*
import kotlinx.android.synthetic.main.fragment_share_gallery.*
import kotlinx.android.synthetic.main.fragment_share_gallery.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.io.File

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class ShareGalleryFragment : Fragment() {

    var secilenResimYolu :String?= null
    var dosyaTuruResim :Boolean?= null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        var view = inflater.inflate(R.layout.fragment_share_gallery, container, false)

        var klasorler = ArrayList<String>()
        var klasorAdlari = ArrayList<String>()


        var aDirArray : Array<out File> = ContextCompat.getExternalFilesDirs(this!!.context!!, null);
        var rootphonestorage : String = Environment.getExternalStorageDirectory().path


        if(aDirArray.size > 1)
        { var rootsdcardstorage = File(aDirArray[1], Environment.DIRECTORY_DCIM)
            var sdcardyoludizisi = rootsdcardstorage.toString().split("/")
            var sdkartyol = sdcardyoludizisi[0]+"/"+sdcardyoludizisi[1]+"/"+sdcardyoludizisi[2]
            var sdkart : String = sdkartyol + "/DCIM/Camera"
            klasorler.add(sdkart)
            klasorAdlari.add("SD Kart")
        }




        var kameraResimleri : String = rootphonestorage  + "/DCIM/Camera"
        var indirilenResimler : String = rootphonestorage  + "/Download"
        var whatsappResimleri : String = rootphonestorage  + "/WhatsApp/Media/WhatsApp Images"
        var ekranresimleri : String = rootphonestorage  + "/PICTURES/Screenshots"


        if(DosyaIslemleri.klasorMevcutMu(kameraResimleri))
        {
            klasorler.add(kameraResimleri)
            klasorAdlari.add("Kamera")

        }
        if(DosyaIslemleri.klasorMevcutMu(indirilenResimler))
        {
            klasorler.add(indirilenResimler)
            klasorAdlari.add("İndirilenler")

        }

        if(DosyaIslemleri.klasorMevcutMu(whatsappResimleri)) {
            klasorler.add(whatsappResimleri)
            klasorAdlari.add("Whatsapp")
        }
        if(DosyaIslemleri.klasorMevcutMu(ekranresimleri)) {
            klasorler.add(ekranresimleri)
            klasorAdlari.add("Ekran Alintilari")
        }

        var spinnerArrayAdapter :ArrayAdapter<String> = ArrayAdapter(activity , android.R.layout.simple_spinner_item , klasorAdlari)
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        view.fragmentShareSpinnerKlasorAdlari.adapter = spinnerArrayAdapter
        view.fragmentShareSpinnerKlasorAdlari.setSelection(0) //ilk baştakini seç açılışta



        view.fragmentShareSpinnerKlasorAdlari.onItemSelectedListener = object  : AdapterView.OnItemSelectedListener
        {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {


          //   setupGridView(DosyaIslemleri.KlasordekiDosyalariGetir(klasorler.get(position)))

                setupRecyclerView(DosyaIslemleri.KlasordekiDosyalariGetir(klasorler.get(position)))


            }
              /*
            fun setupGridView(secilenKlasordekiDosyalar : ArrayList<String>) {

                var gridAdapter =
                    ShareActivityGridViewAdapter(activity!!, R.layout.tek_sutun_grid_resim, secilenKlasordekiDosyalar)
                recyclerViewDosyalar.adapter = gridAdapter

                if (secilenKlasordekiDosyalar.size <= 0)
                    Toast.makeText(context, "İçerik Bulunamadı", Toast.LENGTH_SHORT).show()
                else
                {
                //açılışta ilk dosya gelmesi için
                    secilenResimYolu = secilenKlasordekiDosyalar.get(0)
                    resimVeyaVieoGoster(secilenResimYolu!!)

                recyclerViewDosyalar.setOnItemClickListener(object : AdapterView.OnItemClickListener {
                    override fun onItemClick(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {

                        secilenResimYolu=secilenKlasordekiDosyalar.get(position)
                        resimVeyaVieoGoster(secilenResimYolu!!)


                    }
                })
            }
            }
*/

        }




        view.fragmentShareTextViewIleri.setOnClickListener {
            //fragmentChange(ShareIleriFragment(),"ShareIleriFragmentEklendi")
            activity!!.anaLayout.visibility=View.GONE //şimdiki viewi yani profil düzenleme ana ekranını kapat
            activity!!.fragmentContainerLayout.visibility = View.VISIBLE
            var transaction=activity!!.supportFragmentManager.beginTransaction() //fragmentbaşlat

            if(dosyaTuruResim == true)
            {
                var bitmap = imgCropView.croppedImage
                if(bitmap!= null) {
                    var croppedImagePath = DosyaIslemleri.croppedImageSave(bitmap)
                    EventBus.getDefault().postSticky(EventbusDataEvents.PaylasilacakResmiGonder(croppedImagePath , dosyaTuruResim))
                    videoView.stopPlayback() //arkada video oynamaya devam etmesin

                    transaction.replace(R.id.fragmentContainerLayout,ShareIleriFragment()) //gelenFragmenti koy
                    transaction.addToBackStack("ShareIleriFragmentEklendi")
                    transaction.commit()

                }
            }



        }


        view.fragmentShareImageViewCancel.setOnClickListener {
            activity!!.onBackPressed()
        }



        return view
    }

    private fun setupRecyclerView(klasordekiDosyalar: ArrayList<String>) {


        var recyclerViewAdapter=GalleryRecyclerViewAdapter(klasordekiDosyalar,this.activity!!)
        recyclerViewDosyalar.adapter = recyclerViewAdapter

        var layoutManager = GridLayoutManager(this.activity , 3) //3 sütun tanımladık
        recyclerViewDosyalar.layoutManager=layoutManager!!


        if (klasordekiDosyalar.size <= 0)
            Toast.makeText(context, "İçerik Bulunamadı", Toast.LENGTH_SHORT).show()
        else
        {
            //açılışta ilk dosya gelmesi için
            secilenResimYolu = klasordekiDosyalar.get(0)
            resimVeyaVieoGoster(secilenResimYolu!!)

            //hızlandırmak için kalite düşürmek
            recyclerViewDosyalar.setHasFixedSize(true)
            recyclerViewDosyalar.setItemViewCacheSize(10)
            recyclerViewDosyalar.setDrawingCacheEnabled(true)
            recyclerViewDosyalar.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);

            //////////

            //CLICK OLAYI ADAPTERINDE GERÇEKLEŞİYOR  GalleryRecyclerViewAdapter ' a BAK

            /*recyclerViewDosyalar.setOnItemClickListener(object : AdapterView.OnItemClickListener {
                override fun onItemClick(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {

                    secilenResimYolu=secilenKlasordekiDosyalar.get(position)
                    resimVeyaVieoGoster(secilenResimYolu!!)


                }
            })*/
        }







    }

    private fun fragmentChange(gelenFragment : Fragment, gelenKey:String)  {
        activity!!.anaLayout.visibility=View.GONE //şimdiki viewi yani profil düzenleme ana ekranını kapat
        activity!!.fragmentContainerLayout.visibility = View.VISIBLE
        var transaction=activity!!.supportFragmentManager.beginTransaction() //fragmentbaşlat


        EventBus.getDefault().postSticky(EventbusDataEvents.PaylasilacakResmiGonder(secilenResimYolu , dosyaTuruResim))
        videoView.stopPlayback() //arkada video oynamaya devam etmesin

        transaction.replace(R.id.fragmentContainerLayout,gelenFragment) //gelenFragmenti koy
        transaction.addToBackStack(gelenKey)
        transaction.commit()




    }


    private fun resimVeyaVieoGoster(dosyaYolu: String) {

        if( dosyaYolu.contains(".")) {
            var dosyaTuru: String = dosyaYolu.substring(dosyaYolu.lastIndexOf("."))

            if (dosyaTuru != null) {
                if (dosyaTuru.equals(".mp4"))
              {
               /*
                    dosyaTuruResim = false
                    videoView.visibility = View.VISIBLE
                    imgCropView.visibility = View.GONE
                    videoView.setVideoURI(Uri.parse("file://"+dosyaYolu))
                    videoView.start()
                  */
                }


                else
                {
                    dosyaTuruResim = true
                    videoView.visibility = View.GONE
                    imgCropView.visibility = View.VISIBLE
                    UniversalImageLoader.setImage(dosyaYolu,imgCropView,null,"file://")
                }

            }
        }
    }

    @Subscribe//eventbustan bişey gönderilmişse yakala
    internal fun onSecilenResimEvent(secilenResim : EventbusDataEvents.GallerySecilenDosyaYoluGonder)//benim oluşturduğum classdan geldi
    {


        secilenResimYolu = secilenResim!!.dosyaYolu!! //secilendosyayolu
//eventbus her veri yolladık.ça resmi değiştir
        resimVeyaVieoGoster(secilenResimYolu!!)
    }



    override fun onAttach(context: Context?) {
        super.onAttach(context)
        EventBus.getDefault().register(this)
    }

    override fun onDetach() {
        super.onDetach()
        EventBus.getDefault().unregister(this)

    }

}
