package com.besoci.sausosyal.besoci.Share


import android.os.Bundle
import android.os.Environment
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.besoci.sausosyal.besoci.R
import com.besoci.sausosyal.besoci.Utils.EventbusDataEvents
import com.otaliastudios.cameraview.*
import kotlinx.android.synthetic.main.activity_share.*
import kotlinx.android.synthetic.main.fragment_share_camera.*
import kotlinx.android.synthetic.main.fragment_share_camera.view.*
import org.greenrobot.eventbus.EventBus
import java.io.File
import java.io.FileOutputStream

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class ShareCameraFragment : Fragment() {

     var cameraview : CameraView? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view =  inflater.inflate(R.layout.fragment_share_camera, container, false)
        cameraview=view.cameraView
        cameraview!!.mapGesture(Gesture.PINCH,GestureAction.ZOOM)
        cameraview!!.mapGesture(Gesture.TAP,GestureAction.FOCUS_WITH_MARKER)

        view.imgfotocek.setOnClickListener {
            cameraview!!.capturePicture() //resim çek ama bu resim çekilinde listenera çektim diyo
        }

        view.imgCameraSwitch.setOnClickListener {
            if(cameraview!!.facing == Facing.BACK)
            {
                cameraview!!.facing = Facing.FRONT
            }
            else
            {
                cameraview!!.facing = Facing.BACK

            }

        }


        view.imgFlash.setOnClickListener {




            if(cameraview!!.flash == Flash.OFF)
            {
                cameraview!!.flash = Flash.ON
            imgFlash.setImageResource(R.drawable.ic_flashon)
            }
            else if(cameraview!!.flash == Flash.ON)
            {
                cameraview!!.flash = Flash.AUTO
                imgFlash.setImageResource(R.drawable.ic_flashauto)
            }
            else if(cameraview!!.flash == Flash.AUTO)
            {
                cameraview!!.flash = Flash.OFF
                imgFlash.setImageResource(R.drawable.ic_flashoff)
            }
        }


        cameraview!!.addCameraListener(object : CameraListener()
        {
            override fun onPictureTaken(jpeg: ByteArray?) {
                super.onPictureTaken(jpeg)
                var cekilenFotoAdi : Long = System.currentTimeMillis()
                var cekilenFoto = File(Environment.getExternalStorageDirectory().absolutePath+"/DCIM/Camera/"+cekilenFotoAdi+".jpg")
                var dosyaOlustur = FileOutputStream(cekilenFoto)
                dosyaOlustur.write(jpeg)
                dosyaOlustur.close()

                activity!!.anaLayout.visibility=View.GONE
                activity!!.fragmentContainerLayout.visibility = View.VISIBLE

                var transaction = activity!!.supportFragmentManager.beginTransaction()

                EventBus.getDefault().postSticky(EventbusDataEvents.PaylasilacakResmiGonder(cekilenFoto.absolutePath.toString(),true))
                transaction.replace(R.id.fragmentContainerLayout,ShareIleriFragment())
                transaction.addToBackStack("shareilerifragmenteklendi")
                transaction.commit()



            }
        }
        )

        view.fragmentShareImageViewCancel.setOnClickListener {
            activity!!.onBackPressed()
        }


        return view
    }

    override fun onResume() {
        super.onResume()
        cameraview!!.start()

    }

    override fun onPause() {
        super.onPause()
        cameraview!!.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        if(cameraview != null)
        cameraview!!.destroy()
    }
}
