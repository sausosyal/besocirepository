package com.besoci.sausosyal.besoci.News

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.besoci.sausosyal.besoci.R
import com.besoci.sausosyal.besoci.Utils.BottomNavigationViewHelper
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_news.*

class NewsActivity : AppCompatActivity() {

    val IC_KAPALI_ALAN ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fkapalialan.png?alt=media&token=7719ec94-30f3-4ba6-9560-a89805fb018e"
    val IC_ACIK_ALAN ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Facikalan.png?alt=media&token=dac457dc-4372-4994-b558-d27c40d7a957"
    val IC_YEMEK ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fyiyecek.png?alt=media&token=6eecb1dc-76fc-4640-ba51-d01df723cdb5"
    val IC_ALISVERIS ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Falisveris.png?alt=media&token=8ec792c2-a5bd-4304-be52-33e9594aa438"
    val IC_SPOR ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fspor.png?alt=media&token=3fff50e7-ff98-4fbd-a704-fc60455fb1b3"
    val IC_PIKNIK ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fpiknik.png?alt=media&token=18524be4-669d-4d79-ad68-cb076a7efb82"
    val IC_KULTURSANAT ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fkultursanat.png?alt=media&token=ba5853ea-dd0a-48ea-918a-06edd6ac09bf"
    val IC_AILE="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Ffamily.png?alt=media&token=a7b16446-ccbe-44e2-83da-5ed07e3e0dff"
    val IC_YETISKIN="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fparents.png?alt=media&token=9224a382-d841-4701-ba72-6fce74669236"
    val IC_COCUK="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fchildren.png?alt=media&token=98679575-c6ae-4d41-a0b7-71ded6ed5ecd"
    val IC_YALNIZ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Falone.png?alt=media&token=97809c1e-7f79-44aa-846d-85b33599baca"
    val IC_SEVGILI ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Frelationship.png?alt=media&token=c73f8771-3895-4a5b-8c6c-dc64bbbfdcd6"
    val IC_OGRENCI ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fstudents.png?alt=media&token=989ca8a4-740d-4fd6-bda3-ff51d91e6c3d"
    val IC_DIGER = "https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Finformation.png?alt=media&token=0d8465af-c206-46aa-b3ef-4a1ed1f0a822"





    private val ACTIVITY_NO = 3;
    private val TAG = "News"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news) //değişecek
       setupNavigationView()

        Picasso.with(this@NewsActivity).load(IC_ACIK_ALAN).into(imgAcikAlan)
        Picasso.with(this@NewsActivity).load(IC_KAPALI_ALAN).into(imgKapaliAlan)
        Picasso.with(this@NewsActivity).load(IC_YEMEK).into(imgYemek)
        Picasso.with(this@NewsActivity).load(IC_ALISVERIS).into(imgAlisveris)
        Picasso.with(this@NewsActivity).load(IC_SPOR).into(imgSpor)
        Picasso.with(this@NewsActivity).load(IC_PIKNIK).into(imgPiknik)
        Picasso.with(this@NewsActivity).load(IC_KULTURSANAT).into(imgKultursanat)
        Picasso.with(this@NewsActivity).load(IC_DIGER).into(imgDiger)
        Picasso.with(this@NewsActivity).load(IC_SEVGILI).into(imgSevgili)
        Picasso.with(this@NewsActivity).load(IC_OGRENCI).into(imgOgrenci)
        Picasso.with(this@NewsActivity).load(IC_AILE).into(imgAile)
        Picasso.with(this@NewsActivity).load(IC_YETISKIN).into(imgYetiskin)
        Picasso.with(this@NewsActivity).load(IC_COCUK).into(imgCocuk)
        Picasso.with(this@NewsActivity).load(IC_YALNIZ).into(imgYalniz)



        ButtonDetayliAra.setOnClickListener {

            if
                    (
                checkBoxAcikAlan.isChecked == false &&
                checkBoxKapaliAlan.isChecked == false
                    )
            {

                Toast.makeText(this,"Lutfen en az bir mekan tipi seciniz",Toast.LENGTH_SHORT).show()



            }

            else
            {

                if
                        (
                    checkBoxYemek.isChecked == false &&
                    checkBoxAlisveris.isChecked == false &&
                    checkBoxSpor.isChecked == false &&
                    checkBoxPiknik.isChecked == false &&
                    checkBoxKultursanat.isChecked == false &&
                    checkBoxDiger.isChecked == false


                )
                {

                    Toast.makeText(this,"Lutfen en az bir kategori seciniz",Toast.LENGTH_SHORT).show()



                }
                else
                {

                    if
                            (
                        checkBoxSevgili.isChecked == false &&
                        checkBoxOgrenci.isChecked == false &&
                        checkBoxAile.isChecked == false &&
                        checkBoxYetiskin.isChecked == false &&
                        checkBoxCocuk.isChecked == false &&
                        checkBoxYalniz.isChecked == false

                    )
                    {

                        Toast.makeText(this,"Lutfen en az bir kisi tipi seciniz",Toast.LENGTH_SHORT).show()



                    }
                    else
                    {

                        val intent = Intent(this, ArananActivity::class.java)
                        intent.putExtra("e_acikalan", checkBoxAcikAlan.isChecked)
                        intent.putExtra("e_aile", checkBoxAile.isChecked)
                        intent.putExtra("e_alisveris", checkBoxAlisveris.isChecked)
                        intent.putExtra("e_cocuk", checkBoxCocuk.isChecked)
                        intent.putExtra("e_diger", checkBoxDiger.isChecked)
                        intent.putExtra("e_kapalialan", checkBoxKapaliAlan.isChecked)
                        intent.putExtra("e_kultursanat", checkBoxKultursanat.isChecked)
                        intent.putExtra("e_ogrenci", checkBoxOgrenci.isChecked)
                        intent.putExtra("e_piknik", checkBoxPiknik.isChecked)
                        intent.putExtra("e_sevgili", checkBoxSevgili.isChecked)
                        intent.putExtra("e_spor", checkBoxSpor.isChecked)
                        intent.putExtra("e_yalniz", checkBoxYalniz.isChecked)
                        intent.putExtra("e_yemek", checkBoxYemek.isChecked)
                        intent.putExtra("e_yetiskin", checkBoxYetiskin.isChecked)
                        startActivity(intent)

                    }

                }
            }








        }

        tumunusec.setOnClickListener {


            if(tumunusec.isChecked) {
                tumunusec.setText("Tumunu Kaldir")
                checkBoxAcikAlan.isChecked = true
                checkBoxAile.isChecked = true
                checkBoxAlisveris.isChecked = true
                checkBoxCocuk.isChecked = true
                checkBoxDiger.isChecked = true
                checkBoxKapaliAlan.isChecked = true
                checkBoxKultursanat.isChecked = true
                checkBoxOgrenci.isChecked = true
                checkBoxPiknik.isChecked = true
                checkBoxSevgili.isChecked = true
                checkBoxSpor.isChecked = true
                checkBoxYalniz.isChecked = true
                checkBoxYemek.isChecked = true
                checkBoxYetiskin.isChecked = true
            }
            else
            {
                tumunusec.setText("Tumunu Sec")

                checkBoxAcikAlan.isChecked = false
                checkBoxAile.isChecked = false
                checkBoxAlisveris.isChecked = false
                checkBoxCocuk.isChecked = false
                checkBoxDiger.isChecked = false
                checkBoxKapaliAlan.isChecked = false
                checkBoxKultursanat.isChecked = false
                checkBoxOgrenci.isChecked = false
                checkBoxPiknik.isChecked = false
                checkBoxSevgili.isChecked = false
                checkBoxSpor.isChecked = false
                checkBoxYalniz.isChecked = false
                checkBoxYemek.isChecked = false
                checkBoxYetiskin.isChecked = false
            }


        }



    }


    fun setupNavigationView()
    {
        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewim)
        BottomNavigationViewHelper.setupNavigation(this,bottomNavigationViewim)
        var menu = bottomNavigationViewim.menu
        var menuitem = menu.getItem(ACTIVITY_NO)
        menuitem.setChecked(true)
    }
}
