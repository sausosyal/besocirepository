package com.besoci.sausosyal.besoci.Generic

import android.content.Intent
import android.graphics.PorterDuff
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.besoci.sausosyal.besoci.Models.Posts
import com.besoci.sausosyal.besoci.Models.UserPosts
import com.besoci.sausosyal.besoci.Models.Users
import com.besoci.sausosyal.besoci.Profile.ProfileEditFragment
import com.besoci.sausosyal.besoci.Profile.YukleniyorFragment
import com.besoci.sausosyal.besoci.R
import com.besoci.sausosyal.besoci.Utils.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_user_profile.*
import org.greenrobot.eventbus.EventBus

class UserProfileActivity : AppCompatActivity() {


    private val ACTIVITY_NO = 4;
    private val TAG = "Profile"
    lateinit var tumGonderiler : ArrayList<UserPosts>
    lateinit var mAuth : FirebaseAuth
    lateinit var mAuthListener : FirebaseAuth.AuthStateListener
    lateinit var mRef : DatabaseReference
    lateinit var mUser : FirebaseUser
    lateinit var secilenUserId : String
    val IC_KISIEKLE = "https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fic_kisiekle.png?alt=media&token=c516cd73-a400-4923-b4c9-9a27092fb63c"
    val IC_KISISIL = "https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fic_kisisil.png?alt=media&token=8ff55247-7e73-4395-8cec-5cedffa87319"


    var dialogYukleniyor = YukleniyorFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile)


        //  setupAuthListener()
        mRef = FirebaseDatabase.getInstance().reference
        mAuth=FirebaseAuth.getInstance()
        mUser = mAuth!!.currentUser!!
        secilenUserId = intent.getStringExtra("secilenUserId")

        dialogYukleniyor.show(this.supportFragmentManager,"yukleniyorfragmentiEklendi")



        kullaniciBilgileriniGetir()
        tumGonderiler= ArrayList<UserPosts>()
        setupToolbar()
        kullaniciPostlariniGetir(secilenUserId)

        ProfileGridIcon.setOnClickListener {


            setupRecyclerView(1)

        }
        ProfileListIcon.setOnClickListener {

            setupRecyclerView(2)



        }

        imgBack.setOnClickListener{
            onBackPressed()
        }

        takipEtButton.setOnClickListener {

            mRef.child("following").child(mUser.uid).addListenerForSingleValueEvent(object : ValueEventListener
            {
                override fun onCancelled(p0: DatabaseError?) {

                }

                override fun onDataChange(p0: DataSnapshot?) {
                    if(p0!!.hasChild(secilenUserId))//daha onceden takip ediyorsam
                    {
                        mRef.child("following").child(mUser.uid).child(secilenUserId).removeValue()//takipten cikar
                        mRef.child("follower").child(secilenUserId).child(mUser.uid).removeValue()//ondanda beni cikar
                      //  takipEtButton.setImageResource(R.drawable.ic_kisiekle)
                        Picasso.with(this@UserProfileActivity).load(IC_KISIEKLE).into(takipEtButton)

                        takipciSayilariniGuncelle()

                    }
                    else
                    {
                        mRef.child("following").child(mUser.uid).child(secilenUserId).setValue(secilenUserId)//takipten cikar
                        mRef.child("follower").child(secilenUserId).child(mUser.uid).setValue(mUser.uid)//ondanda beni cikar
                       // takipEtButton.setImageResource(R.drawable.ic_kisisil)
                        Picasso.with(this@UserProfileActivity).load(IC_KISISIL).into(takipEtButton)

                        takipciSayilariniGuncelle()

                    }
                }

            }
            )

        }

    }

        fun takipciSayilariniGuncelle()
        {
        mRef= FirebaseDatabase.getInstance().reference
            mRef.child("following").child(mUser.uid).addListenerForSingleValueEvent(object : ValueEventListener
            {
                override fun onCancelled(p0: DatabaseError?) {

                }

                override fun onDataChange(p0: DataSnapshot?) {



                    var takipEttikleriminSayisi = p0!!.childrenCount.toString()

                    mRef.child("follower").child(secilenUserId).addListenerForSingleValueEvent(object : ValueEventListener{
                        override fun onCancelled(p0: DatabaseError?) {

                        }

                        override fun onDataChange(p0: DataSnapshot?) {
                            var takipEdenlerinSayisi = p0!!.childrenCount.toString()
                            mRef.child("users").child(mUser.uid).child("user_detail").child("takipEdilenSayisi").setValue(takipEttikleriminSayisi)
                            mRef.child("users").child(secilenUserId).child("user_detail").child("takipciSayisi").setValue(takipEdenlerinSayisi)
                        }
                    })



                }

            })
        }

    override fun onResume() {
        setupNavigationView()
        super.onResume()
    }


    private fun kullaniciBilgileriniGetir() {

        mRef.child("users").child(secilenUserId).addValueEventListener(object : ValueEventListener
        {
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(p0: DataSnapshot?) {

                if(p0!!.getValue() != null)
                {
                    var okunanKullaniciBilgileri = p0!!.getValue(Users::class.java)

                    EventBus.getDefault().postSticky(EventbusDataEvents.KullaniciBilgileriniGonder(okunanKullaniciBilgileri))

                    ProfileTextViewProfileName.setText(okunanKullaniciBilgileri!!.user_name)
                    textViewPaylasimSayisi.setText(okunanKullaniciBilgileri!!.user_detail!!.gonderiSayisi)
                    textViewTakipEdilenSayisi.setText(okunanKullaniciBilgileri!!.user_detail!!.takipEdilenSayisi)
                    textViewTakipciSayisi.setText(okunanKullaniciBilgileri!!.user_detail!!.takipciSayisi)
                    ProfileTextViewNameSurname.setText(okunanKullaniciBilgileri!!.name_surname)
                    ProfileTextViewProfileBio.setText(okunanKullaniciBilgileri!!.user_detail!!.aciklama)
                    var imgURL = okunanKullaniciBilgileri!!.user_detail!!.profilResmi
                    UniversalImageLoader.setImage(imgURL!!,ProfileEditCircleProfileImage,null,"")


                    dialogYukleniyor.dismiss()

                }

                takipBilgisiniGetir()



            }

        }
        )


    }

    private fun takipBilgisiniGetir() {
        mRef.child("following").child(mUser.uid).addListenerForSingleValueEvent(object : ValueEventListener
        {
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(p0: DataSnapshot?) {
                if(p0!!.hasChild(secilenUserId))//takip ediyorsam
                {
                 //   takipEtButton.setImageResource(R.drawable.ic_kisisil)
                    Picasso.with(this@UserProfileActivity).load(IC_KISISIL).into(takipEtButton)


                }
                else
                {
                 //   takipEtButton.setImageResource(R.drawable.ic_kisiekle)
                    Picasso.with(this@UserProfileActivity).load(IC_KISIEKLE).into(takipEtButton)

                }
            }

        })
    }



    private fun setupToolbar() {
        imageViewProfileTopSettings.setOnClickListener {
          //  var intent = Intent(this,ProfileSettingsActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
          //  startActivity(intent)


        }

        takipEtButton.setOnClickListener {

          //  fragmentChange(ProfileEditFragment(),"ProfileEditFragmentEklendi")

        }
    }

    private fun fragmentChange(gelenFragment : Fragment, gelenKey:String)  {
     //   profileRoute.visibility=View.GONE //
      //  var transaction=supportFragmentManager.beginTransaction() //fragmentbalat
      //  transaction.replace(R.id.profileContainer,gelenFragment) //gelenFragmenti koy
      //  transaction.addToBackStack(gelenKey)
     //   transaction.commit()
    }


    fun setupNavigationView()
    {
        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewim)
        BottomNavigationViewHelper.setupNavigation(this,bottomNavigationViewim)
        var menu = bottomNavigationViewim.menu
        var menuitem = menu.getItem(ACTIVITY_NO)
        menuitem.setChecked(true)
    }



    private fun kullaniciPostlariniGetir(kullaniciID : String) {



        mRef.child("users").child(kullaniciID).addListenerForSingleValueEvent(object : ValueEventListener
        {
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(p0: DataSnapshot?) {
                var userID = kullaniciID
                var kullaniciAdi = p0!!.getValue(Users::class.java)!!.user_name
                var kullaniciFotoURL = p0!!.getValue(Users::class.java)!!.user_detail!!.profilResmi!!
                mRef.child("posts").child(kullaniciID).addListenerForSingleValueEvent(object : ValueEventListener
                {
                    override fun onCancelled(p0: DatabaseError?) {

                    }

                    override fun onDataChange(p0: DataSnapshot?) {
                        if(p0!!.hasChildren())
                        {
                            for(ds in p0!!.children)
                            {
                                var eklenecekUserPosts = UserPosts()
                                eklenecekUserPosts.UserID = userID
                                eklenecekUserPosts.UserName = kullaniciAdi
                                eklenecekUserPosts.UserPhotoURL = kullaniciFotoURL
                                eklenecekUserPosts.postID = ds.getValue(Posts::class.java)!!.post_id
                                eklenecekUserPosts.postURL = ds.getValue(Posts::class.java)!!.photo_url
                                eklenecekUserPosts.postAciklama = ds.getValue(Posts::class.java)!!.aciklama
                                eklenecekUserPosts.postYuklenmeTarih = ds.getValue(Posts::class.java)!!.yuklenme_tarihi
                                tumGonderiler.add(eklenecekUserPosts)
                            }
                        }

                        setupRecyclerView(2)
                        setupRecyclerView(1)

                    }

                })


            }

        }
        )

    }

    //indis 1 ise gird 2 ise listviewseklinde gosterilir
    private fun setupRecyclerView(indis : Int) {

        if(indis==2)
        {
            ProfileGridIcon.setColorFilter(ContextCompat.getColor(this,R.color.colorSiyah), PorterDuff.Mode.SRC_IN)
            ProfileListIcon.setColorFilter(ContextCompat.getColor(this,R.color.colorIcons),PorterDuff.Mode.SRC_IN)
            var kullaniciPostListe =ProfileRecyclerView
            kullaniciPostListe.adapter = ProfilePostListRecyclerAdapter(this,tumGonderiler)
            kullaniciPostListe.layoutManager= LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)
        }
        else
        {
            ProfileGridIcon.setColorFilter(ContextCompat.getColor(this,R.color.colorIcons),PorterDuff.Mode.SRC_IN)
            ProfileListIcon.setColorFilter(ContextCompat.getColor(this,R.color.colorSiyah),PorterDuff.Mode.SRC_IN)
            var kullaniciPostListe =ProfileRecyclerView
            kullaniciPostListe.adapter = ProfileRecyclerViewAdapter(tumGonderiler,this ,false)
            kullaniciPostListe.layoutManager= GridLayoutManager(this,3)

        }


    }


    override fun onBackPressed() {  //geri basinca
       // profileRoute.visibility = View.VISIBLE //yeniden ana  yap
        profileRoute.visibility = View.VISIBLE //yeniden ana viewi g�r�n�r yap
        profileContainer.visibility = View.INVISIBLE //SONRADAN EKLED�M
        super.onBackPressed()
    }


    /* private fun setupAuthListener() {
         mAuthListener = object : FirebaseAuth.AuthStateListener{
             override fun onAuthStateChanged(p0: FirebaseAuth) {

                 var user = FirebaseAuth.getInstance().currentUser

                 if(user == null) //gi
                 {
                     var intent = Intent(this@ProfileActivity, LoginActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                     intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                     startActivity(intent)
                     finish()
                 }
             }
         }
     }*/
    /* override fun onStart() {
         super.onStart()
         mAuth.addAuthStateListener(mAuthListener)
     }*/

    /*override fun onStop() {
        super.onStop()
        if(mAuthListener != null)
        {
            mAuth.removeAuthStateListener(mAuthListener)
        }
    }*/
}
