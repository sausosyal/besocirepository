package com.besoci.sausosyal.besoci.Utils

import android.content.Context
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.besoci.sausosyal.besoci.R
import kotlinx.android.synthetic.main.tek_sutun_grid_resim.view.*
import org.greenrobot.eventbus.EventBus

class GalleryRecyclerViewAdapter(var klasordekidosyalar : ArrayList<String> , var myContext : Context): RecyclerView.Adapter<GalleryRecyclerViewAdapter.MyViewHolder>() {

lateinit var inflater:LayoutInflater
    init {
        inflater = LayoutInflater.from(myContext)
    }

    override fun getItemCount(): Int {
return  klasordekidosyalar.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

var tekSutunDosya = inflater.inflate(R.layout.tek_sutun_grid_resim,parent,false)
        return MyViewHolder(tekSutunDosya)
    }


    override fun onBindViewHolder(viewholder: MyViewHolder, position: Int) {

        var dosyaYolu = klasordekidosyalar.get(position)
        if( dosyaYolu.contains(".")) {
            var dosyaTuru = dosyaYolu.substring(dosyaYolu.lastIndexOf("."))

            if (dosyaTuru != null) {
                if (dosyaTuru.equals(".mp4"))
                {
                /*  VIDEO KONTROL
                viewholder.videosure.visibility = View.VISIBLE
                    var retriever = MediaMetadataRetriever()
                    retriever.setDataSource(myContext, Uri.parse("file://"+dosyaYolu))
                    var videoSuresi = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
                    var videoSuresiLong = videoSuresi.toLong()

                    viewholder.videosure.setText(convertDuration(videoSuresiLong))
                    UniversalImageLoader.setImage(dosyaYolu,viewholder.dosyaResim,null,"file://")*/
                }
                else
                {
                    viewholder.videosure.visibility = View.GONE
                    UniversalImageLoader.setImage(dosyaYolu,viewholder.dosyaResim,null,"file://")

                }


viewholder.tekSutunDosya.setOnClickListener {
    //küçükresmeclickolayı olunca eventbus iler bu resmin yolunu yollar
    EventBus.getDefault().post(EventbusDataEvents.GallerySecilenDosyaYoluGonder(dosyaYolu))
}



            }
        }
    }


    class MyViewHolder(itemView:View?) : RecyclerView.ViewHolder(itemView!!) {


        var tekSutunDosya = itemView as ConstraintLayout
        var dosyaResim = tekSutunDosya.imageViewtekSutun
        var videosure = tekSutunDosya.textViewVideoSure





    }


    fun convertDuration(duration: Long): String {
        val second =duration/1000%60
        val minute:Long =duration/(1000*60)%60
        val hour =duration/(1000*60*60)%24
        var time=""
        if(hour>0)
        {
            time = String.format("%02d:%02d:%02d",hour,minute,second)
        }
        else
        {
            time = String.format("%02d:%02d",minute,second)

        }

        return time

    }
}