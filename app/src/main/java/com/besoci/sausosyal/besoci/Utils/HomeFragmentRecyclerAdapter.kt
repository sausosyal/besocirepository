package com.besoci.sausosyal.besoci.Utils

import android.content.Context
import android.content.Intent
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.besoci.sausosyal.besoci.Generic.CommentFragment
import com.besoci.sausosyal.besoci.Generic.PostGosterActivity
import com.besoci.sausosyal.besoci.Generic.UserProfileActivity
import com.besoci.sausosyal.besoci.Home.HomeActivity
import com.besoci.sausosyal.besoci.Models.UserPosts
import com.besoci.sausosyal.besoci.Profile.ProfileActivity
import com.besoci.sausosyal.besoci.R
import com.besoci.sausosyal.besoci.VideoRecyclerView.view.Video
import com.besoci.sausosyal.besoci.VideoRecyclerView.view.VideoView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.hoanganhtuan95ptit.autoplayvideorecyclerview.VideoHolder
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.tek_post_recycler_item.view.*
import org.greenrobot.eventbus.EventBus
import java.util.*
import kotlin.Comparator




//TEK POST
class HomeFragmentRecyclerAdapter(var context: Context , var tumGonderiler:ArrayList<UserPosts>): RecyclerView.Adapter<HomeFragmentRecyclerAdapter.MyViewHoler>() {

  //  init
 //   {
        //son çekilen başa gelsin için
      /*  Collections.sort(tumGonderiler,object :Comparator<UserPosts>
        {
            override fun compare(p0: UserPosts?, p1: UserPosts?): Int {
                if(p0!!.postYuklenmeTarih!! > p1!!.postYuklenmeTarih!!)
                    return -1
                else
                    return 1
            }

        }
        )*/
  //  }


    override fun getItemCount(): Int {
        return tumGonderiler.size
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHoler {
       var viewHolder = LayoutInflater.from(context).inflate(R.layout.tek_post_recycler_item,p0,false)

    return MyViewHoler(viewHolder , context)
    }



    override fun onBindViewHolder(p0: MyViewHoler, pos: Int) {

        var videoMu = false
        var dosyaYolu = tumGonderiler.get(pos).postURL
        var dosyaTuru = dosyaYolu!!.substring(dosyaYolu.lastIndexOf("."),dosyaYolu.lastIndexOf(".")+4) //.mp4

        if(dosyaTuru.equals(".mp4"))
        {
            videoMu = true
        }
        p0.setData(pos,tumGonderiler.get(pos),videoMu)


    }


    inner class MyViewHoler (itemView : View? ,myHomeActivity: Context):VideoHolder(itemView!!){

        var olusturulanElemanVideoMu = false
        val IC_IYI ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fic_guzel.png?alt=media&token=9d6be357-6590-4726-b501-61415c92dc80"
        val IC_IYI_YESIL ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fic_guzel_yesil.png?alt=media&token=a3eaeade-9cb6-4b3d-93ab-c5cbc38805c0"
        val IC_NOTR ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fic_notr.png?alt=media&token=0aef3dfd-4679-4e5c-b246-3a17d9549f78"
        val IC_KOTU ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fic_kotu.png?alt=media&token=23b7b71c-a510-4888-b0d4-224b29cdc951"
        val IC_KOTU_KIRMIZI ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fic_kotu_kirmizi.png?alt=media&token=ce10d4a0-999f-47a0-a777-e2356b367ff9"
        val IC_NOTR_SARI ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fic_notr_sari.png?alt=media&token=90757088-be11-4a48-9d8d-0fb2ac83ccd5"
        val IC_INFO ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fic_info.png?alt=media&token=e0a7bdc1-75a5-426a-9997-7dd2708827c1"
        val IC_YORUM ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fic_comment.png?alt=media&token=f5de21ac-b6f7-41ee-ad5f-154658486a95"



        override fun getVideoLayout(): View? {
            if(olusturulanElemanVideoMu)
                return myVideo
            return null
        }

        override fun playVideo() {
            if(olusturulanElemanVideoMu)
            {
                camAnimation.stop()
                //video oynamakta mı dinle
                myVideo.play { object :VideoView.OnPreparedListener
                {
                    override fun onPrepared() {
                        gonderi.visibility=View.GONE
                        camAnimation.start() //oynat bakalım :)
                    }

                }
                }
            }



        }

        override fun stopVideo() {
            if(olusturulanElemanVideoMu)
            {
                camAnimation.start()
                myVideo.stop()
            }


        }


        var tumLayout:ConstraintLayout = itemView as ConstraintLayout

        var profileImage = tumLayout.tekPostProfilResmi
        var adsoyad = tumLayout.tekPostAdSoyad
        var username = tumLayout.tekPostKullaniciAdi
        var gonderi = tumLayout.tekPostResim
        var gonderiAciklama = tumLayout.tekPostAciklama
        var gonderiZamani = tumLayout.tekPostZaman
        var yorumYap = tumLayout.tekPostYorum
        var iyi = tumLayout.tekPostIyiLogo
        var notr = tumLayout.tekPostNotrLogo
        var kotu = tumLayout.tekPostKotuLogo
        var iyi_sayi = tumLayout.tekPostIyiSayi
        var notr_sayi = tumLayout.tekPostNotrSayi
        var kotu_sayi = tumLayout.tekPostKotuSayi
        var yorum_sayi= tumLayout.tekPostYorumSayi
        var myhomeactivity = myHomeActivity
        var myVideo = tumLayout.VideoView
        var camAnimation = tumLayout.cameraAnimation
        var info = tumLayout.tekPostInfo








        fun setData(pos: Int, oankigonderi: UserPosts, videoMu: Boolean)

        {

            Picasso.with(context).load(IC_INFO).into(info)
            Picasso.with(context).load(IC_YORUM).into(yorumYap)




            olusturulanElemanVideoMu = videoMu

            if(olusturulanElemanVideoMu) //videoya
            {
                myVideo.visibility=View.VISIBLE
                gonderi.visibility=View.INVISIBLE
                myVideo.setVideo(Video(oankigonderi.postURL!!,0)) //seekTo 0.saniyeden başla dedi

            }
            else //resime
            {
                myVideo.visibility=View.GONE
                gonderi.visibility=View.VISIBLE
                UniversalImageLoader.setImage(oankigonderi.postURL!!,gonderi,null,"")

            }


            adsoyad.setText(oankigonderi.UserName)
            username.setText(oankigonderi.UserName)
            gonderiAciklama.setText(oankigonderi.postAciklama)
            UniversalImageLoader.setImage(oankigonderi.UserPhotoURL!!,profileImage,null,"")
            gonderiZamani.setText(TimeAgo.getTimeAgo(oankigonderi.postYuklenmeTarih!!))




            adsoyad.setOnClickListener {

                profilAc(oankigonderi.UserID)


            }

            profileImage.setOnClickListener {

                profilAc(oankigonderi.UserID)



            }

            gonderi.setOnClickListener {
                postAc(oankigonderi.postID ,oankigonderi.UserID)


            }

            info.setOnClickListener {
                postAc(oankigonderi.postID,oankigonderi.UserID)



            }






            yorumYap.setOnClickListener {

                EventBus.getDefault().postSticky(EventbusDataEvents.YorumYapilacakGonderininIDsiniGonder(oankigonderi!!.postID))

                (myhomeactivity as HomeActivity).ViewPagerHome.visibility=View.INVISIBLE
                (myhomeactivity as HomeActivity).homeFragmentContainer.visibility=View.VISIBLE
                fragmentChange((myhomeactivity as HomeActivity),CommentFragment(),"CommentFragmentEklendi")
            }

            
            begeniKontrol(oankigonderi)
            yorumKontrol(pos,oankigonderi)

            iyi.setOnClickListener {

                var mRef = FirebaseDatabase.getInstance().reference
                var userID = FirebaseAuth.getInstance().currentUser!!.uid
                mRef.child("onaylar").child(oankigonderi.postID).child("iyi").addListenerForSingleValueEvent(
                    object :ValueEventListener
                    {
                        override fun onCancelled(p0: DatabaseError?) {
                        }

                        override fun onDataChange(p0: DataSnapshot?) {


                            //daha önceden iyi diye oy vermişse
                            if(p0!!.hasChild(userID))
                            {
                                mRef.child("onaylar").child(oankigonderi.postID).child("iyi").child(userID).removeValue()
                               // iyi.setImageResource(R.drawable.ic_guzel)
                                Picasso.with(context).load(IC_IYI).into(iyi)
                            }
                            //daha önce iyi diye oy vermemişse
                            else
                            {
                                //iyiye ekle
                                mRef.child("onaylar").child(oankigonderi.postID).child("iyi").child(userID).setValue(userID)
                                //notr.setImageResource(R.drawable.ic_notr)
                                Picasso.with(context).load(IC_NOTR).into(notr)
                               // kotu.setImageResource(R.drawable.ic_kotu)
                                Picasso.with(context).load(IC_KOTU).into(kotu)
                               // iyi.setImageResource(R.drawable.ic_guzel_yesil)
                                Picasso.with(context).load(IC_IYI_YESIL).into(iyi)

                                //deiğerlerinde varsa çıkart
                                mRef.child("onaylar").child(oankigonderi.postID).child("notr").addListenerForSingleValueEvent(
                                    object :ValueEventListener
                                    {
                                        override fun onCancelled(p0: DatabaseError?) {
                                        }

                                        override fun onDataChange(p0: DataSnapshot?) {
                                            //daha önceden notr diye oy vermişse kaldır
                                            if(p0!!.hasChild(userID))
                                            {
                                                mRef.child("onaylar").child(oankigonderi.postID).child("notr").child(userID).removeValue()

                                            }
                                            //notr diye de vermemişse
                                            else
                                            {

                                                mRef.child("onaylar").child(oankigonderi.postID).child("kotu").addListenerForSingleValueEvent(
                                                    object :ValueEventListener {
                                                        override fun onCancelled(p0: DatabaseError?) {

                                                        }

                                                        override fun onDataChange(p0: DataSnapshot?) {
                                                            //daha önceden kotu diye oy vermişse kaldır
                                                            if(p0!!.hasChild(userID))
                                                            {
                                                                mRef.child("onaylar").child(oankigonderi.postID).child("kotu").child(userID).removeValue()

                                                            }
                                                        }

                                                    }

                                                )




                                            }
                                        }

                                    }
                                )





                            }




                        }

                    }
                )


            }



            notr.setOnClickListener {

                var mRef = FirebaseDatabase.getInstance().reference
                var userID = FirebaseAuth.getInstance().currentUser!!.uid
                mRef.child("onaylar").child(oankigonderi.postID).child("notr").addListenerForSingleValueEvent(
                    object :ValueEventListener
                    {
                        override fun onCancelled(p0: DatabaseError?) {
                        }

                        override fun onDataChange(p0: DataSnapshot?) {


                            //daha önceden notr diye oy vermişse
                            if(p0!!.hasChild(userID))
                            {
                                mRef.child("onaylar").child(oankigonderi.postID).child("notr").child(userID).removeValue()
                             //   notr.setImageResource(R.drawable.ic_notr)
                                Picasso.with(context).load(IC_NOTR).into(notr)
                            }
                            //daha önce notr diye oy vermemişse
                            else
                            {
                                //notre ekle
                                mRef.child("onaylar").child(oankigonderi.postID).child("notr").child(userID).setValue(userID)
                             //   iyi.setImageResource(R.drawable.ic_guzel)
                                Picasso.with(context).load(IC_IYI).into(iyi)
                              //  kotu.setImageResource(R.drawable.ic_kotu)
                                Picasso.with(context).load(IC_KOTU).into(kotu)
                              //  notr.setImageResource(R.drawable.ic_notr_sari)
                                Picasso.with(context).load(IC_NOTR_SARI).into(notr)

                                //deiğerlerinde varsa çıkart
                                mRef.child("onaylar").child(oankigonderi.postID).child("iyi").addListenerForSingleValueEvent(
                                    object :ValueEventListener
                                    {
                                        override fun onCancelled(p0: DatabaseError?) {
                                        }

                                        override fun onDataChange(p0: DataSnapshot?) {
                                            //daha önceden iyi diye oy vermişse kaldır
                                            if(p0!!.hasChild(userID))
                                            {
                                                mRef.child("onaylar").child(oankigonderi.postID).child("iyi").child(userID).removeValue()


                                            }
                                            //iyi diye de vermemişse
                                            else
                                            {

                                                mRef.child("onaylar").child(oankigonderi.postID).child("kotu").addListenerForSingleValueEvent(
                                                    object :ValueEventListener {
                                                        override fun onCancelled(p0: DatabaseError?) {

                                                        }

                                                        override fun onDataChange(p0: DataSnapshot?) {
                                                            //daha önceden kotu diye oy vermişse kaldır
                                                            if(p0!!.hasChild(userID))
                                                            {
                                                                mRef.child("onaylar").child(oankigonderi.postID).child("kotu").child(userID).removeValue()

                                                            }
                                                        }

                                                    }

                                                )




                                            }
                                        }

                                    }
                                )





                            }




                        }

                    }
                )


            }


            kotu.setOnClickListener {

                var mRef = FirebaseDatabase.getInstance().reference
                var userID = FirebaseAuth.getInstance().currentUser!!.uid
                mRef.child("onaylar").child(oankigonderi.postID).child("kotu").addListenerForSingleValueEvent(
                    object :ValueEventListener
                    {
                        override fun onCancelled(p0: DatabaseError?) {
                        }

                        override fun onDataChange(p0: DataSnapshot?) {


                            //daha önceden kotu diye oy vermişse
                            if(p0!!.hasChild(userID))
                            {
                                mRef.child("onaylar").child(oankigonderi.postID).child("kotu").child(userID).removeValue()
                              //  kotu.setImageResource(R.drawable.ic_kotu)
                                Picasso.with(context).load(IC_KOTU).into(kotu)
                            }
                            //daha önce kotu diye oy vermemişse
                            else
                            {
                                //kotuye ekle
                                mRef.child("onaylar").child(oankigonderi.postID).child("kotu").child(userID).setValue(userID)
                             //   iyi.setImageResource(R.drawable.ic_guzel)
                                Picasso.with(context).load(IC_IYI).into(iyi)
                             //   notr.setImageResource(R.drawable.ic_notr)
                                Picasso.with(context).load(IC_NOTR).into(notr)
                             //   kotu.setImageResource(R.drawable.ic_kotu_kirmizi)
                                Picasso.with(context).load(IC_KOTU_KIRMIZI).into(kotu)

                                //deiğerlerinde varsa çıkart
                                mRef.child("onaylar").child(oankigonderi.postID).child("iyi").addListenerForSingleValueEvent(
                                    object :ValueEventListener
                                    {
                                        override fun onCancelled(p0: DatabaseError?) {
                                        }

                                        override fun onDataChange(p0: DataSnapshot?) {
                                            //daha önceden iyi diye oy vermişse kaldır
                                            if(p0!!.hasChild(userID))
                                            {
                                                mRef.child("onaylar").child(oankigonderi.postID).child("iyi").child(userID).removeValue()

                                            }
                                            //iyi diye de vermemişse
                                            else
                                            {

                                                mRef.child("onaylar").child(oankigonderi.postID).child("notr").addListenerForSingleValueEvent(
                                                    object :ValueEventListener {
                                                        override fun onCancelled(p0: DatabaseError?) {

                                                        }

                                                        override fun onDataChange(p0: DataSnapshot?) {
                                                            //daha önceden kotu diye oy vermişse kaldır
                                                            if(p0!!.hasChild(userID))
                                                            {
                                                                mRef.child("onaylar").child(oankigonderi.postID).child("notr").child(userID).removeValue()

                                                            }
                                                        }

                                                    }

                                                )




                                            }
                                        }

                                    }
                                )





                            }




                        }

                    }
                )


            }


        }

        private fun postAc(postID: String?, userID: String?) {
            var intent = Intent(myhomeactivity , PostGosterActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            intent.putExtra("PostId",postID)
            intent.putExtra("UserId",userID)
            myhomeactivity.startActivity(intent)
        }

        private fun profilAc(userID: String?) {

            var intent:Intent
            if(userID!!.equals(FirebaseAuth.getInstance().currentUser!!.uid))
            {
                intent = Intent(myhomeactivity , ProfileActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                intent.putExtra("secilenUserId",userID)
                myhomeactivity.startActivity(intent)
            }
            else
            {
                intent = Intent(myhomeactivity , UserProfileActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                intent.putExtra("secilenUserId",userID)
                myhomeactivity.startActivity(intent)
            }

                 }

        private fun yorumKontrol(pos: Int, oankigonderi: UserPosts) {

            var mRef = FirebaseDatabase.getInstance().reference
            mRef.child("comments").child(oankigonderi!!.postID).addValueEventListener(object : ValueEventListener
            {
                override fun onCancelled(p0: DatabaseError?) {

                }

                override fun onDataChange(p0: DataSnapshot?) {

                    var yorumSayisi = p0!!.childrenCount
                    if(yorumSayisi.toInt() >=1)
                    {
                        yorum_sayi.setText(((yorumSayisi.toInt())-1).toString())
                    }
                    else
                    {
                        yorum_sayi.setText("0")
                    }

                }
            }
            )


        }

        private fun begeniKontrol(oankigonderi: UserPosts) {
            var mRefiyi = FirebaseDatabase.getInstance().reference
            var mRefnotr = FirebaseDatabase.getInstance().reference
            var mRefkotu = FirebaseDatabase.getInstance().reference
            var userID = FirebaseAuth.getInstance().currentUser!!.uid
            mRefiyi.child("onaylar").child(oankigonderi.postID).child("iyi").addValueEventListener(
                object :ValueEventListener
                {
                    override fun onCancelled(p0: DatabaseError?) {

                    }

                    override fun onDataChange(p0: DataSnapshot?) {
                        if(p0!!.getValue()!= null) //önceden bir beğeni alındıysa sayısını yazacağız
                        {
                            iyi_sayi.setText(""+p0!!.childrenCount!!.toString())
                        }
                        else
                        {
                            iyi_sayi.setText("0")
                        }


                        if(p0!!.hasChild(userID))
                        {   //  iyi.setImageResource(R.drawable.ic_guzel_yesil)
                        Picasso.with(context).load(IC_IYI_YESIL).into(iyi)}
                        else
                        {     // iyi.setImageResource(R.drawable.ic_guzel)
                        Picasso.with(context).load(IC_IYI).into(iyi)}
                    }
                })
            mRefnotr.child("onaylar").child(oankigonderi.postID).child("notr").addValueEventListener(
                object :ValueEventListener
                {
                    override fun onCancelled(p0: DatabaseError?) {

                    }

                    override fun onDataChange(p0: DataSnapshot?) {
                        if(p0!!.getValue()!= null) //önceden bir beğeni alındıysa sayısını yazacağız
                        {
                            notr_sayi.setText(""+p0!!.childrenCount!!.toString())
                        }
                        else
                        {
                            notr_sayi.setText("0")
                        }
                        if(p0!!.hasChild(userID))
                        {     //   notr.setImageResource(R.drawable.ic_notr_sari)
                            Picasso.with(context).load(IC_NOTR_SARI).into(notr)
                        }
                        else
                        {   //  notr.setImageResource(R.drawable.ic_notr)
                            Picasso.with(context).load(IC_NOTR).into(notr)
                        }
                    }
                })

            mRefkotu.child("onaylar").child(oankigonderi.postID).child("kotu").addValueEventListener(
                object :ValueEventListener
                {
                    override fun onCancelled(p0: DatabaseError?) {

                    }

                    override fun onDataChange(p0: DataSnapshot?) {
                        if(p0!!.getValue()!= null) //önceden bir beğeni alındıysa sayısını yazacağız
                        {
                            kotu_sayi.setText(""+p0!!.childrenCount!!.toString())
                        }
                        else
                        {
                            kotu_sayi.setText("0")
                        }
                        if(p0!!.hasChild(userID))
                        {   //  kotu.setImageResource(R.drawable.ic_kotu_kirmizi)
                            Picasso.with(context).load(IC_KOTU_KIRMIZI).into(kotu)
                             }
                        else
                        {   //  kotu.setImageResource(R.drawable.ic_kotu)
                            Picasso.with(context).load(IC_KOTU).into(kotu)

                        }
                    }
                })
        }


        private fun fragmentChange(gelenActivity: HomeActivity, gelenFragment: CommentFragment, gelenKey: String) {
            var transaction=gelenActivity.supportFragmentManager.beginTransaction() //fragmentbaşlat
            transaction.replace(R.id.homeFragmentContainer,gelenFragment) //gelenFragmenti koy
            transaction.addToBackStack(gelenKey)
            transaction.commit()
        }

    }


}