package com.besoci.sausosyal.besoci.Weather

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.support.constraint.ConstraintLayout
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.besoci.sausosyal.besoci.News.NewsActivity
import com.besoci.sausosyal.besoci.R
import com.besoci.sausosyal.besoci.Session.Session
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_weather.*
import org.json.JSONObject
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

private const val PERMISSION_REQUEST = 10
class WeatherActivity :AppCompatActivity(){
    lateinit var locationManager: LocationManager
    private var hasGps = false
    private var hasNetwork = false
    private var locationGps: Location? = null
    private var locationNetwork: Location? = null
    var session: Session? = null
    lateinit var mRef : DatabaseReference
    lateinit var mUser : FirebaseUser
    lateinit var mAuth : FirebaseAuth
      var kulAdi : String? = ""
    val GRI = "https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fgri.jpg?alt=media&token=f9a5c3c3-c120-4d75-87e8-bb5afa3b8a7e"
    val MAVI = "https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fmavi.jpg?alt=media&token=f2391af5-ca31-4208-8f26-295c6c602027"
    val SIYAH = "https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fsiyah.png?alt=media&token=2c6fbb47-59b3-4394-af96-2e2b382e0f48"



    private var permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weather)




        textTavsiye.setOnClickListener {
         intent =  Intent(this , NewsActivity::class.java)
startActivity(intent)
        }


        mAuth = FirebaseAuth.getInstance()
        mUser = mAuth.currentUser!!

        mRef = FirebaseDatabase.getInstance().reference
        mRef.child("users").child(mUser.uid).child("name_surname").addListenerForSingleValueEvent(object : ValueEventListener
        {
            override fun onCancelled(p0: DatabaseError?) {
            }

            override fun onDataChange(p0: DataSnapshot?) {
               kulAdi = p0!!.getValue().toString()
            }
        })
        mRef = FirebaseDatabase.getInstance().reference
        val refreshButton = findViewById<ImageView>(R.id.imageViewWeatherRefresh)
        refreshButton.setOnClickListener {
          istekat()
        }



         session = com.besoci.sausosyal.besoci.Session.Session(this)

        // disableView()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermission(permissions)) {
                getLocation()


            } else {
                requestPermissions(permissions, PERMISSION_REQUEST)
            }
        } else {
            getLocation()
        }






istekat()

    }

    private fun istekat() {


        val tvSehir = findViewById<TextView>(R.id.textCity)
        val tvSicaklik = findViewById<TextView>(R.id.textTemp)
        val tvAciklama = findViewById<TextView>(R.id.textDesc)
        val tvLastRefreshDate = findViewById<TextView>(R.id.textLastRefreshDate)
        val imgHavaLogo = findViewById<ImageView>(R.id.imageViewWeather)
        val tumLayout = findViewById<ConstraintLayout>(R.id.arkaPlanHava)



        val url = "http://api.openweathermap.org/data/2.5/weather?lat=${session!!.latitude.toString()}&lon=${session!!.longitude.toString()}&appid=dad04ece879a52b1aaa34defa5cdaf97&lang=tr&units=metric";

        val havaDurumuObjeRequest = JsonObjectRequest(Request.Method.GET,url,null,object : Response.Listener<JSONObject>
        {
            override fun onResponse(response: JSONObject?) {

                var main = response?.getJSONObject("main")
                var sicaklik = main?.getString("temp")!!.split(".")[0]+"°C"
                var sehiradi=response?.getString("name")
                var weather=response?.getJSONArray("weather")
                var aciklama = weather?.getJSONObject(0)?.getString("description")
                var icon = weather?.getJSONObject(0)?.getString("icon")+".png"
                var lastupdate = response?.getString("dt")


                tvSehir.setText(sehiradi.toString())
                tvSicaklik.setText(sicaklik.toString())
                tvAciklama.setText(aciklama.toString())
                tvLastRefreshDate.setText(lastupdate.toString())
                Picasso.with(this@WeatherActivity).load("http://openweathermap.org/img/w/"+icon).into(imgHavaLogo)

                if(icon.equals("01d.png")  ||  icon.equals("02d.png") )  //güneşli veya az bulutlu sabahlar
                {
                   // arkaPlanHava.setBackgroundResource(R.drawable.mavi)
                    Picasso.with(this@WeatherActivity).load(MAVI).into(imageViewArkaPlan)
                    Picasso.with(this@WeatherActivity).load("http://openweathermap.org/img/w/"+icon).into(imgHavaLogo)
                    textTavsiye.setText("Merhaba "+kulAdi+" Bugün Hava Güzel Gibi Görünüyor Dış Mekanlarda Gezip Doğanın Tadını Çıkarabilirsin Senin İçin Önerebileceğim Mekanlara Bakmak İstersen Dokun ")
                }
                else if(icon.equals("03d.png")  ||  icon.equals("04d.png") )  //bulutlu sabahlar
                {
               //     arkaPlanHava.setBackgroundResource(R.drawable.gri)
                    Picasso.with(this@WeatherActivity).load(GRI).into(imageViewArkaPlan)
                    textTavsiye.setText("Merhaba "+kulAdi+" Bugün Hava Kapalı  Gibi Görünüyor İç Mekanlarda Gezebilrsin veya Ben Üşümem Diyorsan  Dışarıda da gezebilrsin  Senin İçin Önerebileceğim Mekanlara Bakmak İstersen Dokun ")
                    Picasso.with(this@WeatherActivity).load("http://openweathermap.org/img/w/"+icon).into(imgHavaLogo)


                }
                else if(icon.equals("09d.png")  ||  icon.equals("10d.png")   ||  icon.equals("11d.png") )  //yağmurlu veya fırtınalı sabahlar
                {

             //       arkaPlanHava.setBackgroundResource(R.drawable.gri)
                    Picasso.with(this@WeatherActivity).load(GRI).into(imageViewArkaPlan)

                    textTavsiye.setText("Merhaba "+kulAdi+" Bugün Hava Yağışlı Gibi Görünüyor İç Mekanlarda Gezebilrsin veya Yağmurun Tadını Çıkarmak İçin Dışarıda da Gezebilrsin  Senin İçin Önerebileceğim Mekanlara Bakmak İstersen Dokun ")
                    Picasso.with(this@WeatherActivity).load("http://openweathermap.org/img/w/"+icon).into(imgHavaLogo)



                }
                else if(icon.equals("13d.png")  )  //karlı sabahlar
                {
                  //  arkaPlanHava.setBackgroundResource(R.drawable.mavi)
                    Picasso.with(this@WeatherActivity).load(MAVI).into(imageViewArkaPlan)

                    textTavsiye.setText("Merhaba "+kulAdi+" Bugün Hava Karlı  Gibi Görünüyor İç Mekanlarda Gezebilrsin veya Karın Tadını Çıkarmak İçin Dışarıda da Gezebilrsin  Senin İçin Önerebileceğim Mekanlara Bakmak İstersen Dokun ")
                    Picasso.with(this@WeatherActivity).load("http://openweathermap.org/img/w/"+icon).into(imgHavaLogo)




                }
                else if(icon.equals("50d.png")  )  //sisli sabahlar
                {
                   // arkaPlanHava.setBackgroundResource(R.drawable.gri)
                    Picasso.with(this@WeatherActivity).load(GRI).into(imageViewArkaPlan)

                    textTavsiye.setText("Merhaba "+kulAdi+" Bugün Hava Sisli  Gibi Görünüyor Dış Mekanlarda Gezmeni Pek Tavsiye Etmem ,  İç Mekanlarda Gezebilrsin   Senin İçin Önerebileceğim Mekanlara Bakmak İstersen Dokun ")
                    Picasso.with(this@WeatherActivity).load("http://openweathermap.org/img/w/"+icon).into(imgHavaLogo)



                }
                if(icon.equals("01n.png")  ||  icon.equals("02n.png") )  //güneşli veya az bulutlu geceler
                {
                  //  arkaPlanHava.setBackgroundResource(R.drawable.siyah)
                    Picasso.with(this@WeatherActivity).load(SIYAH).into(imageViewArkaPlan)

                    textTavsiye.setText("Merhaba "+kulAdi+" Bu Akşam Hava Güzel Gibi Görünüyor Dış Mekanlarda Gezip Doğanın Tadını Çıkarabilirsin Senin İçin Önerebileceğim Mekanlara Bakmak İstersen Dokun ")
                    Picasso.with(this@WeatherActivity).load("http://openweathermap.org/img/w/"+icon).into(imgHavaLogo)



                }
                else if(icon.equals("03n.png")  ||  icon.equals("04n.png") )  //bulutlu geceler
                {
                    //arkaPlanHava.setBackgroundResource(R.drawable.siyah)
                    Picasso.with(this@WeatherActivity).load(SIYAH).into(imageViewArkaPlan)

                    textTavsiye.setText("Merhaba "+kulAdi+" Bu Akşam Hava Kapalı  Gibi Görünüyor İç Mekanlarda Gezebilrsin veya Ben Üşümem Diyorsan  Dışarıda da gezebilrsin  Senin İçin Önerebileceğim Mekanlara Bakmak İstersen Dokun ")
                    Picasso.with(this@WeatherActivity).load("http://openweathermap.org/img/w/"+icon).into(imgHavaLogo)



                }
                else if(icon.equals("09n.png")  ||  icon.equals("10n.png")   ||  icon.equals("11n.png") )  //yağmurlu veya fırtınalı geceler
                {

                    Picasso.with(this@WeatherActivity).load(SIYAH).into(imageViewArkaPlan)

                //    arkaPlanHava.setBackgroundResource(R.drawable.siyah)
                    textTavsiye.setText("Merhaba "+kulAdi+" Bu Akşam Hava Yağışlı Gibi Görünüyor İç Mekanlarda Gezebilrsin veya Yağmurun Tadını Çıkarmak İçin Dışarıda da Gezebilrsin  Senin İçin Önerebileceğim Mekanlara Bakmak İstersen Dokun ")
                    Picasso.with(this@WeatherActivity).load("http://openweathermap.org/img/w/"+icon).into(imgHavaLogo)



                }
                else if(icon.equals("13n.png")  )  //karlı geceler
                {

                    Picasso.with(this@WeatherActivity).load(SIYAH).into(imageViewArkaPlan)

                   // arkaPlanHava.setBackgroundResource(R.drawable.siyah)
                    textTavsiye.setText("Merhaba "+kulAdi+" Bu Akşam Hava Karlı  Gibi Görünüyor İç Mekanlarda Gezebilrsin veya Karın Tadını Çıkarmak İçin Dışarıda da Gezebilrsin  Senin İçin Önerebileceğim Mekanlara Bakmak İstersen Dokun ")
                    Picasso.with(this@WeatherActivity).load("http://openweathermap.org/img/w/"+icon).into(imgHavaLogo)


                }
                else if(icon.equals("50n.png")  )  //sisli geceler
                {

                    Picasso.with(this@WeatherActivity).load(SIYAH).into(imageViewArkaPlan)

                //    arkaPlanHava.setBackgroundResource(R.drawable.siyah)
                    textTavsiye.setText("Merhaba "+kulAdi+" Bu Akşam Hava Sisli  Gibi Görünüyor Dış Mekanlarda Gezmeni Pek Tavsiye Etmem ,  İç Mekanlarda Gezebilrsin   Senin İçin Önerebileceğim Mekanlara Bakmak İstersen Dokun ")
                    Picasso.with(this@WeatherActivity).load("http://openweathermap.org/img/w/"+icon).into(imgHavaLogo)



                }

            }
        },object : Response.ErrorListener{
            override fun onErrorResponse(error: VolleyError?) {
            }
        }

        )

        MySingleton.getInstance(this@WeatherActivity!!)?.addToRequestQueue(havaDurumuObjeRequest);











    }


    @SuppressLint("MissingPermission")
    private fun getLocation() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        hasGps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        hasNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        val df = DecimalFormat("#.###")

        if (hasGps || hasNetwork) {

            if (hasGps) {
                Log.d("CodeAndroidLocation", "hasGps")
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0F, object : LocationListener {
                    override fun onLocationChanged(location: Location?) {
                        if (location != null) {
                            locationGps = location

                            df.roundingMode = RoundingMode.CEILING
                            session!!.latitude=  df.format( locationGps!!.latitude.toFloat()).toString().replace(",",".").toFloat()
                            session!!.longitude=df.format( locationGps!!.longitude.toFloat()).toString().replace(",",".").toFloat()
                           // istekat()
                        }
                    }

                    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

                    }

                    override fun onProviderEnabled(provider: String?) {

                    }

                    override fun onProviderDisabled(provider: String?) {

                    }

                })

                val localGpsLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                if (localGpsLocation != null)
                    locationGps = localGpsLocation
            }
            if (hasNetwork) {
                Log.d("CodeAndroidLocation", "hasGps")
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 0F, object : LocationListener {
                    override fun onLocationChanged(location: Location?) {
                        if (location != null) {
                            locationNetwork = location
                            val df = DecimalFormat("#.###")
                            df.roundingMode = RoundingMode.CEILING
                            session!!.latitude=  df.format( locationNetwork!!.latitude.toFloat()).toString().replace(",",".").toFloat()
                            session!!.longitude=df.format( locationNetwork!!.longitude.toFloat()).toString().replace(",",".").toFloat()
                         //   istekat()

                        }
                    }

                    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

                    }

                    override fun onProviderEnabled(provider: String?) {

                    }

                    override fun onProviderDisabled(provider: String?) {

                    }

                })

                val localNetworkLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                if (localNetworkLocation != null)
                    locationNetwork = localNetworkLocation
            }

            if(locationGps!= null && locationNetwork!= null){
                if(locationGps!!.accuracy > locationNetwork!!.accuracy){
                    val df = DecimalFormat("#.###")
                    df.roundingMode = RoundingMode.CEILING
                    session!!.latitude=  df.format( locationGps!!.latitude.toFloat()).toString().replace(",",".").toFloat()
                    session!!.longitude=df.format( locationGps!!.longitude.toFloat()).toString().replace(",",".").toFloat()

                    //istekat()

                }else{
                    val df = DecimalFormat("#.###")
                    df.roundingMode = RoundingMode.CEILING
                    //şimdi ekliyom
                    if(locationGps != null)
                    {
                        session!!.latitude=  df.format( locationGps!!.latitude.toFloat()).toString().replace(",",".").toFloat()
                    session!!.longitude=df.format( locationGps!!.longitude.toFloat()).toString().replace(",",".").toFloat()

                    }
                    else if(locationNetwork != null)
                    {
                        session!!.latitude=  df.format( locationNetwork!!.latitude.toFloat()).toString().replace(",",".").toFloat()
                        session!!.longitude=df.format( locationNetwork!!.longitude.toFloat()).toString().replace(",",".").toFloat()

                    }
                   // istekat()
                }
            }

        } else {
            startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
        }
    }

    private fun checkPermission(permissionArray: Array<String>): Boolean {
        var allSuccess = true
        for (i in permissionArray.indices) {
            if (checkCallingOrSelfPermission(permissionArray[i]) == PackageManager.PERMISSION_DENIED)
                allSuccess = false
        }
        return allSuccess
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQUEST) {
            var allSuccess = true
            for (i in permissions.indices) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    allSuccess = false
                    val requestAgain = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && shouldShowRequestPermissionRationale(permissions[i])
                    if (requestAgain) {
                        Toast.makeText(this, "Erişim Reddedildi !", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this, "Hizmeti kullanabilmeniz için ayarlara gidip konum erişimine izin vermeniz gerekmektedir", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            if (allSuccess)
getLocation()
        }
    }
}

