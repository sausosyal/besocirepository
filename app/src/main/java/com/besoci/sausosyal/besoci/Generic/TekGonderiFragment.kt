package com.besoci.sausosyal.besoci.Generic


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import android.widget.Toast
import com.besoci.sausosyal.besoci.Models.UserPosts
import com.besoci.sausosyal.besoci.Profile.ProfileActivity

import com.besoci.sausosyal.besoci.R
import com.besoci.sausosyal.besoci.Utils.BottomNavigationViewHelper
import com.besoci.sausosyal.besoci.Utils.EventbusDataEvents
import com.besoci.sausosyal.besoci.Utils.TimeAgo
import com.besoci.sausosyal.besoci.Utils.UniversalImageLoader
import com.besoci.sausosyal.besoci.VideoRecyclerView.view.Video
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_user_profile.*
import kotlinx.android.synthetic.main.fragment_tek_gonderi.*
import kotlinx.android.synthetic.main.fragment_tek_gonderi.view.*
import kotlinx.android.synthetic.main.tek_sutun_grid_resim_profile.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe


class TekGonderiFragment : Fragment() {

    var myView : View? = null
    var secilenGonderi : UserPosts? = null
    var videoMu : Boolean? = null
    lateinit var iyi_sayi :TextView
    lateinit var notr_sayi  :TextView
    lateinit var kotu_sayi  :TextView
    lateinit var yorum_sayi :TextView
    lateinit  var iyi :ImageView
    lateinit  var notr :ImageView
    lateinit  var kotu :ImageView
    val ACTIVITY_NO = 4 //profile
    val IC_IYI ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fic_guzel.png?alt=media&token=9d6be357-6590-4726-b501-61415c92dc80"
    val IC_IYI_YESIL ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fic_guzel_yesil.png?alt=media&token=a3eaeade-9cb6-4b3d-93ab-c5cbc38805c0"
    val IC_NOTR ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fic_notr.png?alt=media&token=0aef3dfd-4679-4e5c-b246-3a17d9549f78"
    val IC_KOTU ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fic_kotu.png?alt=media&token=23b7b71c-a510-4888-b0d4-224b29cdc951"
    val IC_KOTU_KIRMIZI ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fic_kotu_kirmizi.png?alt=media&token=ce10d4a0-999f-47a0-a777-e2356b367ff9"
    val IC_NOTR_SARI ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fic_notr_sari.png?alt=media&token=90757088-be11-4a48-9d8d-0fb2ac83ccd5"
    val IC_INFO ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fic_info.png?alt=media&token=e0a7bdc1-75a5-426a-9997-7dd2708827c1"
    val IC_YORUM ="https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fic_comment.png?alt=media&token=f5de21ac-b6f7-41ee-ad5f-154658486a95"










    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        myView = inflater.inflate(R.layout.fragment_tek_gonderi, container, false)




        var tumLayout:ConstraintLayout = myView as ConstraintLayout










        setData(tumLayout,secilenGonderi!!,videoMu!!)







        myView!!.BackButton.setOnClickListener {

            activity!!.onBackPressed()

        }



        return myView
    }


    fun setupNavigationView()
    {
        BottomNavigationViewHelper.setupBottomNavigationView(myView!!.bottomNavigationViewim)
        BottomNavigationViewHelper.setupNavigation(activity!!,myView!!.bottomNavigationViewim)
        var menu = myView!!.bottomNavigationViewim.menu
        var menuitem = menu.getItem(ACTIVITY_NO)
        menuitem.setChecked(true)
    }

    override fun onResume() {
        super.onResume()
        setupNavigationView()
    }


    fun setData(tumLayout : ConstraintLayout ,oankigonderi: UserPosts, videoMu: Boolean)

    {

        var  secenekler=tumLayout.tekPostSecenekler
        var profileImage = tumLayout.tekPostProfilResmi
        var adsoyad = tumLayout.tekPostAdSoyad
        var username = tumLayout.tekPostKullaniciAdi
        var gonderi = tumLayout.tekPostResim
        var gonderiAciklama = tumLayout.tekPostAciklama
        var gonderiZamani = tumLayout.tekPostZaman
        var yorumYap = tumLayout.tekPostYorum
         iyi = tumLayout.tekPostIyiLogo
         notr = tumLayout.tekPostNotrLogo
         kotu = tumLayout.tekPostKotuLogo
         iyi_sayi = tumLayout.tekPostIyiSayi
         notr_sayi = tumLayout.tekPostNotrSayi
         kotu_sayi = tumLayout.tekPostKotuSayi
         yorum_sayi= tumLayout.tekPostYorumSayi
        var myVideo = tumLayout.VideoView
        var camAnimation = tumLayout.cameraAnimation
        var info = tumLayout.tekPostInfo
        var myhomeactivity = activity

        Picasso.with(context).load(IC_INFO).into(info)
        Picasso.with(context).load(IC_YORUM).into(yorumYap)




        if(videoMu) //videoya
        {
            myVideo.visibility=View.VISIBLE
            gonderi.visibility=View.INVISIBLE
            myVideo.setVideo(Video(oankigonderi.postURL!!,0)) //seekTo 0.saniyeden ba�la dedi

        }
        else //resime
        {
            myVideo.visibility=View.GONE
            gonderi.visibility=View.VISIBLE
            UniversalImageLoader.setImage(oankigonderi.postURL!!,gonderi,null,"")

        }


        adsoyad.setText(oankigonderi.UserName)
        username.setText(oankigonderi.UserName)
        gonderiAciklama.setText(oankigonderi.postAciklama)
        UniversalImageLoader.setImage(oankigonderi.UserPhotoURL!!,profileImage,null,"")
        gonderiZamani.setText(TimeAgo.getTimeAgo(oankigonderi.postYuklenmeTarih!!))


       /* secenekler.setOnClickListener {*/


          /*  var popup: PopupMenu? = null;
            popup = PopupMenu(activity, secenekler)
            popup.inflate(R.menu.popup_menu)*/

        //    popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item: MenuItem? ->

           //     when (item!!.itemId) {
             //       R.id.m_sil-> {
              //          Toast.makeText(activity, item.title, Toast.LENGTH_SHORT).show();
               //     }
               //     R.id.m_duzenle -> {
                //        Toast.makeText(activity, item.title, Toast.LENGTH_SHORT).show();
              //      }
//
              //  }

         //       true
         //   })

         //   popup.show()



      //  }

        yorumYap.setOnClickListener {


            (activity as AppCompatActivity).profileRoute.visibility=View.INVISIBLE
            (activity as AppCompatActivity).profileContainer.visibility=View.VISIBLE
            EventBus.getDefault().postSticky(EventbusDataEvents.YorumYapilacakGonderininIDsiniGonder(oankigonderi!!.postID))
            var transaction=(activity as AppCompatActivity).supportFragmentManager.beginTransaction()
            transaction.replace(R.id.profileContainer,CommentFragment()) //gelenFragmenti koy
            transaction.addToBackStack("tekGonderiFragmentEklendi")
            transaction.commit()





         //   EventBus.getDefault().postSticky(EventbusDataEvents.YorumYapilacakGonderininIDsiniGonder(oankigonderi!!.postID))

          ////  tumLayout.visibility=View.INVISIBLE
          //  fragmentChange((activity as ProfileActivity),CommentFragment(),"CommentFragmentEklendi")
        }





        adsoyad.setOnClickListener {

            profilAc(oankigonderi.UserID)


        }

        profileImage.setOnClickListener {

            profilAc(oankigonderi.UserID)



        }

        gonderi.setOnClickListener {
            postAc(oankigonderi.postID ,oankigonderi.UserID)


        }

        info.setOnClickListener {
            postAc(oankigonderi.postID,oankigonderi.UserID)



        }









        begeniKontrol(oankigonderi)
         yorumKontrol(oankigonderi)

        iyi.setOnClickListener {

            var mRef = FirebaseDatabase.getInstance().reference
            var userID = FirebaseAuth.getInstance().currentUser!!.uid
            mRef.child("onaylar").child(oankigonderi.postID).child("iyi").addListenerForSingleValueEvent(
                object :ValueEventListener
                {
                    override fun onCancelled(p0: DatabaseError?) {
                    }

                    override fun onDataChange(p0: DataSnapshot?) {


                        //daha �nceden iyi diye oy vermi�se
                        if(p0!!.hasChild(userID))
                        {
                            mRef.child("onaylar").child(oankigonderi.postID).child("iyi").child(userID).removeValue()
                         //   iyi.setImageResource(R.drawable.ic_guzel)
                            Picasso.with(context).load(IC_IYI).into(iyi)

                        }
                        //daha �nce iyi diye oy vermemi�se
                        else
                        {
                            //iyiye ekle
                            mRef.child("onaylar").child(oankigonderi.postID).child("iyi").child(userID).setValue(userID)
                          //  notr.setImageResource(R.drawable.ic_notr)
                            Picasso.with(context).load(IC_NOTR).into(notr)

                          //  kotu.setImageResource(R.drawable.ic_kotu)
                            Picasso.with(context).load(IC_KOTU).into(kotu)

                           // iyi.setImageResource(R.drawable.ic_guzel_yesil)
                            Picasso.with(context).load(IC_IYI_YESIL).into(iyi)


                            //dei�erlerinde varsa ��kart
                            mRef.child("onaylar").child(oankigonderi.postID).child("notr").addListenerForSingleValueEvent(
                                object :ValueEventListener
                                {
                                    override fun onCancelled(p0: DatabaseError?) {
                                    }

                                    override fun onDataChange(p0: DataSnapshot?) {
                                        //daha �nceden notr diye oy vermi�se kald�r
                                        if(p0!!.hasChild(userID))
                                        {
                                            mRef.child("onaylar").child(oankigonderi.postID).child("notr").child(userID).removeValue()

                                        }
                                        //notr diye de vermemi�se
                                        else
                                        {

                                            mRef.child("onaylar").child(oankigonderi.postID).child("kotu").addListenerForSingleValueEvent(
                                                object :ValueEventListener {
                                                    override fun onCancelled(p0: DatabaseError?) {

                                                    }

                                                    override fun onDataChange(p0: DataSnapshot?) {
                                                        //daha �nceden kotu diye oy vermi�se kald�r
                                                        if(p0!!.hasChild(userID))
                                                        {
                                                            mRef.child("onaylar").child(oankigonderi.postID).child("kotu").child(userID).removeValue()

                                                        }
                                                    }

                                                }

                                            )




                                        }
                                    }

                                }
                            )





                        }




                    }

                }
            )


        }



        notr.setOnClickListener {

            var mRef = FirebaseDatabase.getInstance().reference
            var userID = FirebaseAuth.getInstance().currentUser!!.uid
            mRef.child("onaylar").child(oankigonderi.postID).child("notr").addListenerForSingleValueEvent(
                object :ValueEventListener
                {
                    override fun onCancelled(p0: DatabaseError?) {
                    }

                    override fun onDataChange(p0: DataSnapshot?) {


                        //daha �nceden notr diye oy vermi�se
                        if(p0!!.hasChild(userID))
                        {
                            mRef.child("onaylar").child(oankigonderi.postID).child("notr").child(userID).removeValue()
                           // notr.setImageResource(R.drawable.ic_notr)
                            Picasso.with(context).load(IC_NOTR).into(notr)

                        }
                        //daha �nce notr diye oy vermemi�se
                        else
                        {
                            //notre ekle
                            mRef.child("onaylar").child(oankigonderi.postID).child("notr").child(userID).setValue(userID)
                            //iyi.setImageResource(R.drawable.ic_guzel)
                            Picasso.with(context).load(IC_IYI).into(iyi)

                          //  kotu.setImageResource(R.drawable.ic_kotu)
                            Picasso.with(context).load(IC_KOTU).into(kotu)

                         //   notr.setImageResource(R.drawable.ic_notr_sari)
                            Picasso.with(context).load(IC_NOTR_SARI).into(notr)


                            //dei�erlerinde varsa ��kart
                            mRef.child("onaylar").child(oankigonderi.postID).child("iyi").addListenerForSingleValueEvent(
                                object :ValueEventListener
                                {
                                    override fun onCancelled(p0: DatabaseError?) {
                                    }

                                    override fun onDataChange(p0: DataSnapshot?) {
                                        //daha �nceden iyi diye oy vermi�se kald�r
                                        if(p0!!.hasChild(userID))
                                        {
                                            mRef.child("onaylar").child(oankigonderi.postID).child("iyi").child(userID).removeValue()


                                        }
                                        //iyi diye de vermemi�se
                                        else
                                        {

                                            mRef.child("onaylar").child(oankigonderi.postID).child("kotu").addListenerForSingleValueEvent(
                                                object :ValueEventListener {
                                                    override fun onCancelled(p0: DatabaseError?) {

                                                    }

                                                    override fun onDataChange(p0: DataSnapshot?) {
                                                        //daha �nceden kotu diye oy vermi�se kald�r
                                                        if(p0!!.hasChild(userID))
                                                        {
                                                            mRef.child("onaylar").child(oankigonderi.postID).child("kotu").child(userID).removeValue()

                                                        }
                                                    }

                                                }

                                            )




                                        }
                                    }

                                }
                            )





                        }




                    }

                }
            )


        }


        kotu.setOnClickListener {

            var mRef = FirebaseDatabase.getInstance().reference
            var userID = FirebaseAuth.getInstance().currentUser!!.uid
            mRef.child("onaylar").child(oankigonderi.postID).child("kotu").addListenerForSingleValueEvent(
                object :ValueEventListener
                {
                    override fun onCancelled(p0: DatabaseError?) {
                    }

                    override fun onDataChange(p0: DataSnapshot?) {


                        //daha �nceden kotu diye oy vermi�se
                        if(p0!!.hasChild(userID))
                        {
                            mRef.child("onaylar").child(oankigonderi.postID).child("kotu").child(userID).removeValue()
                           // kotu.setImageResource(R.drawable.ic_kotu)
                            Picasso.with(context).load(IC_KOTU).into(kotu)

                        }
                        //daha �nce kotu diye oy vermemi�se
                        else
                        {
                            //kotuye ekle
                            mRef.child("onaylar").child(oankigonderi.postID).child("kotu").child(userID).setValue(userID)
                          //  iyi.setImageResource(R.drawable.ic_guzel)
                            Picasso.with(context).load(IC_IYI).into(iyi)

                           // notr.setImageResource(R.drawable.ic_notr)
                            Picasso.with(context).load(IC_NOTR).into(notr)

                         //   kotu.setImageResource(R.drawable.ic_kotu_kirmizi)
                            Picasso.with(context).load(IC_KOTU_KIRMIZI).into(kotu)


                            //dei�erlerinde varsa ��kart
                            mRef.child("onaylar").child(oankigonderi.postID).child("iyi").addListenerForSingleValueEvent(
                                object :ValueEventListener
                                {
                                    override fun onCancelled(p0: DatabaseError?) {
                                    }

                                    override fun onDataChange(p0: DataSnapshot?) {
                                        //daha �nceden iyi diye oy vermi�se kald�r
                                        if(p0!!.hasChild(userID))
                                        {
                                            mRef.child("onaylar").child(oankigonderi.postID).child("iyi").child(userID).removeValue()

                                        }
                                        //iyi diye de vermemi�se
                                        else
                                        {

                                            mRef.child("onaylar").child(oankigonderi.postID).child("notr").addListenerForSingleValueEvent(
                                                object :ValueEventListener {
                                                    override fun onCancelled(p0: DatabaseError?) {

                                                    }

                                                    override fun onDataChange(p0: DataSnapshot?) {
                                                        //daha �nceden kotu diye oy vermi�se kald�r
                                                        if(p0!!.hasChild(userID))
                                                        {
                                                            mRef.child("onaylar").child(oankigonderi.postID).child("notr").child(userID).removeValue()

                                                        }
                                                    }

                                                }

                                            )




                                        }
                                    }

                                }
                            )





                        }




                    }

                }
            )


        }


    }







    private fun postAc(postID: String?, userID: String?) {
        var intent = Intent(activity , PostGosterActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        intent.putExtra("PostId",postID)
        intent.putExtra("UserId",userID)
        activity!!.startActivity(intent)
    }












    private fun profilAc(userID: String?) {

        var intent:Intent
        if(userID!!.equals(FirebaseAuth.getInstance().currentUser!!.uid))
        {
            intent = Intent(activity , ProfileActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            intent.putExtra("secilenUserId",userID)
            activity!!.startActivity(intent)
        }
        else
        {
            intent = Intent(activity , UserProfileActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            intent.putExtra("secilenUserId",userID)
            activity!!.startActivity(intent)
        }

    }



    private fun begeniKontrol(oankigonderi: UserPosts) {
        var mRefiyi = FirebaseDatabase.getInstance().reference
        var mRefnotr = FirebaseDatabase.getInstance().reference
        var mRefkotu = FirebaseDatabase.getInstance().reference
        var userID = FirebaseAuth.getInstance().currentUser!!.uid
        mRefiyi.child("onaylar").child(oankigonderi.postID).child("iyi").addValueEventListener(
            object : ValueEventListener
            {
                override fun onCancelled(p0: DatabaseError?) {

                }

                override fun onDataChange(p0: DataSnapshot?) {
                    if(p0!!.getValue()!= null) //�nceden bir be�eni al�nd�ysa say�s�n� yazaca��z
                    {
                        iyi_sayi.setText(""+p0!!.childrenCount!!.toString())
                    }
                    else
                    {
                        iyi_sayi.setText("0")
                    }


                    if(p0!!.hasChild(userID))
                      //  iyi.setImageResource(R.drawable.ic_guzel_yesil)
                    Picasso.with(context).load(IC_IYI_YESIL).into(iyi)

                    else
                    //    iyi.setImageResource(R.drawable.ic_guzel)
                    Picasso.with(context).load(IC_IYI).into(iyi)

                }
            })
        mRefnotr.child("onaylar").child(oankigonderi.postID).child("notr").addValueEventListener(
            object : ValueEventListener
            {
                override fun onCancelled(p0: DatabaseError?) {

                }

                override fun onDataChange(p0: DataSnapshot?) {
                    if(p0!!.getValue()!= null) //�nceden bir be�eni al�nd�ysa say�s�n� yazaca��z
                    {
                        notr_sayi.setText(""+p0!!.childrenCount!!.toString())
                    }
                    else
                    {
                        notr_sayi.setText("0")
                    }
                    if(p0!!.hasChild(userID))
                       // notr.setImageResource(R.drawable.ic_notr_sari)
                    Picasso.with(context).load(IC_NOTR_SARI).into(notr)

                    else
                     //   notr.setImageResource(R.drawable.ic_notr)
                    Picasso.with(context).load(IC_NOTR).into(notr)

                }
            })

        mRefkotu.child("onaylar").child(oankigonderi.postID).child("kotu").addValueEventListener(
            object : ValueEventListener
            {
                override fun onCancelled(p0: DatabaseError?) {

                }

                override fun onDataChange(p0: DataSnapshot?) {
                    if(p0!!.getValue()!= null) //�nceden bir be�eni al�nd�ysa say�s�n� yazaca��z
                    {
                        kotu_sayi.setText(""+p0!!.childrenCount!!.toString())
                    }
                    else
                    {
                        kotu_sayi.setText("0")
                    }
                    if(p0!!.hasChild(userID))
                    {  //      kotu.setImageResource(R.drawable.ic_kotu_kirmizi)
                    Picasso.with(context).load(IC_KOTU_KIRMIZI).into(kotu)}

                    else
                    {  //  kotu.setImageResource(R.drawable.ic_kotu)
                    Picasso.with(context).load(IC_KOTU).into(kotu)}

                }
            })
    }







    private fun yorumKontrol(oankigonderi: UserPosts) {

        var mRef = FirebaseDatabase.getInstance().reference
        mRef.child("comments").child(oankigonderi!!.postID).addValueEventListener(object : ValueEventListener
        {
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(p0: DataSnapshot?) {

                var yorumSayisi = p0!!.childrenCount
                if(yorumSayisi.toInt() >=1)
                {
                    yorum_sayi.setText(((yorumSayisi.toInt())-1).toString())
                }
                else
                {
                    yorum_sayi.setText("0")
                }

            }
        }
        )


    }












    @Subscribe(sticky = true) //eventbustan poststicky ile bi�ey g�nderilmi�se yakala
    internal fun onSecilenResimEvent(secilengonderi : EventbusDataEvents.secilenGonderiyiGonder)//benim olu�turdu�um classdan geldi
    {


        secilenGonderi = secilengonderi.secilenGonderi
        videoMu = secilengonderi.videoMu

    }



    override fun onAttach(context: Context?) {
        super.onAttach(context)
        EventBus.getDefault().register(this)
    }

    override fun onDetach() {
        super.onDetach()
        EventBus.getDefault().unregister(this)

    }








}
