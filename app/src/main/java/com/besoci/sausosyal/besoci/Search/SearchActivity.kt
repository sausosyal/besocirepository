package com.besoci.sausosyal.besoci.Search

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.View
import com.besoci.sausosyal.besoci.R
import com.besoci.sausosyal.besoci.Utils.BottomNavigationViewHelper
import kotlinx.android.synthetic.main.activity_search.*
import com.algolia.instantsearch.ui.helpers.InstantSearch
import com.algolia.instantsearch.core.helpers.Searcher
import com.algolia.instantsearch.ui.utils.ItemClickSupport
import com.besoci.sausosyal.besoci.Generic.UserProfileActivity
import com.besoci.sausosyal.besoci.Profile.ProfileActivity
import com.google.firebase.auth.FirebaseAuth


class SearchActivity : AppCompatActivity() {

    private val ACTIVITY_NO = 1;
    private val TAG = "Search"
    private val ALGOLIA_APP_ID = "DZU7GPS130"
    private val ALGOLIA_SEARCH_API_KEY = "ccf2616d3296d9ef3c2f1d1cfc44c9e8"
    private val ALGOLIA_INDEX_NAME = "kotlinGezsin"
    lateinit var mAuth : FirebaseAuth

    lateinit var  searcher: Searcher
    lateinit var helper: InstantSearch
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_search)
        mAuth=FirebaseAuth.getInstance()

        setupNavigationView()
        setupAlgoliaSearch()

    }

    private fun setupAlgoliaSearch() {
        searcher = Searcher.create(ALGOLIA_APP_ID, ALGOLIA_SEARCH_API_KEY, ALGOLIA_INDEX_NAME)
        helper = InstantSearch(this, searcher)
      //  helper.search() //bunu koyarsak bi harf yazmasak bile sonuç getirir.
        listeAramaSonuclari.setOnItemClickListener(object : ItemClickSupport.OnItemClickListener
        {
            override fun onItemClick(recyclerView: RecyclerView?, position: Int, v: View?) {
               var secilenUserId = listeAramaSonuclari.get(position).getString("user_id")
                if(secilenUserId.equals(mAuth.currentUser!!.uid.toString()))
                {
                    var intent = Intent(this@SearchActivity,ProfileActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                    startActivity(intent)
                }
                else
                {
                    var intent = Intent(this@SearchActivity,UserProfileActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                    intent.putExtra("secilenUserId",secilenUserId)
                    startActivity(intent)
                }
            }

        }

        )
    }


    fun setupNavigationView()
    {
        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewim)
        BottomNavigationViewHelper.setupNavigation(this,bottomNavigationViewim)
        var menu = bottomNavigationViewim.menu
        var menuitem = menu.getItem(ACTIVITY_NO)
        menuitem.setChecked(true)
    }


    override fun onDestroy() {
        searcher.destroy()
        super.onDestroy()
    }




    override fun onResume() {
        super.onResume()
        setupNavigationView()
    }

}
