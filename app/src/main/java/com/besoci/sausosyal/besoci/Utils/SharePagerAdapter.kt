package com.besoci.sausosyal.besoci.Utils

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.view.ViewGroup

class SharePagerAdapter (frangmentManager : FragmentManager , tabadlari : ArrayList<String>) : FragmentPagerAdapter(frangmentManager){

  private var mFragmentList :ArrayList<Fragment> = ArrayList()
    private var tabAldari : ArrayList<String> =tabadlari


    override fun getItem(p0: Int): Fragment {

        return mFragmentList.get(p0)

    }

    override fun getCount(): Int {

        return mFragmentList.size

    }

    fun addFragment(fragment: Fragment)
    {
        mFragmentList.add(fragment)
    }

     override fun getPageTitle(position: Int): CharSequence? {
        return tabAldari.get(position)
    }

    //FRAGMENTLERDE AÇILANI AKTİF ETME
    //sharepager activitye bak
    fun fragmentiPagerdanSil(viewGroup: ViewGroup,position: Int)
    {
        var silinecekfragment = this.instantiateItem(viewGroup,position)
        this.destroyItem(viewGroup,position,silinecekfragment)

    }
    fun fragmentiPageraEkle(viewGroup: ViewGroup,position: Int)
    {
        this.instantiateItem(viewGroup,position)

    }
}