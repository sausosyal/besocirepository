package com.besoci.sausosyal.besoci.Home

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.besoci.sausosyal.besoci.Models.Posts
import com.besoci.sausosyal.besoci.Models.UserPosts
import com.besoci.sausosyal.besoci.Models.Users
import com.besoci.sausosyal.besoci.R
import com.besoci.sausosyal.besoci.Session.Session
import com.besoci.sausosyal.besoci.Utils.BottomNavigationViewHelper
import com.besoci.sausosyal.besoci.Utils.HomeFragmentRecyclerAdapter
import com.besoci.sausosyal.besoci.VideoRecyclerView.view.CenterLayoutManager
import com.besoci.sausosyal.besoci.Weather.WeatherActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.hoanganhtuan95ptit.autoplayvideorecyclerview.AutoPlayVideoRecyclerView
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import java.util.*

class HomeFragment : Fragment() {

//HOME FRAGMENT RECYCLER ADAPTER



    lateinit var fragmentView : View
    lateinit var tumGonderiler : ArrayList<UserPosts>
    lateinit var sayfaBasinatumGonderiler : ArrayList<UserPosts>
    lateinit var tumTakipEttiklerim : ArrayList<String>
    private val ACTIVITY_NO = 0;
    lateinit var mAuth : FirebaseAuth
    lateinit var mAuthListener : FirebaseAuth.AuthStateListener
    lateinit var mRef : DatabaseReference
    lateinit var mUser : FirebaseUser
    val SAYFA_BASINA_GONDERI_SAYISI = 10
    var sayfaNumarasi = 1
    var sayfaSonunaGelindi = false
    var recyclerView : AutoPlayVideoRecyclerView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

     fragmentView = inflater?.inflate(R.layout.fragment_home,container,false)

        mAuth = FirebaseAuth.getInstance()
        mUser = mAuth.currentUser!!

        mRef = FirebaseDatabase.getInstance().reference
        tumGonderiler = ArrayList<UserPosts>()
        sayfaBasinatumGonderiler = ArrayList<UserPosts>()
        tumTakipEttiklerim = ArrayList<String>()

        tumTakipEttiklerimiGetir()
       // kullaniciPostlariniGetir(mUser.uid!!) //userların kullanıcı id leri yollanacak //DİKKAT




        fragmentView.imgWeather.setOnClickListener {
           // (activity as HomeActivity).ViewPagerHome.setCurrentItem(0)
                val intent = Intent(context, WeatherActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                this.startActivity(intent)

        }

        fragmentView.imageViewProfileTopSettings.setOnClickListener {
            (activity as HomeActivity).ViewPagerHome.setCurrentItem(1)
        }



        fragmentView.refreshLayout.setOnRefreshListener {

            tumGonderiler.clear()
            sayfaBasinatumGonderiler.clear()
            sayfaSonunaGelindi = false
            kullaniciPostlariniGetir()
            fragmentView.refreshLayout.setRefreshing(false)

        }




        return fragmentView
    }

    private fun tumTakipEttiklerimiGetir() {

        tumTakipEttiklerim.add(mUser.uid)//önce kendini ekledi

        mRef.child("following").child(mUser.uid).addListenerForSingleValueEvent(object :ValueEventListener
        {
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(p0: DataSnapshot?) {


                if(p0!!.getValue()!=null)
                {
                    for(ds in p0!!.children)
                    {
                        tumTakipEttiklerim.add(ds.key) //takip ettiğin kişileri tek tek dön ve listeye ekle
                    }

                    kullaniciPostlariniGetir()
                }
                else
                {}

            }

        })


    }

    private fun kullaniciPostlariniGetir() {

        for(i in 0..tumTakipEttiklerim.size -1)
        {

            mRef = FirebaseDatabase.getInstance().reference

            var kullaniciID = tumTakipEttiklerim.get(i)

            mRef.child("users").child(kullaniciID).addListenerForSingleValueEvent(object : ValueEventListener
            {

                override fun onCancelled(p0: DatabaseError?) {

                }

                override fun onDataChange(p0: DataSnapshot?) {
                    var userID = kullaniciID
                    var kullaniciAdi = p0!!.getValue(Users::class.java)!!.user_name
                    var kullaniciFotoURL = p0!!.getValue(Users::class.java)!!.user_detail!!.profilResmi!!
                    mRef.child("posts").child(kullaniciID).addListenerForSingleValueEvent(object : ValueEventListener
                    {
                        override fun onCancelled(p0: DatabaseError?) {

                        }

                        override fun onDataChange(p0: DataSnapshot?) {
                            if(p0!!.hasChildren())
                            {
                                for(ds in p0!!.children)
                                {
                                    var eklenecekUserPosts = UserPosts()
                                    eklenecekUserPosts.UserID = userID
                                    eklenecekUserPosts.UserName = kullaniciAdi
                                    eklenecekUserPosts.UserPhotoURL = kullaniciFotoURL
                                    eklenecekUserPosts.postID = ds.getValue(Posts::class.java)!!.post_id
                                    eklenecekUserPosts.postURL = ds.getValue(Posts::class.java)!!.photo_url
                                    eklenecekUserPosts.postAciklama = ds.getValue(Posts::class.java)!!.aciklama
                                    eklenecekUserPosts.postYuklenmeTarih = ds.getValue(Posts::class.java)!!.yuklenme_tarihi
                                    tumGonderiler.add(eklenecekUserPosts)
                                }
                            }
                            else
                            {}

                            if(i>= tumTakipEttiklerim.size-1) //tum takipçiler geldikten sonra recycler view dolsun
                            setupRecyclerView()
                        }

                    })


                }

            }
            )
        }




    }

    private fun setupRecyclerView() {



        //gönderileri sırala/////////
        //son çekilen başa gelsin için
        Collections.sort(tumGonderiler,object :Comparator<UserPosts>
        {
            override fun compare(p0: UserPosts?, p1: UserPosts?): Int {
                if(p0!!.postYuklenmeTarih!! > p1!!.postYuklenmeTarih!!)
                    return -1
                else
                    return 1
            }

        }
        )
        //////////////////////////////////////////////
        var gidecek_sayi = 0
        if(tumGonderiler.size < 10)
            gidecek_sayi = tumGonderiler.size
        else
            gidecek_sayi = SAYFA_BASINA_GONDERI_SAYISI

        for(i in 0..gidecek_sayi-1)
        {
            sayfaBasinatumGonderiler.add(tumGonderiler.get(i))

        }



         recyclerView = fragmentView.recyclerview
        var recyclerAdapter = HomeFragmentRecyclerAdapter(this.activity!!,sayfaBasinatumGonderiler)
        recyclerView!!.layoutManager=CenterLayoutManager(this.activity!!,LinearLayoutManager.VERTICAL,false) //reverse layout true yaparsan sonrakiler başta baştakiler sonra gösterilecektir

        recyclerView!!.adapter = recyclerAdapter


        recyclerView!!.addOnScrollListener(object : RecyclerView.OnScrollListener()
        {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val layoutManager = recyclerView!!.layoutManager as CenterLayoutManager

                if(dy > 0 && layoutManager.findLastVisibleItemPosition() == recyclerView!!.adapter!!.itemCount - 1)
                {
                    if(!sayfaSonunaGelindi)
                    listeyeYeniElemanlarEkle()
                }
            }

        }
        )




    }

    private fun listeyeYeniElemanlarEkle() { //SAYFALAMA YAPIMI
        var gidecek_sayi = 0
        if(tumGonderiler.size < 10)
            gidecek_sayi = tumGonderiler.size
        else
            gidecek_sayi = SAYFA_BASINA_GONDERI_SAYISI
        var altSinir = (sayfaNumarasi*gidecek_sayi)
        var ustSinir = ((sayfaNumarasi+1)*gidecek_sayi - 1)
        for(i in  altSinir  .. ustSinir)
        {
            if(sayfaBasinatumGonderiler.size <= tumGonderiler.size -1 )
            {
                sayfaBasinatumGonderiler.add(tumGonderiler.get(i))
            recyclerView!!.adapter!!.notifyDataSetChanged()}
            else
            {
                sayfaSonunaGelindi = true
                sayfaNumarasi =0
                break



            }

        }

        sayfaNumarasi++
    }


    override fun onResume() {
        setupNavigationView()   //navigationviewi ata (Alt Menu)
        super.onResume()
    }

    fun setupNavigationView()
    {
        BottomNavigationViewHelper.setupBottomNavigationView(fragmentView.bottomNavigationViewim)
        BottomNavigationViewHelper.setupNavigation(activity!!,fragmentView.bottomNavigationViewim)
        var menu = fragmentView.bottomNavigationViewim.menu
        var menuitem = menu.getItem(ACTIVITY_NO)
        menuitem.setChecked(true)
    }



}