package com.besoci.sausosyal.besoci.Utils

import android.content.Context
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ProgressBar
import android.widget.TextView
import com.besoci.sausosyal.besoci.R
import kotlinx.android.synthetic.main.tek_sutun_grid_resim.view.*

class ShareActivityGridViewAdapter(context: Context, resource: Int,
                                   var klasordekiDosyalar: ArrayList<String>) :
    ArrayAdapter<String>(context, resource, klasordekiDosyalar) {
     var inflater : LayoutInflater
    lateinit var viewholder : ViewHolder
    var tek_sutun_resim : View? = null

    init {
       inflater = LayoutInflater.from(context)
    }

inner class ViewHolder ()
{
    lateinit var imageview:GridViewImage
   // lateinit var progressbar:ProgressBar
    lateinit var tv_sure :TextView
}

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {


         tek_sutun_resim = convertView



        if(tek_sutun_resim == null)
        {

            tek_sutun_resim = inflater.inflate(R.layout.tek_sutun_grid_resim,parent,false)
viewholder = ViewHolder()
            viewholder.imageview = tek_sutun_resim!!.imageViewtekSutun
           // viewholder.progressbar = tek_sutun_resim!!.progressBarTekSutun
            viewholder.tv_sure = tek_sutun_resim!!.textViewVideoSure


            tek_sutun_resim!!.setTag(viewholder)
        }
        else
        {
viewholder = tek_sutun_resim!!.getTag() as ViewHolder

        }

               var dosyaYolu = klasordekiDosyalar.get(position)
                if( dosyaYolu.contains(".")) {
               var dosyaTuru = dosyaYolu.substring(dosyaYolu.lastIndexOf("."))





        if (dosyaTuru != null) {
            if (dosyaTuru.equals(".mp4"))
            {
                viewholder.tv_sure.visibility = View.VISIBLE
                var retriever = MediaMetadataRetriever()
                retriever.setDataSource(context, Uri.parse("file://"+dosyaYolu))
                var videoSuresi = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
                var videoSuresiLong = videoSuresi.toLong()

                viewholder.tv_sure.setText(convertDuration(videoSuresiLong))
            }
            else
            {
                viewholder.tv_sure.visibility = View.GONE

            }

        }
        }







        UniversalImageLoader.setImage(klasordekiDosyalar.get(position),viewholder.imageview,null,"file://")

        return tek_sutun_resim!!

    }


    fun convertDuration(duration: Long): String {
        val second =duration/1000%60
        val minute:Long =duration/(1000*60)%60
        val hour =duration/(1000*60*60)%24
        var time=""
        if(hour>0)
        {
            time = String.format("%02d:%02d:%02d",hour,minute,second)
        }
        else
        {
            time = String.format("%02d:%02d",minute,second)

        }

return time

    }




}