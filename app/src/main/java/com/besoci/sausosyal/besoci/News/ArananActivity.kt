package com.besoci.sausosyal.besoci.News

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.besoci.sausosyal.besoci.Models.Posts
import com.besoci.sausosyal.besoci.Models.UserPosts
import com.besoci.sausosyal.besoci.Models.Users
import com.besoci.sausosyal.besoci.R
import com.besoci.sausosyal.besoci.Utils.BottomNavigationViewHelper
import com.besoci.sausosyal.besoci.Utils.HomeFragmentRecyclerAdapter
import com.besoci.sausosyal.besoci.VideoRecyclerView.view.CenterLayoutManager
import com.besoci.sausosyal.besoci.Weather.WeatherActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.hoanganhtuan95ptit.autoplayvideorecyclerview.AutoPlayVideoRecyclerView
import kotlinx.android.synthetic.main.activity_aranan.*
import kotlinx.android.synthetic.main.activity_home.*
import java.util.*

class ArananActivity : AppCompatActivity() {

    lateinit var tumGonderiler : ArrayList<UserPosts>
    lateinit var sayfaBasinatumGonderiler : ArrayList<UserPosts>
    lateinit var tumTakipEttiklerim : ArrayList<String>
    private val ACTIVITY_NO = 3;
    lateinit var mAuth : FirebaseAuth
    lateinit var mAuthListener : FirebaseAuth.AuthStateListener
    lateinit var mRef : DatabaseReference
    lateinit var mUser : FirebaseUser
    val SAYFA_BASINA_GONDERI_SAYISI = 10
    var sayfaNumarasi = 1
    var sayfaSonunaGelindi = false
    var recyclerView : AutoPlayVideoRecyclerView? = null
    var e_acikalan :Boolean = false
    var e_aile :Boolean = false
    var e_alisveris :Boolean = false
    var e_cocuk :Boolean = false
    var e_diger :Boolean = false
    var e_kapalialan :Boolean = false
    var e_kultursanat :Boolean = false
    var e_ogrenci :Boolean = false
    var e_piknik :Boolean = false
    var e_sevgili :Boolean = false
    var e_spor :Boolean = false
    var e_yalniz :Boolean = false
    var e_yemek :Boolean = false
    var e_yetiskin :Boolean = false
    var emekan_tipi:String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_aranan)

        e_acikalan = intent.extras.getBoolean("e_acikalan")
        e_aile=  intent.extras.getBoolean("e_aile")
        e_alisveris=  intent.extras.getBoolean("e_alisveris")
        e_cocuk =  intent.extras.getBoolean("e_cocuk")
        e_diger =  intent.extras.getBoolean("e_diger")
        e_kapalialan=  intent.extras.getBoolean("e_kapalialan")
        e_kultursanat =  intent.extras.getBoolean("e_kultursanat")
        e_ogrenci=  intent.extras.getBoolean("e_ogrenci")
        e_piknik=  intent.extras.getBoolean("e_piknik")
        e_sevgili=  intent.extras.getBoolean("e_sevgili")
        e_spor=  intent.extras.getBoolean("e_spor")
        e_yalniz=  intent.extras.getBoolean("e_yalniz")
        e_yemek=  intent.extras.getBoolean("e_yemek")
        e_yetiskin=  intent.extras.getBoolean("e_yetiskin")



        mAuth = FirebaseAuth.getInstance()
        mUser = mAuth.currentUser!!

        mRef = FirebaseDatabase.getInstance().reference
        tumGonderiler = ArrayList<UserPosts>()
        sayfaBasinatumGonderiler = ArrayList<UserPosts>()
        tumTakipEttiklerim = ArrayList<String>()

        tumTakipEttiklerimiGetir()
        // kullaniciPostlariniGetir(mUser.uid!!)




        imgWeather.setOnClickListener {
            // (activity as HomeActivity).ViewPagerHome.setCurrentItem(0)
            val intent = Intent(this, WeatherActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            this.startActivity(intent)

        }

        imageViewProfileTopSettings.setOnClickListener {
            this.ViewPagerHome.setCurrentItem(1)
        }



        refreshLayout.setOnRefreshListener {

            tumGonderiler.clear()
            sayfaBasinatumGonderiler.clear()
            sayfaSonunaGelindi = false
            kullaniciPostlariniGetir()
            refreshLayout.setRefreshing(false)

        }





    }



    private fun tumTakipEttiklerimiGetir() {

        tumTakipEttiklerim.add(mUser.uid)

        mRef.child("following").child(mUser.uid).addListenerForSingleValueEvent(object : ValueEventListener
        {
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(p0: DataSnapshot?) {


                if(p0!!.getValue()!=null)
                {
                    for(ds in p0!!.children)
                    {
                        tumTakipEttiklerim.add(ds.key) //takip etti�in ki�ileri tek tek d�n ve listeye ekle
                    }

                    kullaniciPostlariniGetir()
                }
                else
                {}

            }

        })


    }

    private fun kullaniciPostlariniGetir() {

        for(i in 0..tumTakipEttiklerim.size -1)
        {

            mRef = FirebaseDatabase.getInstance().reference

            var kullaniciID = tumTakipEttiklerim.get(i)

            mRef.child("users").child(kullaniciID).addListenerForSingleValueEvent(object : ValueEventListener
            {

                override fun onCancelled(p0: DatabaseError?) {

                }

                override fun onDataChange(p0: DataSnapshot?) {
                    var userID = kullaniciID
                    var kullaniciAdi = p0!!.getValue(Users::class.java)!!.user_name
                    var kullaniciFotoURL = p0!!.getValue(Users::class.java)!!.user_detail!!.profilResmi!!
                    mRef.child("posts").child(kullaniciID).addListenerForSingleValueEvent(object : ValueEventListener
                    {
                        override fun onCancelled(p0: DatabaseError?) {

                        }

                        override fun onDataChange(p0: DataSnapshot?) {
                            if(p0!!.hasChildren())
                            {
                                for(ds in p0!!.children)
                                {
                                    var eklenecekUserPosts = UserPosts()
                                    eklenecekUserPosts.UserID = userID
                                    eklenecekUserPosts.UserName = kullaniciAdi
                                    eklenecekUserPosts.UserPhotoURL = kullaniciFotoURL
                                    eklenecekUserPosts.postID = ds.getValue(Posts::class.java)!!.post_id
                                    eklenecekUserPosts.postURL = ds.getValue(Posts::class.java)!!.photo_url
                                    eklenecekUserPosts.postAciklama = ds.getValue(Posts::class.java)!!.aciklama
                                    eklenecekUserPosts.postYuklenmeTarih = ds.getValue(Posts::class.java)!!.yuklenme_tarihi



                                    var mekan_tipi =  ds.child("mekan_tipi").getValue()
                                    var acikalan = false
                                    if(mekan_tipi.toString().equals("Acik Alan"))  acikalan=true
                                    var kapalialan = false
                                    if(mekan_tipi.toString().equals("Kapali Alan"))  kapalialan=true




                                    var kategori =  ds.child("kategori").getValue()
                                    var piknikkamp = false
                                    if(kategori.toString().equals("Piknik-Kamp"))  piknikkamp=true
                                    var kultursanat = false
                                    if(kategori.toString().equals("Kultur-Sanat"))  kultursanat=true
                                    var yemek = false
                                    if(kategori.toString().equals("Yemek"))  yemek=true
                                    var spor = false
                                    if(kategori.toString().equals("Spor"))  spor=true
                                    var alisveris = false
                                    if(kategori.toString().equals("Alisveris"))  alisveris=true
                                    var diger = false
                                    if(kategori.toString().equals("Diger"))  diger=true
                                    var ogrenci =  ds.child("ogrenci").getValue()
                                    var cocuk =  ds.child("cocuk").getValue()
                                    var aile =  ds.child("aile").getValue()
                                    var yetiskin =  ds.child("yetiskin").getValue()
                                    var sevgili =  ds.child("sevgili").getValue()
                                    var yalniz =  ds.child("yalniz").getValue()











                                    if( ( (e_acikalan == acikalan && acikalan == true) || (e_kapalialan==kapalialan && e_kapalialan==true) )
                                        &&
                                        (
                                                (e_yemek == yemek && yemek == true) ||  (e_alisveris == alisveris && alisveris ==  true) || (e_spor == spor && spor == true)
                                        || (e_piknik ==piknikkamp && piknikkamp == true) || (e_kultursanat == kultursanat && kultursanat == true) || (e_diger == diger && diger == true) )
                                        &&
                                        (
                                                (e_sevgili == sevgili && sevgili == true)
                                                ||
                                                        (e_ogrenci == ogrenci && ogrenci == true)
                                                ||
                                                        (e_aile == aile && aile == true)
                                                ||
                                                        (e_yetiskin == yetiskin && yetiskin == true)
                                                ||
                                                        (e_cocuk == cocuk && cocuk == true)
                                                ||
                                                        (e_yalniz == yalniz && yalniz==true)

                                                )


                                    )
                                    {
                                        tumGonderiler.add(eklenecekUserPosts)
                                    }




                                }
                            }


                            if(i>= tumTakipEttiklerim.size-1)
                                setupRecyclerView()
                        }

                    })


                }

            }
            )
        }




    }

    private fun setupRecyclerView() {




        Collections.sort(tumGonderiler,object :Comparator<UserPosts>
        {
            override fun compare(p0: UserPosts?, p1: UserPosts?): Int {
                if(p0!!.postYuklenmeTarih!! > p1!!.postYuklenmeTarih!!)
                    return -1
                else
                    return 1
            }

        }
        )
        //////////////////////////////////////////////


               var gidecek_sayi = 0
        if(tumGonderiler.size < 10)
            gidecek_sayi = tumGonderiler.size
        else
            gidecek_sayi = SAYFA_BASINA_GONDERI_SAYISI

        for(i in 0..gidecek_sayi-1)
        {
            sayfaBasinatumGonderiler.add(tumGonderiler.get(i))

        }



        recyclerView = recyclerview
        var recyclerAdapter = HomeFragmentRecyclerAdapter(this,sayfaBasinatumGonderiler)
        recyclerView!!.layoutManager= CenterLayoutManager(this, LinearLayoutManager.VERTICAL,false) //reverse layout true yaparsan sonrakiler ba�ta ba�takiler sonra g�sterilecektir

        recyclerView!!.adapter = recyclerAdapter


        recyclerView!!.addOnScrollListener(object : RecyclerView.OnScrollListener()
        {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val layoutManager = recyclerView!!.layoutManager as CenterLayoutManager

                if(dy > 0 && layoutManager.findLastVisibleItemPosition() == recyclerView!!.adapter!!.itemCount - 1)
                {
                    if(!sayfaSonunaGelindi)
                        listeyeYeniElemanlarEkle()
                }
            }

        }
        )




    }

    private fun listeyeYeniElemanlarEkle() { //SAYFALAMA YAPIMI

        var gidecek_sayi = 0
        if(tumGonderiler.size < 10)
            gidecek_sayi = tumGonderiler.size
        else
            gidecek_sayi = SAYFA_BASINA_GONDERI_SAYISI


        var altSinir = (sayfaNumarasi*gidecek_sayi)
        var ustSinir = ((sayfaNumarasi+1)*gidecek_sayi - 1)
        for(i in  altSinir  .. ustSinir)
        {
            if(sayfaBasinatumGonderiler.size <= tumGonderiler.size -1 )
            {
                sayfaBasinatumGonderiler.add(tumGonderiler.get(i))
                recyclerView!!.adapter!!.notifyDataSetChanged()}
            else
            {
                sayfaSonunaGelindi = true
                sayfaNumarasi =0
                break



            }

        }

        sayfaNumarasi++
    }


    override fun onResume() {
        setupNavigationView()   //navigationviewi ata (Alt Menu)
        super.onResume()
    }

    fun setupNavigationView()
    {
        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewim)
        BottomNavigationViewHelper.setupNavigation(this,bottomNavigationViewim)
        var menu = bottomNavigationViewim.menu
        var menuitem = menu.getItem(ACTIVITY_NO)
        menuitem.setChecked(true)
    }

}
