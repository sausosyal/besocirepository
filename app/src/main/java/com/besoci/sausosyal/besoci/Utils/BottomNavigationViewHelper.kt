package com.besoci.sausosyal.besoci.Utils

import android.content.Context
import android.content.Intent
import android.provider.ContactsContract
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.NavigationView
import android.support.swiperefreshlayout.R
import android.view.MenuItem
import com.besoci.sausosyal.besoci.Home.HomeActivity
import com.besoci.sausosyal.besoci.News.NewsActivity
import com.besoci.sausosyal.besoci.Profile.ProfileActivity
import com.besoci.sausosyal.besoci.Search.SearchActivity
import com.besoci.sausosyal.besoci.Share.ShareActivity
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx

class BottomNavigationViewHelper {


    companion object {

        fun setupBottomNavigationView(bottomNavigationViewEx: BottomNavigationViewEx)
        {
            bottomNavigationViewEx.enableShiftingMode(false);
            bottomNavigationViewEx.enableItemShiftingMode(false);
            bottomNavigationViewEx.enableAnimation(false);
            bottomNavigationViewEx.setTextVisibility(false);
        }


        fun setupNavigation(context: Context,bottomNavigationViewEx: BottomNavigationViewEx)
        {
            bottomNavigationViewEx.onNavigationItemSelectedListener = object :BottomNavigationView.OnNavigationItemSelectedListener
            {
                override fun onNavigationItemSelected(item: MenuItem): Boolean { //her öge tıklandığında bu metod tetiklenir ve item olarak tıklanan metodu alır


                    when(item.itemId) //switch case yapısı
                    {
                        com.besoci.sausosyal.besoci.R.id.ic_home ->
                        {
                            val intent =Intent(context,HomeActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                            context.startActivity(intent)
                            return true
                        }
                        com.besoci.sausosyal.besoci.R.id.ic_search ->
                        {
                            val intent =Intent(context,SearchActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                            context.startActivity(intent)
                            return true
                        }
                        com.besoci.sausosyal.besoci.R.id.ic_share ->
                        {
                            val intent =Intent(context,ShareActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                            context.startActivity(intent)
                            return true
                        }
                        com.besoci.sausosyal.besoci.R.id.ic_news ->
                        {
                            val intent =Intent(context,NewsActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                            context.startActivity(intent)
                            return true
                        }
                        com.besoci.sausosyal.besoci.R.id.ic_porfile ->
                        {
                            val intent =Intent(context,ProfileActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                            context.startActivity(intent)
                            return true
                        }



                    }

                    return false


                }

            }

        }


    }


}