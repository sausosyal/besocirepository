package com.besoci.sausosyal.besoci.Generic


import android.content.Context
import android.content.DialogInterface
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.Toast
import com.besoci.sausosyal.besoci.Models.Comments
import com.besoci.sausosyal.besoci.Models.Users

import com.besoci.sausosyal.besoci.R
import com.besoci.sausosyal.besoci.Utils.EventbusDataEvents
import com.besoci.sausosyal.besoci.Utils.TimeAgo
import com.besoci.sausosyal.besoci.Utils.UniversalImageLoader
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.android.gms.auth.api.Auth
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_comment.*
import kotlinx.android.synthetic.main.fragment_comment.view.*
import kotlinx.android.synthetic.main.tek_satir_comment_item.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.w3c.dom.Comment


class CommentFragment : Fragment() {

    var yorumYapilacakGonderininID:String?=null
    lateinit var mAuth : FirebaseAuth
    lateinit var mRef : DatabaseReference
    lateinit var mRef2 : DatabaseReference
    lateinit var mUser : FirebaseUser
    lateinit var fragmentView : View
    lateinit var myAdapter: FirebaseRecyclerAdapter<Comments,CommentViewHolder>
    val IC_ONAY = "https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fic_onay.png?alt=media&token=92bddce5-58e3-427a-8f55-d8c8482e597a"
    val IC_ONAY_YESIL = "https://firebasestorage.googleapis.com/v0/b/trasqu-faed9.appspot.com/o/drawable%2Fic_onay_yesil.png?alt=media&token=f754f63c-8b46-4dd6-ba9e-1a535ea1f436"


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
         fragmentView = inflater.inflate(R.layout.fragment_comment, container, false)


        mAuth=FirebaseAuth.getInstance()
        mUser=mAuth.currentUser!!

        setupCommentRecyclerView()

        setupProfilePicture()

        fragmentView.yorumGonder.setOnClickListener {

          //any dedim çünkü tüm tipler olabilir bizde de string ve long var
            var yeniYorum = hashMapOf<String,Any>(
                "user_id" to mUser.uid,
                "yorum" to fragmentView.editTextyorum.text.toString(),
                "yorum_begeni" to "0",
                "yorum_tarih" to ServerValue.TIMESTAMP)


            FirebaseDatabase.getInstance().getReference().child("comments").child(yorumYapilacakGonderininID).push().setValue(yeniYorum)

            fragmentView.editTextyorum.setText("")

            fragmentView.yorumlarRecyclerView.smoothScrollToPosition(fragmentView.yorumlarRecyclerView.adapter!!.itemCount)



        }


        fragmentView.CommentCloseIcon.setOnClickListener {
            activity!!.onBackPressed()
        }

        return fragmentView
    }

    private fun setupCommentRecyclerView() {
        mRef=FirebaseDatabase.getInstance().reference.child("comments").child(yorumYapilacakGonderininID )

        //Hangi Düğümü Dinleyeceğimizi seçiyoruz
        val options = FirebaseRecyclerOptions.Builder<Comments>().setQuery(mRef,Comments::class.java).build()

        myAdapter = object :FirebaseRecyclerAdapter<Comments,CommentViewHolder>(options)
        {
            override fun onCreateViewHolder(p0: ViewGroup, p1: Int): CommentViewHolder {
                var commentViewHolder=LayoutInflater.from(p0.context).inflate(R.layout.tek_satir_comment_item,p0,false)
                return CommentViewHolder(commentViewHolder )
            }

            override fun onBindViewHolder(holder: CommentViewHolder, position: Int, model: Comments) {

                holder.setData(model)

                //ilk yorum açıklama ise beğen iconu kaybolsun
                if(position == 0 && (yorumYapilacakGonderininID!!.equals(getRef(0).key)))
                {
                    holder.onayIcon.visibility = View.GONE
                    holder.onaySayisi.visibility = View.GONE

                }
                
                holder.setBegenOlayi(yorumYapilacakGonderininID,getRef(position).key)
                holder.setBegenDurumu(yorumYapilacakGonderininID,getRef(position).key)


            }

        }


        fragmentView.yorumlarRecyclerView.adapter=myAdapter
        fragmentView.yorumlarRecyclerView.layoutManager = LinearLayoutManager(activity,LinearLayoutManager.VERTICAL,false)
    }

    private fun setupProfilePicture() {
        mRef2 = FirebaseDatabase.getInstance().reference.child("users")
            mRef2.child(mUser.uid).child("user_detail").addListenerForSingleValueEvent(object : ValueEventListener
            {
                override fun onCancelled(p0: DatabaseError?) {

                }

                override fun onDataChange(p0: DataSnapshot?) {

                    var profilePictureUrl = p0!!.child("profilResmi").getValue().toString()
                    UniversalImageLoader.setImage(profilePictureUrl,yorumYapResim,null,"")



                }

            }
            )
    }

    inner class CommentViewHolder(itemView: View?) :RecyclerView.ViewHolder(itemView!!){

        var tumCommentLayoutu : ConstraintLayout=itemView as ConstraintLayout
        var yorumYapanUserPhoto= tumCommentLayoutu.tekCommentProfilePhoto
        var kullaniciAdi= tumCommentLayoutu.tekCommentUsername
        var yorum= tumCommentLayoutu.tekCommentYorum
        var onaySayisi= tumCommentLayoutu.tekCommentOnaySayisi
        var yorumSure= tumCommentLayoutu.tekCommentYorumSure
        var onayIcon= tumCommentLayoutu.tekCommentOnayButton
        var a:String=""



        fun setData(oAnOlusturulanYorum:Comments)
        {
            yorum.setText(oAnOlusturulanYorum.yorum)
            yorumSure.setText(""+TimeAgo.getTimeAgo(oAnOlusturulanYorum.yorum_tarih!!))
            onaySayisi.setText(oAnOlusturulanYorum.yorum_begeni)

            kullaniciBilgileriniGetir(oAnOlusturulanYorum.user_id)




        }

        private fun kullaniciBilgileriniGetir(user_id: String?) {

            var mRef=FirebaseDatabase.getInstance().reference
            mRef.child("users").child(user_id).addListenerForSingleValueEvent(object :ValueEventListener
            {
                override fun onCancelled(p0: DatabaseError?) {

                }

                override fun onDataChange(p0: DataSnapshot?) {

                    kullaniciAdi.setText(p0!!.getValue(Users::class.java)!!.user_name)
                    UniversalImageLoader.setImage(p0!!.getValue(Users::class.java)!!.user_detail!!.profilResmi!!.toString(),yorumYapanUserPhoto,null,"")



                }

            }
            )
        }

        fun setBegenOlayi(yorumYapilacakGonderininID: String?, begenlecekYorumID: String?) {

            var mRef = FirebaseDatabase.getInstance().reference.child("comments").child(yorumYapilacakGonderininID).child(begenlecekYorumID)





            onayIcon.setOnClickListener {




                //BEĞENİ KONTROL//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                mRef.child("begenenler").addListenerForSingleValueEvent(object : ValueEventListener
                {
                    override fun onCancelled(p0: DatabaseError?) {

                    }

                    override fun onDataChange(p0: DataSnapshot?) {

                        //DAHA ÖNCEDEN BEĞENMİŞ MİYİM/////////////////////
                        if(p0!!.hasChild(FirebaseAuth.getInstance().currentUser!!.uid))
                        {
                            mRef.child("begenenler").child(FirebaseAuth.getInstance().currentUser!!.uid).removeValue() //beğenmişsem beğenmemi kaldır
                            Picasso.with(context).load(IC_ONAY).into(onayIcon)

                         //   onayIcon.setImageResource(IC_ONAY)

                        }
                         //BEĞENMEMİŞİM////////////////////////
                        else
                        {
                            mRef.child("begenenler").child(FirebaseAuth.getInstance().currentUser!!.uid).setValue(FirebaseAuth.getInstance().currentUser!!.uid)
                          // onayIcon.setImageResource(R.drawable.ic_onay_yesil)
                            Picasso.with(context).load(IC_ONAY_YESIL).into(onayIcon)


                        }
                    }

                })

                /////////////////////////////////////////////////////////////////////////////////////////////////
            }


        }

        fun setBegenDurumu(yorumYapilacakGonderininID: String?, begenlecekYorumID: String?) {
            var mRef = FirebaseDatabase.getInstance().reference.child("comments").child(yorumYapilacakGonderininID).child(begenlecekYorumID)
            //BEĞENİ KONTROL//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            mRef.child("begenenler").addListenerForSingleValueEvent(object : ValueEventListener
            {
                override fun onCancelled(p0: DatabaseError?) {

                }

                override fun onDataChange(p0: DataSnapshot?) {

                    //BEĞENİ SAYISI GETİR
                    if(p0!!.exists())
                    {
                        onaySayisi.setText(p0!!.childrenCount.toString() +" Onay")
                    }
                    else
                    {
                        onaySayisi.setText("Onay Yok")
                    }

                    //DAHA ÖNCEDEN BEĞENMİŞ MİYİM/////////////////////
                    if(p0!!.hasChild(FirebaseAuth.getInstance().currentUser!!.uid))
                    {
                      //  onayIcon.setImageResource(R.drawable.ic_onay_yesil)
                        Picasso.with(context).load(IC_ONAY_YESIL).into(onayIcon)


                    }
                    //BEĞENMEMİŞİM////////////////////////
                    else
                    {
                   //     onayIcon.setImageResource(R.drawable.ic_onay)
                        Picasso.with(context).load(IC_ONAY).into(onayIcon)


                    }
                }

            })

            /////////////////////////////////////////////////////////////////////////////////////////////////
        }



        init {
            tumCommentLayoutu.setOnLongClickListener {
                var popup: PopupMenu? = null;
                popup = PopupMenu(context, yorum)
                popup.inflate(R.menu.act_menu)

                popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item: MenuItem? ->

                    when (item!!.itemId) {
                        R.id.header1 -> {



                       //     val intent =  Intent(myContext , PostGuncelleActivity::class.java)
                       //     intent.putExtra("OankiPost",oAnkiGonderi.postID)
                       //     intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)

                        //    myContext.applicationContext.startActivity(intent)
                            Toast.makeText(context,"Uzgunuz Yorum Guncelleme Ozelligi Gecici Olarak Kullanim Disidir",Toast.LENGTH_SHORT).show()



                         //   Toast.makeText(myContext, item.title, Toast.LENGTH_SHORT).show();
                        }
                        R.id.header2 -> { //SIL


                            val builder = AlertDialog.Builder(context!!)
                            builder.setTitle("SİL")
                            builder.setMessage("Silmek istediğinizden emin misiniz?")
                            builder.setPositiveButton("İPTAL", object : DialogInterface.OnClickListener {
                                override fun onClick(dialog: DialogInterface, id: Int) {

                                }
                            })
                            builder.setNegativeButton("SİL", object : DialogInterface.OnClickListener {
                                override fun onClick(dialog: DialogInterface, id: Int) {

                                  //  var mRefComments : DatabaseReference = FirebaseDatabase.getInstance().reference
                                   // var mAuth=FirebaseAuth.getInstance()
                                    //mRefComments.child("comments").child(yorumYapilacakGonderininID.toString()).child("").removeValue()   //yorumlardan da sil
Toast.makeText(context,"Uzgunuz Yorum Silme Ozelligi Gecici Olarak Kullanim Disidir",Toast.LENGTH_SHORT).show()

                                //    val intent =  Intent(myContext , ProfileActivity::class.java)
                                 //   intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)

                                 //   myContext.applicationContext.startActivity(intent)*/
                                }
                            })
                            builder.show()















                        }

                    }

                    true
                })
                var mRefka : DatabaseReference = FirebaseDatabase.getInstance().reference
                mRefka.child("users").child(mUser.uid).child("user_name").addListenerForSingleValueEvent(object : ValueEventListener
                {
                    override fun onCancelled(p0: DatabaseError?) {
                    }

                    override fun onDataChange(p0: DataSnapshot?) {
                        if(kullaniciAdi.text.equals( p0!!.getValue().toString() ))
                                {
                            popup.show()
                            }
                        else
                        {
                          //  Toast.makeText(context, "Sadece KEndi Yorumuna",Toast.LENGTH_SHORT).show()
                        }
                    }
                })

          //      if(kullaniciAdi.equals(   mRefka.child("users").child(mUser.uid).child("user_name").toString() ) )
            //        {
                 //   popup.show()
             //   Toast.makeText(context,mRefka.child("users").child(mUser.uid).child("user_name").toString(),Toast.LENGTH_SHORT).show()
            //    }



                false//Bu satır silinmeyecek
            }


        }

        //BURAYA GELECEK DENEYELIM
        /////////////////////////////////////////////////////


       /* tumCommentLayoutu.setOnLongClickListener {




            var popup: PopupMenu? = null;
            popup = PopupMenu(myContext, tekSutunDosya)
            popup.inflate(R.menu.act_menu)

            popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item: MenuItem? ->

                when (item!!.itemId) {
                    R.id.header1 -> {



                        val intent =  Intent(myContext , PostGuncelleActivity::class.java)
                        intent.putExtra("OankiPost",oAnkiGonderi.postID)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)

                        myContext.applicationContext.startActivity(intent)



                        Toast.makeText(myContext, item.title, Toast.LENGTH_SHORT).show();
                    }
                    R.id.header2 -> { //SIL


                        val builder = AlertDialog.Builder(myContext)
                        builder.setTitle("SİL")
                        builder.setMessage("Silmek istediğinizden emin misiniz?")
                        builder.setPositiveButton("İPTAL", object : DialogInterface.OnClickListener {
                            override fun onClick(dialog: DialogInterface, id: Int) {

                            }
                        })
                        builder.setNegativeButton("SİL", object : DialogInterface.OnClickListener {
                            override fun onClick(dialog: DialogInterface, id: Int) {

                                var mRef : DatabaseReference = FirebaseDatabase.getInstance().reference
                                var mRefComments : DatabaseReference = FirebaseDatabase.getInstance().reference
                                var mRefLikes : DatabaseReference = FirebaseDatabase.getInstance().reference
                                var mAuth=FirebaseAuth.getInstance()
                                var mUser : FirebaseUser = mAuth!!.currentUser!!


                                mRef.child("posts").child(mUser.uid).child(oAnkiGonderi.postID.toString()).removeValue() //postu sil
                                mRef.child("onaylar").child(oAnkiGonderi.postID.toString()).removeValue()  //likelardan da sil
                                mRef.child("comments").child(oAnkiGonderi.postID.toString()).removeValue()   //yorumlardan da sil


                                postSayisiniGuncelle()
                                val intent =  Intent(myContext , ProfileActivity::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)

                                myContext.applicationContext.startActivity(intent)
                            }
                        })
                        builder.show()















                    }

                }

                true
            })
            if(kendimi) //kendisi ise popup çıksın
            {
                popup.show()
            }



            false//Bu satır silinmeyecek
        }
        /////////////////////////////////////////////////////
*/








    }

    @Subscribe(sticky = true) //eventbustan poststicky ile bişey gönderilmişse yakala
    internal fun onYorumYapilacakGonderi(gonderi : EventbusDataEvents.YorumYapilacakGonderininIDsiniGonder)//benim oluşturduğum classdan geldi
    {


        yorumYapilacakGonderininID = gonderi!!.gonderiID!!

    }






    override fun onAttach(context: Context?) {
        super.onAttach(context)
        EventBus.getDefault().register(this)
    }

    override fun onDetach() {
        super.onDetach()
        EventBus.getDefault().unregister(this)

    }

    override fun onStart() {
        super.onStart()
        myAdapter.startListening()
    }

    override fun onStop() {
        super.onStop()
        myAdapter.stopListening()

    }
}
