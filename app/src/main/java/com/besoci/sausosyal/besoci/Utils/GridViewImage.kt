package com.besoci.sausosyal.besoci.Utils

import android.content.Context
import android.support.v7.widget.AppCompatImageView
import android.util.AttributeSet

class GridViewImage  : AppCompatImageView {


    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec) //genişlik neyse yükseklik o olsun dedik
    }

}