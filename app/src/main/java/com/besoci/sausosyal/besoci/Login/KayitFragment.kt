package com.besoci.sausosyal.besoci.Login


import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.besoci.sausosyal.besoci.Login.RegisterActivity
import com.besoci.sausosyal.besoci.Models.UserDetails
import com.besoci.sausosyal.besoci.Models.Users

import com.besoci.sausosyal.besoci.R
import com.besoci.sausosyal.besoci.Utils.EventbusDataEvents
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.fragment_kayit.*
import kotlinx.android.synthetic.main.fragment_kayit.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe


class KayitFragment : Fragment() {
    var verificationID =""
    var gelenkod = ""
    var telno = ""
    var gelenMail=""
    var isEmail:Boolean = true
    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null
    private var mAuth: FirebaseAuth? = null
    var adsoyad =""
    var kullaniciadi=""
    var sifre = ""
    var userid = ""
    var kaydedilecekKullaniciDetay:UserDetails? = null
    var activity: RegisterActivity? = null
  private var mProgressBar: ProgressDialog? = null



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_kayit, container, false)


        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!.child("users")
        mAuth = FirebaseAuth.getInstance()

     //bence kalmalı aşağıdaki
        if(mAuth!!.currentUser != null)
        {
            mAuth!!.signOut()
        }


       mProgressBar = ProgressDialog(activity!!)


        view.buttonum.setOnClickListener {


            if (FragmentKayitEditTextAdSoyad.length() < 6 ||
                fragmentKayitEditTextKullaniciAdi.length() < 6 ||
                fragmentKayitEditTextSifre.length() < 6
            ) {
                Toast.makeText(
                    activity!!,
                    "Alanlar en az 6 karakterden olusmalidir",
                    Toast.LENGTH_SHORT
                ).show()
            } else {


            var kullaniciAdiKullanimdaMi = false
            mDatabaseReference!!.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError?) {


                }

                override fun onDataChange(p0: DataSnapshot?) {

                    if (p0!!.getValue() != null) {

                        for (user in p0!!.children) {

                            var okunanKullanici = user.getValue(Users::class.java)
                            if (okunanKullanici!!.user_name.equals(fragmentKayitEditTextKullaniciAdi.text.toString())) {

                                Toast.makeText(
                                    activity!!,
                                    "Bu kullanıcı adı  önceden alınmış.Lütfen farklı bir kullanıcı adı giriniz.",
                                    Toast.LENGTH_SHORT
                                ).show()
                                kullaniciAdiKullanimdaMi = true
                                break

                            }


                        }

                        if (!kullaniciAdiKullanimdaMi) {

                            sifre = view.fragmentKayitEditTextSifre.text.toString()
                            kullaniciadi = view.fragmentKayitEditTextKullaniciAdi.text.toString()
                            adsoyad = view.FragmentKayitEditTextAdSoyad.text.toString()



                            if (isEmail) //KULLANICI MAIL ILE KAYIT OLMAK ISTIYOR
                            {
                                mProgressBar!!.setTitle("Lütfen Bekleyiniz")
                                mProgressBar!!.setMessage("Kaydınız Gerçekleştiriliyor")
                                mProgressBar!!.show()
                                mAuth!!.createUserWithEmailAndPassword(gelenMail, sifre)
                                    .addOnCompleteListener(object : OnCompleteListener<AuthResult> {

                                        override fun onComplete(p0: Task<AuthResult>) {

                                            mProgressBar!!.hide()

                                            if (p0.isSuccessful) {
                                                userid = mAuth!!.currentUser!!.uid
                                                kaydedilecekKullaniciDetay = UserDetails("0", "0", "0", "", "", "")

                                                var kaydedilecekKullanici = Users(
                                                    gelenMail,
                                                    sifre,
                                                    kullaniciadi,
                                                    adsoyad,
                                                    "",
                                                    "",
                                                    userid,
                                                    kaydedilecekKullaniciDetay
                                                )
                                                //Verify Email
                                                verifyEmail();
                                                //update user profile information
                                                val currentUserDb = mDatabaseReference!!.child(userid)
                                                currentUserDb.setValue(kaydedilecekKullanici).addOnCompleteListener(
                                                    object : OnCompleteListener<Void> {
                                                        override fun onComplete(p0: Task<Void>) {
                                                            if (!p0!!.isSuccessful) //authanticationda kullanıcı oluştu ama realtimedatabase de oluşamadıysa authanticationdaki kullanıcıyı silelim
                                                            {
                                                                mAuth!!.currentUser!!.delete()
                                                                Toast.makeText(
                                                                    activity!!, "KULLANICI KAYDEDİLEMEDİ",
                                                                    Toast.LENGTH_SHORT
                                                                ).show()
                                                            }
                                                        }

                                                    }

                                                )





                                                updateUserInfoAndUI()

                                            } else {
                                                Log.w("TAG", "createUserWithEmail:failure", p0.exception)
                                                Toast.makeText(
                                                    activity!!, "Authentication failed.",
                                                    Toast.LENGTH_SHORT
                                                ).show()
                                            }
                                        }


                                    }


                                    )


                            } else       //KULLANICI TELEFON ILE KAYIT OLMAK ISTIYOR
                            {
                                mProgressBar!!.setTitle("Lütfen Bekleyiniz")
                                mProgressBar!!.setMessage("Kaydınız Gerçekleştiriliyor")
                                mProgressBar!!.show()
                                var telefonIleKayitAdresi: String = telno + "@telefonilekayit.com"
                                mAuth!!.createUserWithEmailAndPassword(telefonIleKayitAdresi, sifre)
                                    .addOnCompleteListener(object : OnCompleteListener<AuthResult> {

                                        override fun onComplete(p0: Task<AuthResult>) {

                                            mProgressBar!!.hide()

                                            if (p0.isSuccessful) {
                                                userid = mAuth!!.currentUser!!.uid
                                                kaydedilecekKullaniciDetay = UserDetails("0", "0", "0", "", "", "")

                                                var kaydedilecekKullanici = Users(
                                                    "",
                                                    sifre,
                                                    kullaniciadi,
                                                    adsoyad,
                                                    telno,
                                                    telefonIleKayitAdresi,
                                                    userid,
                                                    kaydedilecekKullaniciDetay
                                                )
                                                //update user profile information
                                                val currentUserDb = mDatabaseReference!!.child(userid)
                                                currentUserDb.setValue(kaydedilecekKullanici).addOnCompleteListener(
                                                    object : OnCompleteListener<Void> {
                                                        override fun onComplete(p0: Task<Void>) {
                                                            if (!p0!!.isSuccessful) //authanticationda kullanıcı oluştu ama realtimedatabase de oluşamadıysa authanticationdaki kullanıcıyı silelim
                                                            {
                                                                mAuth!!.currentUser!!.delete()
                                                                Toast.makeText(
                                                                    activity!!, "KULLANICI KAYDEDİLEMEDİ",
                                                                    Toast.LENGTH_SHORT
                                                                ).show()
                                                            }
                                                        }

                                                    }

                                                )
                                                updateUserInfoAndUI()

                                            } else {
                                                Log.w("TAG", "createUserWithPhone:failure", p0.exception)
                                                Toast.makeText(
                                                    activity!!, "HATA OLUŞTU.",
                                                    Toast.LENGTH_SHORT
                                                ).show()
                                            }
                                        }


                                    }


                                    )

                            }

                        }

                    }

                }


            })


        }
        }

        //   view.FragmentKayitEditTextAdSoyad.addTextChangedListener(watcher)
        //   view.fragmentKayitEditTextKullaniciAdi.addTextChangedListener(watcher)
        //   view.fragmentKayitEditTextSifre.addTextChangedListener(watcher)
        //fragmentKayitButtonKaydol





        return view
    }

    /*  var watcher : TextWatcher = object :TextWatcher
      {
          override fun afterTextChanged(p0: Editable?) {
              TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
          }

          override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
              TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
          }

          override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {


  if(s!!.length>=6)
  {
    if(
        FragmentKayitEditTextAdSoyad.text.toString().length >= 6 &&
        fragmentKayitEditTextKullaniciAdi.text.toString().length >= 6 &&
        fragmentKayitEditTextSifre.text.toString().length >= 6
    )
    {
        fragmentKayitButtonKaydol.isEnabled=true
    }
     else
    {
        fragmentKayitButtonKaydol.isEnabled=false

    }
  }
              else
  {
      fragmentKayitButtonKaydol.isEnabled=false

  }



          }


      }*/


    @Subscribe(sticky = true) //eventbustan poststicky ile bişey gönderilmişse yakala
    internal fun onKayitEvent(kayitBilgileri : EventbusDataEvents.KayitBilgileriniGonder)//benim oluşturduğum classdan geldi
    {
        if(kayitBilgileri.mailMi)
        {

            isEmail =true
            gelenMail = kayitBilgileri.eMail!!
            Log.e("murat",gelenMail)

        }
        else
        {
            isEmail=false
            verificationID = kayitBilgileri.verificationId!!
            gelenkod = kayitBilgileri.code!!
            telno = kayitBilgileri.telNo!!

        }

    }
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        EventBus.getDefault().register(this)
    }

    override fun onDetach() {
        super.onDetach()
        EventBus.getDefault().unregister(this)

    }
    private fun updateUserInfoAndUI() {
        //start next activity
        val intent = Intent(activity!!, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }

    private fun verifyEmail() {
        val mUser = mAuth!!.currentUser;
        mUser!!.sendEmailVerification()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Toast.makeText(activity,"Lütfen Mail Adresinize Gelen Doğrulama Linkine Tıklayarak Kaydınızı Aktif Hale Getiriniz",Toast.LENGTH_SHORT).show()
                }
                else {
                    Log.e("TAG", "sendEmailVerification", task.exception)
                    Toast.makeText(activity,"Mail Adresinize Doğrulama Linki Gönderilemedi",Toast.LENGTH_SHORT).show()
                }
            }




    }


    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        this.activity= activity as RegisterActivity?
    }


}
