package com.besoci.sausosyal.besoci.Session


import android.content.Context
import android.content.SharedPreferences
import android.location.Location
import android.preference.PreferenceManager
import com.besoci.sausosyal.besoci.Models.Users

class Session(cntx: Context) {

    public val prefs: SharedPreferences

    var prefDurum: Boolean
        get() = prefs.getBoolean("pref_durum", false)
        set(prefDurum) {
            prefs.edit().putBoolean("pref_durum", prefDurum).commit()
        }



    //istanbul koordinatı
    var latitude: Float
        get() = prefs.getFloat("pref_latitude", 41.014.toFloat())
        set(latitude) {
            prefs.edit().putFloat("pref_latitude", latitude).commit()
        }

    //istanbul koordinatı
    var longitude: Float
        get() = prefs.getFloat("pref_longitude", 28.950.toFloat())
        set(longitude) {
            prefs.edit().putFloat("pref_longitude", longitude).commit()
        }

     var lastRefreshDateWeather: String
         get() = prefs.getString("pref_lastRefreshDateWeather", "03.02.2019")
         set(lastRefreshDateWeather) {
             prefs.edit().putString("pref_lastRefreshDateWeather", lastRefreshDateWeather).commit()
         }


     var girisDurumu: Boolean
         get() = prefs.getBoolean("pref_girisdurumu", false)
         set(girisdurumu) {
             prefs.edit().putBoolean("pref_girisdurumu", girisdurumu).commit()
         }
    var adsoyad: String
        get() = prefs.getString("pref_adsoyad", "")
        set(adsoyad) {
            prefs.edit().putString("pref_adsoyad", adsoyad).commit()
        }


    init {
        prefs = PreferenceManager.getDefaultSharedPreferences(cntx)
    }


}