package com.besoci.sausosyal.besoci.Login

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.besoci.sausosyal.besoci.Home.HomeActivity
import com.besoci.sausosyal.besoci.R
import com.besoci.sausosyal.besoci.Session.Session

class GirisActivity : AppCompatActivity() {


    var session: Session? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_giris)
        session = com.besoci.sausosyal.besoci.Session.Session(this)



        if(session!!.girisDurumu)
        {
            val intent = Intent(this, HomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            finish()
        }
        else
        {
            val intent = Intent(this, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            finish()

        }
    }
}
