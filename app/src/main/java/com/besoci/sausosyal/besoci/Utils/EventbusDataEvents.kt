package com.besoci.sausosyal.besoci.Utils

import com.besoci.sausosyal.besoci.Models.UserPosts
import com.besoci.sausosyal.besoci.Models.Users

class EventbusDataEvents {

    internal class KayitBilgileriniGonder(var telNo:String? , var eMail : String? , var verificationId:String? ,var code:String? , var mailMi:Boolean)
    internal class KullaniciBilgileriniGonder(var kullanici:Users?)
    internal class PaylasilacakResmiGonder(var resimYolu:String? , var dosyaTuruResim:Boolean?)
    internal class GallerySecilenDosyaYoluGonder(var dosyaYolu:String?)
    internal class YorumYapilacakGonderininIDsiniGonder(var gonderiID:String?)
    internal class secilenGonderiyiGonder(var secilenGonderi:UserPosts? , var videoMu : Boolean)



}