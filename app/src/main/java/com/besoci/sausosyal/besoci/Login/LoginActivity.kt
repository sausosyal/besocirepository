package com.besoci.sausosyal.besoci.Login

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import com.besoci.sausosyal.besoci.Home.HomeActivity
import com.besoci.sausosyal.besoci.Models.Users
import com.besoci.sausosyal.besoci.R
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {


    lateinit var mRef : DatabaseReference
    lateinit var mAuth : FirebaseAuth
    lateinit var mAuthListener : FirebaseAuth.AuthStateListener


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        //setupAuthListener()
        dbBaglantilariAc()
         init()



        LoginTextViewUyeOl.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            finish()
        }




    }



    private fun dbBaglantilariAc() {

        mAuth=FirebaseAuth.getInstance()
        mRef=FirebaseDatabase.getInstance().reference
    }

     fun init()
     {
        // LoginEditTextEpostaTelefon.addTextChangedListener(watcher)
       //  LoginEditTextSifre.addTextChangedListener(watcher)

         LoginButtonGiris.setOnClickListener {

             if(LoginEditTextEpostaTelefon.length() <= 0 || LoginEditTextSifre.length() <= 0 )
             {
                 Toast.makeText(this@LoginActivity,"ALANLAR BOS GECILEMEZ",Toast.LENGTH_SHORT).show()
             }

             else
             {
                 oturumAcmaDenetimi(LoginEditTextEpostaTelefon.text.toString(),LoginEditTextSifre.text.toString())
             }

         }


     }

    private fun oturumAcmaDenetimi(kullaniciAdi: String, sifre: String) {

        mRef.child("users").orderByChild("email").addListenerForSingleValueEvent(object :ValueEventListener
        {
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(p0: DataSnapshot?) {

                var kullaniciBulunamadi:Boolean = true
                for(ds in p0!!.children)
                {
                    var okunanKullanici = ds.getValue(Users::class.java)
                    if(okunanKullanici!!.email!!.toString().equals(kullaniciAdi) || okunanKullanici!!.user_name.toString().equals(kullaniciAdi))
                        {
                            oturumAc(okunanKullanici,sifre,false)
                            kullaniciBulunamadi = false
                            break

                        }
                    else if(okunanKullanici!!.phone_number.toString().equals(kullaniciAdi))
                    {
                        oturumAc(okunanKullanici,sifre,true)
                        kullaniciBulunamadi = false
                        break
                    }


                }

                if(kullaniciBulunamadi)
                Toast.makeText(this@LoginActivity,"KULLANICI BULUNAMADI",Toast.LENGTH_SHORT).show()


            }

        }
        )

    }


    private fun oturumAc(kullanici:Users,sifre:String,telefonMu :Boolean)
    {
        var girisYapacagimEmail=kullanici.email.toString()
//email telefon boş mu diye bakarsın
        if(kullanici.email_phone_number.toString().length<2 )//yani boşsa
        {
            //authanticationa bak

            mAuth.signInWithEmailAndPassword(girisYapacagimEmail,sifre.toString())
                .addOnCompleteListener(object :OnCompleteListener<AuthResult>
                {
                    override fun onComplete(p0: Task<AuthResult>) {
                        if(p0!!.isSuccessful)
                        {
                            if(mAuth.currentUser!!.isEmailVerified)
                            {         Toast.makeText(this@LoginActivity,"HOŞGELDİNİZ",Toast.LENGTH_SHORT).show()
                            var intent = Intent(this@LoginActivity,HomeActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                            //    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            startActivity(intent)
                            finish()  }
                            else
                            {  Toast.makeText(this@LoginActivity,"Lütfen Mailinize Gelen Aktivasyon Kodunu Onaylayınız",Toast.LENGTH_SHORT).show()}

                        }
                        else
                        {
                            Toast.makeText(this@LoginActivity,"Kullanıcı Adı Veya Şifre Hatalı",Toast.LENGTH_SHORT).show()


                        }
                    }

                }
                )
        }
        else
        {


                girisYapacagimEmail=kullanici.email_phone_number.toString()



            mAuth.signInWithEmailAndPassword(girisYapacagimEmail,sifre)
                .addOnCompleteListener(object :OnCompleteListener<AuthResult>
                {
                    override fun onComplete(p0: Task<AuthResult>) {
                        if(p0!!.isSuccessful)
                        {
                            Toast.makeText(this@LoginActivity,"HOŞGELDİNİZ",Toast.LENGTH_SHORT).show()
                            var intent = Intent(this@LoginActivity,HomeActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                            //    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            startActivity(intent)
                            finish()
                        }
                        else
                        {
                            Toast.makeText(this@LoginActivity,"Kullanıcı Adı Veya Şifre Hatalı",Toast.LENGTH_SHORT).show()


                        }
                    }

                }
                )


        }




    }



  /*  private fun setupAuthListener() {
        mAuthListener = object : FirebaseAuth.AuthStateListener{
            override fun onAuthStateChanged(p0: FirebaseAuth) {

                var user = FirebaseAuth.getInstance().currentUser

                if(user != null) //giriş yapabildiyse
                {
                    var intent = Intent(this@LoginActivity,HomeActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                //    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    finish()
                }
            }
        }
    }*/
 /*   override fun onStart() {
        super.onStart()
        mAuth.addAuthStateListener(mAuthListener)
    }
*/
 /*   override fun onStop() {
        super.onStop()
        if(mAuthListener != null)
        {
            mAuth.removeAuthStateListener(mAuthListener)
        }
    }
*/

/* var watcher : TextWatcher = object :TextWatcher{
         override fun afterTextChanged(p0: Editable?) {
         }

         override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
         }

         override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

              if(LoginEditTextEpostaTelefon.text.toString().length > 1 && LoginEditTextSifre.text.toString().length>1)
              {
                  LoginButtonGiris.isEnabled=true
              }
             else
              {
                  LoginButtonGiris.isEnabled=false
              }


         }
     }
     */



}
