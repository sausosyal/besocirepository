package com.besoci.sausosyal.besoci.Login

import android.content.Intent
import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.text.InputType
import android.view.View
import android.widget.Toast
import com.besoci.sausosyal.besoci.Home.HomeActivity
import com.besoci.sausosyal.besoci.Models.Users
import com.besoci.sausosyal.besoci.R
import com.besoci.sausosyal.besoci.Utils.EventbusDataEvents
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_register.*
import org.greenrobot.eventbus.EventBus

class RegisterActivity : AppCompatActivity() ,FragmentManager.OnBackStackChangedListener {
    lateinit var manager :FragmentManager
    lateinit var mRef:DatabaseReference
     var facebold: Typeface? = null
     var facethin: Typeface? = null
    lateinit var mAuthListener : FirebaseAuth.AuthStateListener
    lateinit var mAuth : FirebaseAuth



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
   //     setupAuthListener()
        mAuth = FirebaseAuth.getInstance()
        mRef=FirebaseDatabase.getInstance().reference

         facebold = Typeface.createFromAsset(
            assets,
            "fonts/robotobold.ttf"
        )
        facethin = Typeface.createFromAsset(
            assets,
            "fonts/robotolight.ttf"
        )
        RegisterTextViewEpostaIleKaydol.setTypeface(facebold)
        RegisterTextViewTelefonIleKayitOl.setTypeface(facethin)
         manager = supportFragmentManager
        manager.addOnBackStackChangedListener(this) //bu activity bu listeneri gerçekleştiriyor diye belirttik
        init()


        RegisterTextViewGirisYap.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            startActivity(intent)
            finish()
        }


    }

    private fun init() {
        RegisterTextViewEpostaIleKaydol.setOnClickListener {
            RegisterEditTextEpostaTelefon.setText("")
            RegisterEditTextEpostaTelefon.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
            RegisterEditTextEpostaTelefon.setHint("E-POSTA")

            RegisterTextViewEpostaIleKaydol.setTypeface(facebold)
            RegisterTextViewTelefonIleKayitOl.setTypeface(facethin)
            RegisterEditTextEpostaTelefon.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_mail, 0, 0, 0 );




        }
        RegisterTextViewTelefonIleKayitOl.setOnClickListener {
            RegisterEditTextEpostaTelefon.setText("")
            RegisterEditTextEpostaTelefon.inputType = InputType.TYPE_CLASS_PHONE
            RegisterEditTextEpostaTelefon.setHint("TELEFON Orn : +905559990099 ")
            RegisterTextViewTelefonIleKayitOl.setTypeface(facebold)
            RegisterTextViewEpostaIleKaydol.setTypeface(facethin)
            RegisterEditTextEpostaTelefon.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_phone, 0, 0, 0 );


        }
        RegisterButtonIleri.setOnClickListener {
                if(!RegisterEditTextEpostaTelefon.hint.toString().toUpperCase().equals("E-POSTA"))
                {
                    if(isValidTelefon(RegisterEditTextEpostaTelefon.text.toString()))
                    {


                        var telefonKullanimdaMi =false
                        mRef.child("users").addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onCancelled(p0: DatabaseError?) {



                            }

                            override fun onDataChange(p0: DataSnapshot?) {

                                if(p0!!.getValue() != null)
                                {

                                    for(user in p0!!.children)
                                    {

                                        var okunanKullanici = user.getValue(Users::class.java)
                                        if(okunanKullanici!!.phone_number.equals(RegisterEditTextEpostaTelefon.text.toString()))
                                        {

                                            Toast.makeText(this@RegisterActivity,"Bu telefon numarası önceden kullanılmış.Lütfen farklı bir numara giriniz.",Toast.LENGTH_SHORT).show()
                                            telefonKullanimdaMi=true
                                            break

                                        }


                                    }

                                    if(!telefonKullanimdaMi)
                                    {
                                        fragmentChange(TelefonKoduGirFragment(),"TelefonkoduGirFragmentEklendi")
                                        EventBus.getDefault().postSticky(EventbusDataEvents.KayitBilgileriniGonder(RegisterEditTextEpostaTelefon.text.toString(),null,null,null,false))
                                        //poststicky demek açıldığı gibi yayın varsa subscribe stickysi true olanlar alsın bu yaynı
                                    }

                                }

                            }


                        })













                    }

                    else
                    {
                        Toast.makeText(this,"Lutfen Gecerli Bir Telefon Numarasi Giriniz",Toast.LENGTH_SHORT).show()
                    }

                }
                else
                {



                    if(isValidEmail(RegisterEditTextEpostaTelefon.text.toString()))
                    {

                        var emailKullanimdaMi =false
                        mRef.child("users").addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onCancelled(p0: DatabaseError?) {



                            }

                            override fun onDataChange(p0: DataSnapshot?) {

                                if(p0!!.getValue() != null)
                                {

                                    for(user in p0!!.children)
                                    {

                                        var okunanKullanici = user.getValue(Users::class.java)
                                        if(okunanKullanici!!.email.equals(RegisterEditTextEpostaTelefon.text.toString()))
                                        {

                                            Toast.makeText(this@RegisterActivity,"Bu mail adresi önceden kullanılmış.Lütfen farklı bir adres giriniz.",Toast.LENGTH_SHORT).show()
                                            emailKullanimdaMi=true
                                            break

                                        }


                                    }

                                    if(!emailKullanimdaMi)
                                    {
                                        fragmentChange(KayitFragment(),"EmailGirisYontemiFragmentEklendi")
                                        EventBus.getDefault().postSticky(EventbusDataEvents.KayitBilgileriniGonder(null,RegisterEditTextEpostaTelefon.text.toString(),null,null,true))
                                        //poststicky demek açıldığı gibi yayın varsa subscribe stickysi true olanlar alsın bu yaynı
                                    }

                                }

                            }


                        })








                    }

                    else
                    {
                        Toast.makeText(this,"Lutfen Gecerli Bir Mail Adresi Giriniz",Toast.LENGTH_SHORT).show()
                    }


                }


            }






    }

    private fun fragmentChange(gelenFragment : Fragment, gelenKey:String)  {
        LoginRoute.visibility= View.GONE //şimdiki viewi yani profil düzenleme ana ekranını kapat
        var transaction=supportFragmentManager.beginTransaction() //fragmentbaşlat
        transaction.replace(R.id.LoginContainer,gelenFragment) //gelenFragmenti koy
        transaction.addToBackStack(gelenKey)
        transaction.commit()




    }


    override fun onBackStackChanged() { //geri var ı diye artık buradan bakıp bi önceki fragment varsa ona gidiccez
        if(manager.backStackEntryCount <=0) //stackte eleman yoksa yani başka fragment yoksa geri yaptığımda
            LoginRoute.visibility = View.VISIBLE //ana activityi gorunur yap
    }

    /*  text içi değişince
  RegisterEditTextEpostaTelefon.addTextChangedListener(object :TextWatcher
    {
        override fun afterTextChanged(p0: Editable?) {
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, start: Int, before: Int, count: Int) {

        }

    }
    )
    */

    fun isValidEmail(kontrolEdilecekEmail:String):Boolean
    {
        if(kontrolEdilecekEmail==null || kontrolEdilecekEmail.length<=5)
            return false

        return android.util.Patterns.EMAIL_ADDRESS.matcher(kontrolEdilecekEmail).matches() //email adresiyse true döner
    }
    fun isValidTelefon(kontrolEdilecekTelefon:String):Boolean
    {
        if(kontrolEdilecekTelefon==null || kontrolEdilecekTelefon.length != 13)  //+90 lı telefon numarası girmek zorunda
            return false

        return android.util.Patterns.PHONE.matcher(kontrolEdilecekTelefon).matches() //email adresiyse true döner
    }


   /* private fun setupAuthListener() {
        mAuthListener = object : FirebaseAuth.AuthStateListener{
            override fun onAuthStateChanged(p0: FirebaseAuth) {

                var user = FirebaseAuth.getInstance().currentUser

                if(user != null) //giriş yapabildiyse
                {
                    var intent = Intent(this@RegisterActivity, HomeActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                    startActivity(intent)
                    finish()
                }
            }
        }
    }*/
  /*  override fun onStart() {
        super.onStart()
        mAuth.addAuthStateListener(mAuthListener)
    }
*/
  /*  override fun onStop() {
        super.onStop()
        if(mAuthListener != null)
        {
            mAuth.removeAuthStateListener(mAuthListener)
        }
    }
*/

}
