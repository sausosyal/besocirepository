package com.besoci.sausosyal.besoci.Share

import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.support.v4.view.ViewPager
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.Toast
import com.besoci.sausosyal.besoci.Manifest
import com.besoci.sausosyal.besoci.R
import com.besoci.sausosyal.besoci.Utils.BottomNavigationViewHelper
import com.besoci.sausosyal.besoci.Utils.SharePagerAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.DexterError
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.PermissionRequestErrorListener
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_share.*

class ShareActivity : AppCompatActivity() {

    private val ACTIVITY_NO = 2;
    private val TAG = "Share"


  //  lateinit var mAuth : FirebaseAuth
 //   lateinit var mAuthListener : FirebaseAuth.AuthStateListener
//    lateinit var mRef : DatabaseReference
//    lateinit var mUser : FirebaseUser


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share) //değişecek

        IzinlerIste()

    }

    private fun IzinlerIste() {
        Dexter.withActivity(this)
            .withPermissions(android.Manifest.permission.CAMERA,
                android.Manifest.permission.READ_EXTERNAL_STORAGE,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                android.Manifest.permission.RECORD_AUDIO
                ).withListener(object :MultiplePermissionsListener
            {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {

                    if(report!!.areAllPermissionsGranted())
                    {
                        setupShareViewPager()

                    }
                    if(report!!.isAnyPermissionPermanentlyDenied())
                    {
                        var builder = AlertDialog.Builder(this@ShareActivity)

                        builder.setTitle("İzin İsteği")
                        builder.setMessage("Ayarlardan uygalamaya izin vermeniz gerekiyor ")
                        builder.setPositiveButton("Ayarlara Git", object : DialogInterface.OnClickListener
                        {
                            override fun onClick(dialog: DialogInterface?, p1: Int) {

                                dialog!!.cancel()

                                var intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                                var uri = Uri.fromParts("package",packageName,null)
                                intent.setData(uri)
                                startActivity(intent)
                                finish()
                            }

                        }
                        )
                        builder.setNegativeButton("İptal", object : DialogInterface.OnClickListener
                        {
                            override fun onClick(dialog: DialogInterface?, p1: Int) {
                                dialog!!.cancel()
                                finish()

                            }

                        }
                        )

                        builder.show()
                    }



                }

                override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?)
                {
                    var builder = AlertDialog.Builder(this@ShareActivity)

                    builder.setTitle("İzin İsteği")
                    builder.setMessage("Uygalamaya izin vermeniz gerekiyor , Onaylıyor musunuz ? ")
                    builder.setPositiveButton("Onay Ver", object : DialogInterface.OnClickListener
                    {
                        override fun onClick(dialog: DialogInterface?, p1: Int) {

                            dialog!!.cancel()
                            token!!.continuePermissionRequest()

                        }

                    }
                    )
                    builder.setNegativeButton("İptal", object : DialogInterface.OnClickListener
                    {
                        override fun onClick(dialog: DialogInterface?, p1: Int) {
                            dialog!!.cancel()
                            token!!.cancelPermissionRequest()
                            finish()

                        }

                    }
                    )

                    builder.show()


                }

            }
            ).withErrorListener(object : PermissionRequestErrorListener
            {
                override fun onError(error: DexterError?) {
                    Toast.makeText(applicationContext,"Hata Oluştu",Toast.LENGTH_LONG)
                }

            }).check()
    }

    private fun setupShareViewPager() {

        var tabAdlar = ArrayList<String>()
        tabAdlar.add("GALERİDEN SEÇ")
        tabAdlar.add("FOTOĞRAF ÇEK")
        // VIDEO tabAdlar.add("VİDEO ÇEK")


        var sharePagerAdapter = SharePagerAdapter(supportFragmentManager,tabAdlar)
        sharePagerAdapter.addFragment(ShareGalleryFragment())
        sharePagerAdapter.addFragment(ShareCameraFragment())
      //VIDEO  sharePagerAdapter.addFragment(ShareVideoFragment())

        shareViewPager.adapter=sharePagerAdapter

        shareViewPager.offscreenPageLimit = 1


        ////////////////////////////////////////////////////////////////////////////////////////////
        //FRAGMENTLERDE AÇILANI AKTİF ETME
        //sharepager adaptere bak

        sharePagerAdapter.fragmentiPagerdanSil(shareViewPager,1)
       //VIDEO sharePagerAdapter.fragmentiPagerdanSil(shareViewPager,2)
        shareViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener
        {
            override fun onPageScrollStateChanged(pos: Int) {


            }

            override fun onPageScrolled(pos: Int, p1: Float, p2: Int) {

            }

            override fun onPageSelected(pos: Int) {

                if(pos == 0) //galeri
                {
                    sharePagerAdapter.fragmentiPagerdanSil(shareViewPager,1)
                 //   sharePagerAdapter.fragmentiPagerdanSil(shareViewPager,2) //VIDEO
                    sharePagerAdapter.fragmentiPageraEkle(shareViewPager,0)

                }
                else if(pos == 1) //fotocek
                {
                    sharePagerAdapter.fragmentiPagerdanSil(shareViewPager,0)
                //    sharePagerAdapter.fragmentiPagerdanSil(shareViewPager,2) //VIDEO
                    sharePagerAdapter.fragmentiPageraEkle(shareViewPager,1)
                }
           /*VIDEO     else if(pos == 2) //videocek
                {
                    sharePagerAdapter.fragmentiPagerdanSil(shareViewPager,0)
                    sharePagerAdapter.fragmentiPagerdanSil(shareViewPager,1)
                    sharePagerAdapter.fragmentiPageraEkle(shareViewPager,2)
                }
                */
            }

        }
        )
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





        shareTabLayout.setupWithViewPager(shareViewPager)
    }


    override fun onBackPressed() {  //normalde geri yapıyor biz fragment kullandığımız için bu şekidle override ettik
        anaLayout.visibility = View.VISIBLE
        fragmentContainerLayout.visibility = View.GONE
        super.onBackPressed()
    }


}
