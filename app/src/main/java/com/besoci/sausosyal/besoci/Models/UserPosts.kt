package com.besoci.sausosyal.besoci.Models

class UserPosts {

    var UserID : String? = null
    var UserName : String? = null
    var UserPhotoURL : String? = null
    var postID : String? = null
    var postAciklama : String? = null
    var postURL : String? = null
    var postYuklenmeTarih : Long? = null


    constructor(
        UserID: String?,
        UserName: String?,
        UserPhotoURL: String?,
        postID: String?,
        postAciklama: String?,
        postURL: String?,
        postYuklenmeTarih: Long?
    ) {
        this.UserID = UserID
        this.UserName = UserName
        this.UserPhotoURL = UserPhotoURL
        this.postID = postID
        this.postAciklama = postAciklama
        this.postURL = postURL
        this.postYuklenmeTarih = postYuklenmeTarih
    }

    constructor()
    {}


}