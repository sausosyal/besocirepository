package com.besoci.sausosyal.besoci.Share

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.besoci.sausosyal.besoci.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_post_guncelle.*

class PostGuncelleActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_guncelle)



        var mRef : DatabaseReference = FirebaseDatabase.getInstance().reference
        var mAuth= FirebaseAuth.getInstance()
        var mUser : FirebaseUser = mAuth!!.currentUser!!
        mRef.child("posts").child(mUser.uid).child(intent.getStringExtra("OankiPost")).addListenerForSingleValueEvent(object :
            ValueEventListener
        {
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(p0: DataSnapshot?) {
                editTextIleriAciklama.setText(p0!!.child("aciklama").getValue().toString())
                Picasso.with(this@PostGuncelleActivity).load(p0!!.child("photo_url").getValue().toString()).into(imageViewIleri)

                if(p0!!.child("mekan_tipi").getValue().toString() == "Acik Alan")
                {
                    spinnerMekanTipi.setSelection(0)
                }
                if(p0!!.child("mekan_tipi").getValue().toString() == "Kapali Alan")
                {
                    spinnerMekanTipi.setSelection(1)

                }
                if(p0!!.child("kategori").getValue().toString() == "Yemek")
                {
                    spinnerKategori.setSelection(0)
                }
                if(p0!!.child("kategori").getValue().toString() == "Alisveris")
                {
                    spinnerKategori.setSelection(1)

                }
                if(p0!!.child("kategori").getValue().toString() == "Spor")
                {
                    spinnerKategori.setSelection(2)

                }
                if(p0!!.child("kategori").getValue().toString() == "Piknik-Kamp")
                {
                    spinnerKategori.setSelection(3)

                }
                if(p0!!.child("kategori").getValue().toString() == "Kultur-Sanat")
                {
                    spinnerKategori.setSelection(4)

                }
                if(p0!!.child("kategori").getValue().toString() == "Diger")
                {
                    spinnerKategori.setSelection(5)

                }
                checkBoxAileler.isChecked = p0!!.child("aile").getValue() as Boolean
                checkBoxSevgililer.isChecked = p0!!.child("sevgili").getValue() as Boolean
                checkBoxYalnizlar.isChecked = p0!!.child("yalniz").getValue() as Boolean
                checkBoxCocuklar.isChecked = p0!!.child("cocuk").getValue() as Boolean
                checkBoxYetiskinler.isChecked = p0!!.child("yetiskin").getValue() as Boolean
                checkBoxOgrenciler.isChecked = p0!!.child("ogrenci").getValue() as Boolean


                fragmentShareIleriTextViewPaylas.setOnClickListener {

                     mRef  = FirebaseDatabase.getInstance().reference
                     mAuth= FirebaseAuth.getInstance()
                     mUser  = mAuth!!.currentUser!!
                     mRef.child("posts").child(mUser.uid).child(intent.getStringExtra("OankiPost")).child("aile").setValue( checkBoxAileler.isChecked )
                     mRef.child("posts").child(mUser.uid).child(intent.getStringExtra("OankiPost")).child("sevgili").setValue( checkBoxSevgililer.isChecked )
                     mRef.child("posts").child(mUser.uid).child(intent.getStringExtra("OankiPost")).child("yalniz").setValue( checkBoxYalnizlar.isChecked )
                     mRef.child("posts").child(mUser.uid).child(intent.getStringExtra("OankiPost")).child("cocuk").setValue( checkBoxCocuklar.isChecked )
                     mRef.child("posts").child(mUser.uid).child(intent.getStringExtra("OankiPost")).child("yetiskin").setValue( checkBoxYetiskinler.isChecked )
                     mRef.child("posts").child(mUser.uid).child(intent.getStringExtra("OankiPost")).child("ogrenci").setValue( checkBoxOgrenciler.isChecked )
                     mRef.child("posts").child(mUser.uid).child(intent.getStringExtra("OankiPost")).child("kategori").setValue( spinnerKategori.selectedItem.toString() )
                     mRef.child("posts").child(mUser.uid).child(intent.getStringExtra("OankiPost")).child("mekan_tipi").setValue( spinnerMekanTipi.selectedItem.toString() )
                     mRef.child("posts").child(mUser.uid).child(intent.getStringExtra("OankiPost")).child("aciklama").setValue( editTextIleriAciklama.text.toString() )

                    Toast.makeText(this@PostGuncelleActivity , "Guncelleme Basarili" , Toast.LENGTH_SHORT).show()

                    onBackPressed()



                }

// mRef.child("posts").child(mUser.uid).child(postid).child("yuklenme_tarihi").setValue(ServerValue.TIMESTAMP)

               // mRef.child("users").child(mUser.uid).child("user_detail").child("gonderiSayisi").setValue(oankiGonderiSayisi.toString())

            }

        }
        )






    }
}
