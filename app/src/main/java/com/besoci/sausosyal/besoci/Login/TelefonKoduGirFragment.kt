package com.besoci.sausosyal.besoci.Login


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast

import com.besoci.sausosyal.besoci.R
import com.besoci.sausosyal.besoci.Utils.EventbusDataEvents
import com.google.firebase.FirebaseException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import kotlinx.android.synthetic.main.fragment_telefon_kodu_gir.*
import kotlinx.android.synthetic.main.fragment_telefon_kodu_gir.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.concurrent.TimeUnit


class TelefonKoduGirFragment : Fragment() {
var gelenTelNo =""
    lateinit var callbacks:PhoneAuthProvider.OnVerificationStateChangedCallbacks //lateinit demek sana değeri sonradan ataacam demek
    var verificationID =""
    var gelenkod = ""
    lateinit var progressbar : ProgressBar


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        var view = inflater.inflate(R.layout.fragment_telefon_kodu_gir, container, false)
        view.FragmentTelefonKoduTextViewTelno.setText(gelenTelNo)
        progressbar = view.TelefonKoduGirProgressBar


        setupCallback()


        view.FragmentTelefonKoduGirButtonIleri.setOnClickListener {

            if(FragmentTelefonKoduGirEditTextOnayKodu.text.toString().equals(gelenkod))
            {
                fragmentChange(KayitFragment(),"KayitFragmentEklendi")
                EventBus.getDefault().postSticky(EventbusDataEvents.KayitBilgileriniGonder(gelenTelNo,null,verificationID,gelenkod,false))

            }
            else
            {

                Toast.makeText(activity,"Kod Hatalı",Toast.LENGTH_LONG).show()

            }
        }


        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            gelenTelNo,      // Phone number to verify
            60,               // Timeout duration
            TimeUnit.SECONDS, // Unit of timeout
            this.activity!!,             // Activity (for callback binding)
            callbacks) // OnVerificationStateChangedCallbacks



        return view
    }

    private fun setupCallback() {
        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            override fun onVerificationCompleted(credential: PhoneAuthCredential) =//sms mesajı gelince
                if(!credential.smsCode.isNullOrEmpty())
                {   progressbar.visibility = View.INVISIBLE
                    Toast.makeText(activity,"SMS Gonderildi",Toast.LENGTH_SHORT).show()

                    gelenkod = credential.smsCode!!
                    progressbar.visibility = View.INVISIBLE

                }
                else
                {
                    progressbar.visibility = View.INVISIBLE
                    Toast.makeText(activity,"SMS Daha Onceden Gonderilmis",Toast.LENGTH_SHORT).show()

                }

            override fun onVerificationFailed(e: FirebaseException) {
                Toast.makeText(activity,"Hata Olustu",Toast.LENGTH_LONG).show()
                progressbar.visibility = View.INVISIBLE
            }

            override fun onCodeSent(verificationId: String?, token: PhoneAuthProvider.ForceResendingToken)//kod gönderildiğinde
            {
                progressbar.visibility = View.VISIBLE
           verificationID = verificationId!!
            }
        }
    }

    @Subscribe(sticky = true) //eventbustan poststicky ile bişey gönderilmişse yakala
internal fun onTelefonNoEvent(kayitBilgileri : EventbusDataEvents.KayitBilgileriniGonder)//benim oluşturduğum classdan geldi
{
    gelenTelNo = kayitBilgileri.telNo!!
    Log.e("murat",gelenTelNo)
}
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        EventBus.getDefault().register(this)
    }

    override fun onDetach() {
        super.onDetach()
        EventBus.getDefault().unregister(this)

    }

    private fun fragmentChange(gelenFragment : Fragment, gelenKey:String)  {
        var transaction=activity!!.supportFragmentManager.beginTransaction() //fragmentbaşlat
        transaction.replace(R.id.LoginContainer,gelenFragment) //gelenFragmenti koy
        transaction.addToBackStack(gelenKey)
        transaction.commit()




    }
}
