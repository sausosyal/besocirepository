package com.besoci.sausosyal.besoci.Share


import android.graphics.Camera
import android.graphics.Color
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

import com.besoci.sausosyal.besoci.R
import com.besoci.sausosyal.besoci.Utils.EventbusDataEvents
import com.otaliastudios.cameraview.*
import kotlinx.android.synthetic.main.activity_share.*
import kotlinx.android.synthetic.main.fragment_share_video.view.*
import org.greenrobot.eventbus.EventBus
import java.io.File

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class ShareVideoFragment : Fragment() {
    var videoview : CameraView? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        var view =  inflater.inflate(R.layout.fragment_share_video, container, false)
        var kirmizi = Color.parseColor("#ff1a1a");
        var siyah = Color.parseColor("#000000");
        videoview=view.videoView
        videoview!!.mapGesture(Gesture.PINCH, GestureAction.ZOOM)
        videoview!!.mapGesture(Gesture.TAP,GestureAction.FOCUS_WITH_MARKER)

        var olusacakVideoDosyaAdi : Long = System.currentTimeMillis()
        var olusacakVideoDosya = File(Environment.getExternalStorageDirectory().absolutePath+"/DCIM/Camera/"+olusacakVideoDosyaAdi+".mp4")


        videoview!!.addCameraListener(object : CameraListener()
        {
            override fun onVideoTaken(video: File?) {
                super.onVideoTaken(video)


                activity!!.anaLayout.visibility=View.GONE
                activity!!.fragmentContainerLayout.visibility = View.VISIBLE

                var transaction = activity!!.supportFragmentManager.beginTransaction()

                EventBus.getDefault().postSticky(EventbusDataEvents.PaylasilacakResmiGonder(video!!.absolutePath.toString(),false))
                transaction.replace(R.id.fragmentContainerLayout,ShareIleriFragment())
                transaction.addToBackStack("shareilerifragmenteklendi")
                transaction.commit()



            }
        }
        )




        view.imgvideocek.setOnTouchListener(object : View.OnTouchListener
        {
            override fun onTouch(p0: View?, event: MotionEvent?): Boolean {
                if(event!!.action== MotionEvent.ACTION_DOWN)
                {
                    videoview!!.startCapturingVideo(olusacakVideoDosya)

                    view.imgvideocek.setColorFilter(kirmizi)
                    return true
                }
                else if(event!!.action== MotionEvent.ACTION_UP)
                {
                    videoview!!.stopCapturingVideo()
                    view.imgvideocek.setColorFilter(siyah)

                    return true
                }

                return false
            }

        }
        )

        view.imageVideoSwitch.setOnClickListener {
            if(videoview!!.facing == Facing.BACK)
            {
                videoview!!.facing = Facing.FRONT
            }
            else
            {
                videoview!!.facing = Facing.BACK

            }

        }
        view.imgvflash.setOnClickListener {




            if(videoview!!.flash == Flash.OFF)
            {
                videoview!!.flash = Flash.ON
                view.imgvflash.setImageResource(R.drawable.ic_flashon)
            }
            else if(videoview!!.flash == Flash.ON)
            {
                videoview!!.flash = Flash.AUTO
                view.imgvflash.setImageResource(R.drawable.ic_flashauto)
            }
            else if(videoview!!.flash == Flash.AUTO)
            {
                videoview!!.flash = Flash.OFF
                view.imgvflash.setImageResource(R.drawable.ic_flashoff)
            }
        }


        view.fragmentShareImageViewCancel.setOnClickListener {
            activity!!.onBackPressed()
        }

        return view
    }

    override fun onResume() {
        super.onResume()
        videoview!!.start()

    }

    override fun onPause() {
        super.onPause()
        videoview!!.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        if(videoview != null)
        videoview!!.destroy()
    }
}


