package com.besoci.sausosyal.besoci.Utils

import android.graphics.Bitmap
import android.os.AsyncTask
import android.os.Environment
import android.support.v4.app.Fragment
import com.besoci.sausosyal.besoci.Profile.YukleniyorFragment
import com.besoci.sausosyal.besoci.Share.ShareIleriFragment
import com.iceteck.silicompressorr.SiliCompressor
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import kotlin.Comparator

class DosyaIslemleri {

    companion object {

        fun KlasordekiDosyalariGetir(klasorAdi : String):ArrayList<String>
        {
            var tumDosyalar = ArrayList<String>()


            var file = File(klasorAdi)

            var klasordekiTumDosyalar = file.listFiles()

            if(klasordekiTumDosyalar != null)
            {

 //resimleri tarihe gore sonran başa sıralama
                if(klasordekiTumDosyalar.size > 1)
                {
                    Arrays.sort(klasordekiTumDosyalar,object : Comparator<File> {
                        override fun compare(p0: File?, p1: File?): Int {
                            if(p0!!.lastModified() > p1!!.lastModified())
                                return -1
                            else
                                return 1
                        }
                    })

                }



                for(i in 0.. klasordekiTumDosyalar.size - 1)
                {
                    if(klasordekiTumDosyalar[i].isFile)
                    {
                        var okunanDosyaYolu : String = klasordekiTumDosyalar[i].absolutePath
                        if( okunanDosyaYolu.contains("."))
                        {
                            var dosyaTuru : String = okunanDosyaYolu.substring(okunanDosyaYolu.lastIndexOf("."))
                            if(dosyaTuru.equals(".jpg") || dosyaTuru.equals(".jpeg") || dosyaTuru.equals(".png") ) // || dosyaTuru.equals(".mp4")) VIDEO KONTROL
                            {
                                tumDosyalar.add(okunanDosyaYolu)
                            }
                        }


                    }

                }
            }



            return tumDosyalar
        }

        fun cpmpressResim(fragment: Fragment, secilenResimYolu: String?) {


            ResimCompressasyncTask(fragment).execute(secilenResimYolu)

        }

        fun cpmpressVideo(fragment: Fragment, secilenResimYolu: String) {

            VideCompressAsyncTask(fragment).execute(secilenResimYolu)
        }


        fun klasorMevcutMu(klasorAdi : String):Boolean
        {

            var dosya = File(klasorAdi)

            var klasordekiDosyalar = dosya.listFiles()

            if(klasordekiDosyalar != null && klasordekiDosyalar.size > 0)   //bu klasorde kayit varsa
            {
                return true
            }

            return false



        }

        fun croppedImageSave(bitmap: Bitmap): String {

            var yeniDosyaninAdi = "cropimage"+System.currentTimeMillis().toString().substring(8,12)+".jpg"
            val bytes = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG,80,bytes) //yuzde 80 kalite ile kirptik

            val ExternalStorageDirectory = Environment.getExternalStorageDirectory().path + "/PICTURES/Screenshots"
            val fileKlasor = File(ExternalStorageDirectory)
            val file = File(ExternalStorageDirectory+File.separator+yeniDosyaninAdi)

            var fileOutputStream : FileOutputStream? = null
            try {
                if(fileKlasor.isDirectory || fileKlasor.mkdirs())
                {
                    file.createNewFile()
                    fileOutputStream = FileOutputStream(file)
                    fileOutputStream.write(bytes.toByteArray())
                }

            }
            catch (e:IOException)
            {
                e.printStackTrace()
            }
            finally {
                if(fileOutputStream != null)
                {
                    try {
                        return file.absolutePath
                        fileOutputStream.close()
                    } catch (e:IOException)
                    {
                        e.printStackTrace()
                    }
                }
            }

            return ""

        }


    }

    internal class VideCompressAsyncTask(fragment: Fragment):AsyncTask<String,String,String>()
    {

        var myFragment : Fragment = fragment
        var compressFragment = YukleniyorFragment()

        override fun onPreExecute() {
            compressFragment.show(myFragment.activity!!.supportFragmentManager,"CompressDialogBasladi")
            compressFragment.isCancelable = false
            super.onPreExecute()
        }
        override fun doInBackground(vararg params: String?): String? {
            var yeniOlusanDosyaninKlasoru = File(Environment.getExternalStorageDirectory().absolutePath+"/DCIM/Camera/")

            if(yeniOlusanDosyaninKlasoru.isDirectory || yeniOlusanDosyaninKlasoru.mkdirs())
            {

              var yenidosyapath=  SiliCompressor.with(myFragment.context).compressVideo(params[0],yeniOlusanDosyaninKlasoru.path)
                return yenidosyapath
            }

            return null

        }

        override fun onPostExecute(result: String?) {

            if(!result.isNullOrEmpty())
            {
                compressFragment.dismiss()
                (myFragment as ShareIleriFragment).uploadStorage(result)

            }

            super.onPostExecute(result)
        }



    }


    internal class ResimCompressasyncTask(fragment: Fragment):AsyncTask<String,String,String>()
    {

        var myFragment : Fragment = fragment
        var compressFragment = YukleniyorFragment()



        override fun onPreExecute() {
            compressFragment.show(myFragment.activity!!.supportFragmentManager,"CompressDialogBasladi")
            compressFragment.isCancelable = false
            super.onPreExecute()
        }

        override fun doInBackground(vararg params: String?): String? {

            var yeniOlusanDosyaninKlasoru = File(Environment.getExternalStorageDirectory().absolutePath+"/DCIM/Camera/")
            if(yeniOlusanDosyaninKlasoru.isDirectory || yeniOlusanDosyaninKlasoru.mkdirs())
            {

                var yeniDosyaYolu = SiliCompressor.with(myFragment.context).compress(params[0],yeniOlusanDosyaninKlasoru)
                return yeniDosyaYolu
            }

            return null




            //sıkıştırılmış dosya yolu
        }

        override fun onPostExecute(filePath: String?) {
            compressFragment.dismiss()
            (myFragment as ShareIleriFragment).uploadStorage(filePath)
            super.onPostExecute(filePath)
        }

    }


}