package com.besoci.sausosyal.besoci.Share


import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.besoci.sausosyal.besoci.Home.HomeActivity
import com.besoci.sausosyal.besoci.Models.Posts
import com.besoci.sausosyal.besoci.Profile.YukleniyorFragment

import com.besoci.sausosyal.besoci.R
import com.besoci.sausosyal.besoci.Utils.DosyaIslemleri
import com.besoci.sausosyal.besoci.Utils.EventbusDataEvents
import com.besoci.sausosyal.besoci.Utils.UniversalImageLoader
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.OnProgressListener
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.fragment_share_ileri.*
import kotlinx.android.synthetic.main.fragment_share_ileri.view.*
import kotlinx.android.synthetic.main.fragment_yukleniyor.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.lang.Exception

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class ShareIleriFragment : Fragment() {

    var secilenResimYolu:String? = null
    var dosyaTuruResim:Boolean? = null
    //lateinit var photoUri : Uri

    lateinit var mAuth : FirebaseAuth
    lateinit var mRef : DatabaseReference
    lateinit var mUser : FirebaseUser
    lateinit var mStorageReference: StorageReference




    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view =  inflater.inflate(R.layout.fragment_share_ileri, container, false)

        UniversalImageLoader.setImage(secilenResimYolu!!,view!!.imageViewIleri,null,"file://")

        //photoUri = Uri.parse("file://"+secilenResimYolu)

        mAuth = FirebaseAuth.getInstance()
        mUser = mAuth.currentUser!!
        mRef = FirebaseDatabase.getInstance().reference
        mStorageReference = FirebaseStorage.getInstance().reference

        view.fragmentShareIleriTextViewPaylas.setOnClickListener {


            if (editTextIleriAciklama.text.toString().length <= 0) {
                Toast.makeText(context,"Aciklama Bos Gecilemez",Toast.LENGTH_SHORT).show()
            }
             else
            {


            if (dosyaTuruResim!!)//resim dosyası sıkıştırma işlemi
            {

                DosyaIslemleri.cpmpressResim(this, secilenResimYolu)
            } else  //video dosyası sıkıştırma işlemi
            {
                DosyaIslemleri.cpmpressVideo(this, secilenResimYolu!!)
            }


        }
        }

        view.fragmentShareIleriImageViewBack.setOnClickListener {
            this.activity!!.onBackPressed()
        }


        return view
    }

    private fun veriTabaninaYaz(yazilacakURL:String) {

        var postid = mRef.child("posts").child(mUser.uid).push().key
        var yuklenenPost = Posts(mUser.uid,postid,0,editTextIleriAciklama.text.toString() ,yazilacakURL,"","",spinnerMekanTipi.selectedItem.toString(),spinnerKategori.selectedItem.toString()
        ,checkBoxAileler.isChecked,checkBoxSevgililer.isChecked,checkBoxCocuklar.isChecked,checkBoxYetiskinler.isChecked,checkBoxOgrenciler.isChecked,checkBoxYalnizlar.isChecked
        )//LATITUDE LONGITUDE BOS


        mRef.child("posts").child(mUser.uid).child(postid).setValue(yuklenenPost)
        mRef.child("posts").child(mUser.uid).child(postid).child("yuklenme_tarihi").setValue(ServerValue.TIMESTAMP) //timestamp cinsinde kaydetti

        //gönderi açıklamasını yorum düğümüne ilk yorum olarak eklemek
        if(!editTextIleriAciklama.text.toString().isNullOrEmpty())
        {
            mRef.child("comments").child(postid).child(postid).child("user_id").setValue(mUser.uid)
            mRef.child("comments").child(postid).child(postid).child("yorum_tarih").setValue(ServerValue.TIMESTAMP)
            mRef.child("comments").child(postid).child(postid).child("yorum").setValue(editTextIleriAciklama.text.toString())
            mRef.child("comments").child(postid).child(postid).child("yorum_begeni").setValue("0")

        }

        postSayisiniGuncelle()

        var intent = Intent(activity,HomeActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        startActivity(intent)

    }


    fun postSayisiniGuncelle()  //post sayisini 1 arttırıyoruz
    {
        mRef.child("users").child(mUser.uid).child("user_detail").addListenerForSingleValueEvent(object : ValueEventListener
        {
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(p0: DataSnapshot?) {
                var oankiGonderiSayisi = p0!!.child("gonderiSayisi").getValue().toString().toInt() //o anki gönderi sayısını getir
                oankiGonderiSayisi++ //1 arttır
                mRef.child("users").child(mUser.uid).child("user_detail").child("gonderiSayisi").setValue(oankiGonderiSayisi.toString())

            }

        }
        )
    }






    @Subscribe(sticky = true) //eventbustan poststicky ile bişey gönderilmişse yakala
    internal fun onSecilenResimEvent(secilenResim : EventbusDataEvents.PaylasilacakResmiGonder)//benim oluşturduğum classdan geldi
    {


        secilenResimYolu = secilenResim!!.resimYolu!!
        dosyaTuruResim = secilenResim!!.dosyaTuruResim

    }



    override fun onAttach(context: Context?) {
        super.onAttach(context)
        EventBus.getDefault().register(this)
    }

    override fun onDetach() {
        super.onDetach()
        EventBus.getDefault().unregister(this)

    }

    fun uploadStorage(filePath: String?) {

        var fileUri = Uri.parse("file://"+filePath)

        var dialogYukleniyor = YukleniyorFragment()
        dialogYukleniyor.show(activity!!.supportFragmentManager,"yukleniyorfragmentiEklendi")
        dialogYukleniyor.isCancelable = false


        var uploadTask = mStorageReference.child("users").child(mUser.uid).child(fileUri.lastPathSegment+"_post").putFile(fileUri)
            .addOnCompleteListener(object : OnCompleteListener<UploadTask.TaskSnapshot>{
                override fun onComplete(p0: Task<UploadTask.TaskSnapshot>) {
                    if(p0!!.isSuccessful)
                    {
                        veriTabaninaYaz(p0!!.getResult().downloadUrl.toString())
                        dialogYukleniyor.dismiss()
                        Toast.makeText(activity,"Yükleme Başarılı",Toast.LENGTH_LONG).show()

                    }
                    else
                    {
                        dialogYukleniyor.dismiss()
                        Toast.makeText(activity,"Yükleme Sırasında Bir Hata Oluştu",Toast.LENGTH_LONG).show()

                    }
                }
            }).addOnFailureListener(object : OnFailureListener{
                override fun onFailure(p0: Exception) {
                    dialogYukleniyor.dismiss()
                    Toast.makeText(activity,"Yükleme Sırasında Bir Hata Oluştu",Toast.LENGTH_LONG).show()

                }
            })
            .addOnProgressListener ( object : OnProgressListener<UploadTask.TaskSnapshot>
            {
                override fun onProgress(p0: UploadTask.TaskSnapshot?) {
                    var progress:Double = 100.0 * p0!!.bytesTransferred / p0!!.totalByteCount
                    dialogYukleniyor.tvYukleniyor.text = "% " + progress.toInt().toString() +"  yüklendi"
                }

            }
            )
    }

}
