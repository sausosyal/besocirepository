package com.besoci.sausosyal.besoci.Utils

import android.content.Context
import android.graphics.Bitmap
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import com.besoci.sausosyal.besoci.R
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.nostra13.universalimageloader.core.assist.FailReason
import com.nostra13.universalimageloader.core.assist.ImageScaleType
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener

class UniversalImageLoader (val mContext:Context){
    val config :ImageLoaderConfiguration
    get(){

        val defaultOptions = DisplayImageOptions.Builder()
            .showImageOnLoading(loadingimage)
            .showImageForEmptyUri(defaultImage)
            .showImageOnFail(defaultImage)
            .cacheOnDisk(true).cacheInMemory(true)
            .cacheOnDisk(true).resetViewBeforeLoading(true)
            .imageScaleType(ImageScaleType.EXACTLY)
            .considerExifParams(true) //resim dikeyse dikey , yataysa yatay göster
            .bitmapConfig(Bitmap.Config.RGB_565) //kaliteyi düşürüyor resimde
            .displayer(FadeInBitmapDisplayer(400)).build()

        return ImageLoaderConfiguration.Builder(mContext)
            .defaultDisplayImageOptions(defaultOptions)
            .memoryCache(WeakMemoryCache())
            .diskCacheSize(100*1024*1024).build()


    }
companion object {
    private val defaultImage = R.drawable.ic_profile
    private val loadingimage = R.drawable.loading
    fun setImage(imageURL: String, imageView:ImageView?, mProgressBar: ProgressBar?, prefix:String)
    {
//prefix web sitesinden çekilecek resimler için http://
        val imageLoader = ImageLoader.getInstance()
        imageLoader.displayImage(prefix+imageURL,imageView,object :ImageLoadingListener{
            override fun onLoadingComplete(imageUri: String?, view: View?, loadedImage: Bitmap?) {

                if(mProgressBar!= null)
                    mProgressBar.visibility = View.GONE

            }

            override fun onLoadingStarted(imageUri: String?, view: View?) {
                if(mProgressBar!= null)
                    mProgressBar.visibility = View.VISIBLE

            }

            override fun onLoadingCancelled(imageUri: String?, view: View?) {
                if(mProgressBar!= null)
                    mProgressBar.visibility = View.GONE
            }

            override fun onLoadingFailed(imageUri: String?, view: View?, failReason: FailReason?) {
                if(mProgressBar!= null)
                    mProgressBar.visibility = View.GONE
            }


        })
    }
}



}